//
//  QZCheckNetwork.h
//  DDInternshipStu
//
//  Created by 何川 on 15/10/31.
//  Copyright © 2015年 何川. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QZCheckNetwork : NSObject
+(void)checkNetwork;
//检测联网状态
+ (BOOL)isNetworkConnected;
@end
