//
//  QZStandardString.h
//  DDInternshipStu
//
//  Created by 何川 on 15/10/23.
//  Copyright © 2015年 何川. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QZStandardString : NSObject
//神马都是字符串
+(NSString *)standardString:(id)obj;
@end
