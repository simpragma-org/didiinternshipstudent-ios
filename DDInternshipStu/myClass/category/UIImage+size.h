//
//  UIImage+size.h
//  DDInternshipStu
//
//  Created by 何川 on 15/10/28.
//  Copyright © 2015年 何川. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (size)
//对图片尺寸进行压缩--
-(UIImage*)imageScaledToSize:(CGSize)newSize;
@end
