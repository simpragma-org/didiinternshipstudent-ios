//
//  UIViewController+tool.m
//  DDInternshipStu
//
//  Created by 何川 on 15/9/18.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import "UIViewController+tool.h"

@implementation UIViewController (tool)

-(UIView *)createGrayView
{
    UIView *grayView = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    grayView.backgroundColor = [UIColor blackColor];
    grayView.alpha = 0.5;
    grayView.hidden = YES;
    [self.view addSubview:grayView];
    
    return grayView;
}

//添加键盘监听
-(void)addKeyBoardNotificationWithHideKeyBoardAction:(SEL)hideKeyBoardAction andShowKeyBoardAction:(SEL)showKeyBoardAction
{
    //键盘监听
    [[NSNotificationCenter defaultCenter] addObserver:self selector:hideKeyBoardAction name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:showKeyBoardAction name:UIKeyboardWillShowNotification object:nil];
}

//相机或相册页面
-(void)createImagePickerControllerWithSourceType:(UIImagePickerControllerSourceType)sourceType
{
    // 跳转到相机或相册页面
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    imagePickerController.delegate = self;
    
    imagePickerController.allowsEditing = YES;
    
    imagePickerController.sourceType = sourceType;
    
    //导航栏背景图片
    [imagePickerController.navigationBar setBackgroundImage:[UIImage imageNamed:@"1px"] forBarMetrics:UIBarMetricsDefault];
    imagePickerController.navigationBar.shadowImage = [UIImage imageNamed:@"分割线.png"];
    //导航栏标题颜色
    [imagePickerController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,nil]];
    //导航栏按钮颜色
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
    [self presentViewController:imagePickerController animated:YES completion:^{}];
}
@end
