//
//  QZCheckAccesstoken.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/3.
//  Copyright © 2015年 何川. All rights reserved.
//

#import "QZCheckAccesstoken.h"

@implementation QZCheckAccesstoken
{
    NSMutableDictionary *_newParameters;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.request = [[QZRequest alloc]init];
        self.request.delegate = self;
        _newParameters = [[NSMutableDictionary alloc]init];
    }
    return self;
}
+ (id)sharedAccesstoken
{
    static QZCheckAccesstoken *sharedAccesstoken;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedAccesstoken = [[QZCheckAccesstoken alloc] init];
    });
    
    return sharedAccesstoken;
}

@end
