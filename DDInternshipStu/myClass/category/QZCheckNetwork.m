//
//  QZCheckNetwork.m
//  DDInternshipStu
//
//  Created by 何川 on 15/10/31.
//  Copyright © 2015年 何川. All rights reserved.
//

#import "QZCheckNetwork.h"
#import "AFNetworking.h"
#import "Reachability.h"

@implementation QZCheckNetwork
+(void)checkNetwork
{
    //开启网络监测状态提示
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    
    NSURL* netUrl = [NSURL URLWithString:@"http://baidu.com"];
    AFHTTPRequestOperationManager* manager = [[AFHTTPRequestOperationManager alloc]initWithBaseURL:netUrl];
    NSOperationQueue* operationQueue = manager.operationQueue;
    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi:
                //[[iToast makeText:@"网络正常"]show];
                [operationQueue setSuspended:NO];
                break;
                
            case AFNetworkReachabilityStatusNotReachable:
                [[iToast makeText:NSLocalizedString(@"RemoteDistanceKey", @"")]show];
                //世界上最遥远的距离就是没有网
                //The world's most remote distance is no network
                [operationQueue setSuspended:YES];
                break;
                
            default:
                break;
        }
    }];
    [manager.reachabilityManager startMonitoring];
}

//检测联网状态
+ (BOOL)isNetworkConnected {
    Reachability *r = [Reachability reachabilityWithHostName:@"www.baidu.com"];
    switch ([r currentReachabilityStatus]) {
        case NotReachable:
            return NO;
            break;
        case ReachableViaWWAN:
        case ReachableViaWiFi:
            return YES;
            break;
    }
}
@end
