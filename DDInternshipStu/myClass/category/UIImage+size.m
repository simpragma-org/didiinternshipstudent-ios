//
//  UIImage+size.m
//  DDInternshipStu
//
//  Created by 何川 on 15/10/28.
//  Copyright © 2015年 何川. All rights reserved.
//

#import "UIImage+size.h"

@implementation UIImage (size)
//对图片尺寸进行压缩--
-(UIImage*)imageScaledToSize:(CGSize)newSize
{
    // Create a graphics image context
    UIGraphicsBeginImageContext(newSize);
    
    // Tell the old image to draw in this new context, with the desired
    // new size
    [self drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Get the new image from the context
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // End the context
    UIGraphicsEndImageContext();
    
    // Return the new image.
    return newImage;
}
@end
