
//
//  QZStandardString.m
//  DDInternshipStu
//
//  Created by 何川 on 15/10/23.
//  Copyright © 2015年 何川. All rights reserved.
//

#import "QZStandardString.h"

@implementation QZStandardString
+(NSString *)standardString:(id)obj
{
    if([obj isKindOfClass:[NSNull class]] || obj == nil){
        return @"";
    }else if(![obj isKindOfClass:[NSString class]]){
        return [NSString stringWithFormat:@"%@",obj];
    }
    return obj;
}

@end
