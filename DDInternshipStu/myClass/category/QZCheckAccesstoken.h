//
//  QZCheckAccesstoken.h
//  DDInternshipStu
//
//  Created by 何川 on 15/11/3.
//  Copyright © 2015年 何川. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum{
    GETAndCache,
    GET,
    POSTAndCache,
    POST,
    UploadPOST
}QZRequestWay;

@interface QZCheckAccesstoken : NSObject<QZRequestProtocol>
@property (strong,nonatomic)QZRequest *request;

@property (strong,nonatomic)NSString *urlString;
@property (strong,nonatomic)id parameters;
@property (strong,nonatomic)NSString *tagString;
@property (assign,nonatomic)BOOL stop;
@property (assign,nonatomic)BOOL ser;
@property (assign,nonatomic)BOOL isCache;
@property (strong,nonatomic)NSData *imageData;
@property (strong,nonatomic)NSString *pathKey;

@property (assign,nonatomic)id requestDelegate;

@property (assign,nonatomic)QZRequestWay requestWay;

+ (id)sharedAccesstoken;

//获取一个新的Accesstoken
-(void)getNewAccesstoken;
@end
