//
//  NSString+size.h
//  DDInternshipStu
//
//  Created by 何川 on 15/9/16.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (size)
-(CGSize)getSizeWithMaxWidth:(CGFloat)maxWidth andFontSize:(CGFloat)fontSize;
@end
