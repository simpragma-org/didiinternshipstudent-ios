//
//  UIView+nice.h
//  DDInternshipStu
//
//  Created by 何川 on 15/10/16.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (nice)
-(void)RoundedLayerWithCornerRadius:(CGFloat)radius andBorderColor:(UIColor *)color andBorderWidth:(CGFloat)width;

//给某些UIView加圆角边框,圆角边框的属性存在attributeDic中
-(void)RoundedLayerWithAttributeDic:(NSDictionary *)attributeDic;

//给lable或textView的内容加行间距
-(void)lineSpacing:(CGFloat)spacing;
@end
