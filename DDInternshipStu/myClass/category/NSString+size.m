//
//  NSString+size.m
//  DDInternshipStu
//
//  Created by 何川 on 15/9/16.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import "NSString+size.h"

@implementation NSString (size)
-(CGSize)getSizeWithMaxWidth:(CGFloat)maxWidth andFontSize:(CGFloat)fontSize
{
    CGSize size = [self boundingRectWithSize:CGSizeMake(maxWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]} context:nil].size;
    
    return size;
}

@end
