//
//  UIViewController+tool.h
//  DDInternshipStu
//
//  Created by 何川 on 15/9/18.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (tool)<UITextViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>

-(UIView *)createGrayView;//灰色图层

//添加键盘监听
-(void)addKeyBoardNotificationWithHideKeyBoardAction:(SEL)hideKeyBoardAction andShowKeyBoardAction:(SEL)showKeyBoardAction;

-(void)createImagePickerControllerWithSourceType:(UIImagePickerControllerSourceType)sourceType;
@end
