//
//  SQShowFullImage.h
//  DDInternshipStu
//
//  Created by 何川 on 15/10/20.
//  Copyright © 2015年 何川. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQShowFullImage : NSObject <UIScrollViewDelegate>

+(void)showImage:(UIImageView*)avatarImageView;

@end
