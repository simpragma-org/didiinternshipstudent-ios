//
//  UIViewController+SQAlertView.h
//  DDInternshipStu
//
//  Created by 何川 on 15/10/28.
//  Copyright © 2015年 何川. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (SQAlertView)

- (void)showAlertWithString:(id)sender;

@end
