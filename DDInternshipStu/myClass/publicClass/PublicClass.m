//
//  PublicClass.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/25.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "PublicClass.h"

@implementation PublicClass

//处理文章的格式 -关于、规则等
+ (NSAttributedString *)handleArticleWithAttributeStringFromHTML:(NSString *)HTMLTypeString
{
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[HTMLTypeString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    return attrStr;
}

//获得当前时间
+ (NSString *)nowTime
{
    NSDate* now = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    comps = [calendar components:unitFlags fromDate:now];
    int hour = (int)[comps hour];
    int min = (int)[comps minute];
    int sec = (int)[comps second];
    int sum = (hour*3600+min*60+sec)*1000;
    NSString *time = [NSString stringWithFormat:@"%d",sum];
    return time;
}

@end
