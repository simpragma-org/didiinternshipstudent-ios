//
//  PublicClass.h
//  DDInternshipStu
//
//  Created by 何川 on 15/11/25.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PublicClass : NSObject

//处理文章的格式 -关于、规则等
+ (NSAttributedString *)handleArticleWithAttributeStringFromHTML:(NSString *)HTMLTypeString;
//获取当前时间
+ (NSString *)nowTime;

@end
