//
//  QZRequest.h
//  shenBian
//
//  Created by 何川 on 15/8/18.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@protocol QZRequestProtocol <NSObject>


@optional
-(void)request_GET_FinishValue :(id)value tagNSString :(NSString *)tag;

-(void)request_POST_FinishValue :(id)value tagNSString:(NSString *)tag;

-(void)request_Upload_POST_FinishValue :(id)value tagNSString:(NSString *)tag;

-(void)request_Uploads_POST_FinishValue :(id)value tagNSString:(NSString *)tag;

-(void)setUploadProgress :(long long)totalBytesWritten andMax:(long long)totalBytesExpectedToWrite;

- (void)requestFailed:(NSError *)error tagNSString:(NSString *)tag;


@end

@interface QZRequest : NSObject
@property (assign,nonatomic)id<QZRequestProtocol>delegate;


@property (strong,nonatomic) AFHTTPRequestOperationManager *manager;
+(id)shareRequest;

-(instancetype)init_QZrequestWithMethod:(id<QZRequestProtocol>)delegate;
//isCache是否加缓存
-(void )QZRequest_GET :(NSString *)urlString parameters:(id)parameters tagNSString:(NSString *)tag  stopRequest:(BOOL)stop isSerializer:(BOOL)ser isCache:(BOOL)isCache;

-(void )QZRequest_GET :(NSString *)urlString parameters:(id)parameters tagNSString:(NSString *)tag  stopRequest:(BOOL)stop isSerializer:(BOOL)ser;

//isCache是否加缓存
-(void )QZRequest_POST :(NSString *)urlString parameters:(id)parameters tagNSString:(NSString *)tag  stopRequest:(BOOL)stop isSerializer:(BOOL)ser isCache:(BOOL)isCache;

-(void )QZRequest_POST :(NSString *)urlString parameters:(id)parameters tagNSString:(NSString *)tag  stopRequest:(BOOL)stop isSerializer:(BOOL)ser;

-(void)QZRequest_Upload_POST:(NSString *)urlString  parameters:(id)parameters imageData:(NSData *)path pathKey:(NSString *)key tagNSString:(NSString *)tag stopRequest:(BOOL)stop isSerializer:(BOOL)ser;

-(void)QZRequest_Uploads_POST :(NSString *)urlString parameters:(id)parameters tagNSString:(NSString *)tag file:(NSDictionary *)fileDic stopRequest:(BOOL)stop isSerializer:(BOOL)ser;
@end
