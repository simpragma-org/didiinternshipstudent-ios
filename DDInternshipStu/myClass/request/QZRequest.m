//
//  QZRequest.m
//  shenBian
//
//  Created by 何川 on 15/8/18.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import "QZRequest.h"
#import "HTFHudHelper.h"
#import "QZCheckAccesstoken.h"
#import "StubbedControllerViewController.h"

@implementation QZRequest

-(instancetype)init_QZrequestWithMethod:(id<QZRequestProtocol>)delegate
{
    self = [super init];
    if (self){
        //        self.delegate = delegate;
    }
    return self;
}
+(id)shareRequest
{
    static QZRequest *shareRequest;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        shareRequest = [[QZRequest alloc] init];
    });
    return shareRequest;
}
//防止Accesstoken失效用的
-(void)checkAccesstokenWithUrlString:(NSString *)urlString andParameters:(id)parameters andTag:(NSString *)tag andStop:(BOOL)stop andSer:(BOOL)ser andIsCache:(BOOL)isCache andImageData:(NSData *)imageData andPathKey:(NSString *)pathKey andRequestWay:(QZRequestWay)requestWay
{
    [[QZCheckAccesstoken sharedAccesstoken] setUrlString:urlString];
    [[QZCheckAccesstoken sharedAccesstoken] setParameters:parameters];
    [[QZCheckAccesstoken sharedAccesstoken] setTagString:tag];
    [[QZCheckAccesstoken sharedAccesstoken] setStop:stop];
    [[QZCheckAccesstoken sharedAccesstoken] setSer:ser];
    [[QZCheckAccesstoken sharedAccesstoken] setIsCache:isCache];
    [[QZCheckAccesstoken sharedAccesstoken] setImageData:imageData];
    [[QZCheckAccesstoken sharedAccesstoken] setPathKey:pathKey];
    [[QZCheckAccesstoken sharedAccesstoken] setRequestWay:requestWay];
}
-(void )QZRequest_GET :(NSString *)urlString parameters:(id)parameters tagNSString:(NSString *)tag  stopRequest:(BOOL)stop isSerializer:(BOOL)ser isCache:(BOOL)isCache
{
        NSMutableString *caseKey = [NSMutableString stringWithString:urlString];
        NSArray *keys = [parameters allKeys];
        NSArray *values = [parameters allValues];
        for (int i = 0; i < keys.count; i ++) {
            [caseKey appendString:[NSString stringWithFormat:@"/%@=%@",keys[i],values[i]]];
        }
        if (![QZCheckNetwork isNetworkConnected]) {
            [[iToast makeText:NSLocalizedString(@"NetworkKey", @"")]show];
            //请检查您的网络
            //Please check your network
            //没有连接网络就提取缓存里面数据
            
            if(isCache){
                if ([self.delegate respondsToSelector:@selector(request_POST_FinishValue: tagNSString:)]) {
                    id responseObject = [[TMCache sharedCache] objectForKey:caseKey];
                    if (responseObject) {//并且有缓存的情况下
                        [self.delegate request_POST_FinishValue:responseObject tagNSString:tag];
                    }
                }
            }
            return;
        }
    
    if (stop ==YES) {
        [_manager.operationQueue cancelAllOperations];
    }
    
    self.manager = [AFHTTPRequestOperationManager manager];
    
    //取消自动解析
    if (ser == YES) {
        self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }else{
        self.manager.responseSerializer.acceptableContentTypes =[NSSet setWithObjects:@"text/html",@"text/plain",nil];
    }
    //菊花
    [[HTFHudHelper sharedInstance]showHudAcitivityOnWindow];
    [_manager GET:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //Accesstoken失效怎么办？答：加上下面代码就好了
        NSDictionary *dic = nil;
        if([responseObject isKindOfClass:[NSData class]]){
            dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        }else{
            dic = responseObject;
        }
        if ([dic[@"errcode"] intValue] == -2) {
            [[QZCheckAccesstoken sharedAccesstoken] setRequestDelegate:self];
            
            [self checkAccesstokenWithUrlString:urlString andParameters:parameters andTag:tag andStop:stop andSer:ser andIsCache:isCache andImageData:nil andPathKey:nil andRequestWay:GETAndCache];
            
            //accesstoken失效就重新获取Accesstoken，并且再次进行上次请求
            [[QZCheckAccesstoken sharedAccesstoken] getNewAccesstoken];
            return ;
        }
        //请求成功时才做
        NSError*error;
        //隐藏菊花
        [[HTFHudHelper sharedInstance]hideHud];
        if(self.delegate && [self.delegate respondsToSelector:@selector(request_GET_FinishValue: tagNSString:)]){
            [self.delegate request_GET_FinishValue:responseObject tagNSString:tag];
            //数据请求成功就缓存起来吧
            if (isCache) {
                [[TMCache sharedCache] setObject:responseObject forKey:caseKey];
            }
        }
        if (error) {
            NSLog(@"JSON Error:%@",error);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //隐藏菊花
        [[HTFHudHelper sharedInstance]hideHud];
        NSLog(@"Error: %@", error);
        NSLog(@"问题失败%@",operation);
        if (self.delegate && [self.delegate respondsToSelector:@selector(requestFailed:tagNSString:)]) {
            [self.delegate requestFailed:error tagNSString:tag];
        }
    }];
}
/**
 get方式请求数据
 */
-(void )QZRequest_GET :(NSString *)urlString parameters:(id)parameters tagNSString:(NSString *)tag  stopRequest:(BOOL)stop isSerializer:(BOOL)ser
{
    if (![QZCheckNetwork isNetworkConnected]) {
        [[iToast makeText:NSLocalizedString(@"NetworkKey", @"")]show];
        return;
    }
    
    if (stop ==YES) {
        [_manager.operationQueue cancelAllOperations];
    }
    
    self.manager = [AFHTTPRequestOperationManager manager];
    
    //取消自动解析
    if (ser == YES) {
        self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }else{
        self.manager.responseSerializer.acceptableContentTypes =[NSSet setWithObjects:@"text/html",@"text/plain",nil];
    }
    
    //菊花
    [[HTFHudHelper sharedInstance]showHudAcitivityOnWindow];
    [_manager GET:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //隐藏菊花
        [[HTFHudHelper sharedInstance]hideHud];
        
        //请求成功时才做
        NSError*error;
        //Accesstoken失效怎么办？答：加上下面代码就好了
        NSDictionary *dic = nil;
        if([responseObject isKindOfClass:[NSData class]]){
            dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        }else{
            dic = responseObject;
        }
        if ([dic[@"errcode"] intValue] == -2) {
            [[QZCheckAccesstoken sharedAccesstoken] setRequestDelegate:self];
            
            [self checkAccesstokenWithUrlString:urlString andParameters:parameters andTag:tag andStop:stop andSer:ser andIsCache:nil andImageData:nil andPathKey:nil andRequestWay:GET];
            
            //accesstoken失效就重新获取Accesstoken，并且再次进行上次请求
            [[QZCheckAccesstoken sharedAccesstoken] getNewAccesstoken];
            return ;
        }
        
        if(self.delegate && [self.delegate respondsToSelector:@selector(request_GET_FinishValue: tagNSString:)]){
            [self.delegate request_GET_FinishValue:responseObject tagNSString:tag];
        }
        
        if (error) {
            NSLog(@"JSON Error:%@",error);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //隐藏菊花
//        [[HTFHudHelper sharedInstance]hideHud];
        NSLog(@"Error: %@", error);
        NSLog(@"问题失败%@",operation);
        if (self.delegate && [self.delegate respondsToSelector:@selector(requestFailed:tagNSString:)]) {
            [self.delegate requestFailed:error tagNSString:tag];
        }
        
    }];
}
//isCache是否加缓存
-(void )QZRequest_POST :(NSString *)urlString parameters:(id)parameters tagNSString:(NSString *)tag  stopRequest:(BOOL)stop isSerializer:(BOOL)ser
{
    if (![QZCheckNetwork isNetworkConnected]) {
        [[iToast makeText:[NSString stringWithFormat:NSLocalizedString(@"Check network", nil)]]show];
        return;
    }
    if (stop ==YES) {
        [_manager.operationQueue cancelAllOperations];
    }
    self.manager = [AFHTTPRequestOperationManager manager];
    //取消自动解析
    if (ser == YES) {
        self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }else{
        _manager.responseSerializer.acceptableContentTypes =[NSSet setWithObjects:@"text/html",@"text/plain",@"application/json",nil];
    }
    
    if (![tag isEqualToString:@"set_loc"]) {
        //菊花
        [[HTFHudHelper sharedInstance]showHudAcitivityOnWindow];
        }
    //菊花
//    [[HTFHudHelper sharedInstance]showHudAcitivityOnWindow];
//********** TEST CONTROLLER START **********//
    
    
        StubbedControllerViewController *stubbedView = [[StubbedControllerViewController alloc] init];
        
        id dictionary = [stubbedView get_stubbed_data_urlString:urlString tag:tag];
        
        if (!(dictionary==NULL)) {
        [self.delegate request_POST_FinishValue:dictionary tagNSString:tag];
        
        return;
    }
    
//********** TEST CONTROLLER END **********//

    
    
    [_manager POST:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //成功
        //Accesstoken失效怎么办？答：加上下面代码就好了
        NSDictionary *dic = nil;
        if([responseObject isKindOfClass:[NSData class]]){
            dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        }else{
            dic = responseObject;
        }
        if ([dic[@"errcode"] intValue] == -2) {
            [[QZCheckAccesstoken sharedAccesstoken] setRequestDelegate:self];
            
            [self checkAccesstokenWithUrlString:urlString andParameters:parameters andTag:tag andStop:stop andSer:ser andIsCache:nil andImageData:nil andPathKey:nil andRequestWay:POST];
            
            //accesstoken失效就重新获取Accesstoken，并且再次进行上次请求
            [[QZCheckAccesstoken sharedAccesstoken] getNewAccesstoken];
            return ;
        }
        if(self.delegate && [self.delegate respondsToSelector:@selector(request_POST_FinishValue: tagNSString:)]){
            [self.delegate request_POST_FinishValue:responseObject tagNSString:tag];
        }
        //隐藏菊花
        [[HTFHudHelper sharedInstance]hideHud];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        if (self.delegate && [self.delegate respondsToSelector:@selector(requestFailed:tagNSString:)]) {
//            //            //隐藏菊花
//            [[HTFHudHelper sharedInstance]hideHud];
//        }
        if (error.code == -999||error.code == 3840||error.code == -1009||error.code == -1001||error.code == -1003||error.code == -1004||error.code == -1002||error.code == -1012||error.code == -1021||error.code == 500||error.code == -1100||error.code == 406||error.code == -1016||error.code == -1011||error.code == -1202) {
            [[iToast makeText:NSLocalizedString(@"PossibleKey", @"")]show];
            //Possible Network oh well
            //可能网络不好哦
        }else{
            if(error.code == -1005){
                [[iToast makeText:NSLocalizedString(@"NetWorkGoodKey", @"")]show];
                //网络不好，请换个网络重新尝试！
                //Network is not good , please try again another network !
            }else{
                [self.delegate requestFailed:error tagNSString:tag];
            }
        }
        NSLog(@"operation: %@", operation.responseString);
        NSLog(@"Error: %@", error);
    }];

}
/**
 POST请求 参数ser 是否自解析 isCache是否加缓存
 */
-(void )QZRequest_POST :(NSString *)urlString parameters:(id)parameters tagNSString:(NSString *)tag  stopRequest:(BOOL)stop isSerializer:(BOOL)ser  isCache:(BOOL)isCache
{
    NSMutableString *caseKey = [NSMutableString stringWithString:urlString];
    NSArray *keys = [parameters allKeys];
    NSArray *values = [parameters allValues];
    for (int i = 0; i < keys.count; i ++) {
        [caseKey appendString:[NSString stringWithFormat:@"/%@=%@",keys[i],values[i]]];
//        [caseKey appendString:[NSString stringWithFormat:@"&%@=%@",keys[i],values[i]]];

    }
    if (![QZCheckNetwork isNetworkConnected]) {
        [[iToast makeText:NSLocalizedString(@"NetworkKey", @"")]show];
        //没有连接网络就提取缓存里面数据
        if(isCache){
            if ([self.delegate respondsToSelector:@selector(request_POST_FinishValue: tagNSString:)]) {
                id responseObject = [[TMCache sharedCache] objectForKey:caseKey];
                if (responseObject) {//并且有缓存的情况下
                    [self.delegate request_POST_FinishValue:responseObject tagNSString:tag];
                }
            }
        }
        return;
    }
    if (stop ==YES) {
        [_manager.operationQueue cancelAllOperations];
    }
    
    self.manager = [AFHTTPRequestOperationManager manager];
    //取消自动解析
    if (ser == YES) {
        self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }else{
        _manager.responseSerializer.acceptableContentTypes =[NSSet setWithObjects:@"text/html",@"text/plain",@"application/json",nil];
    }
    //菊花
    
    
    
    [[HTFHudHelper sharedInstance]showHudAcitivityOnWindow];
    //***to do
//    if (test_stub_mode) {
//        response_object = get_stubbed_data(urlString, parameters);
//        [self.delegate request_POST_FinishValue:responseObject tagNSString:tag];
//    }

    //********** TEST CONTROLLER START **********//
        
    StubbedControllerViewController *stubbedView = [[StubbedControllerViewController alloc] init];
    
    id dictionary = [stubbedView get_stubbed_data_urlString:urlString tag:tag];
    
    if (!(dictionary==NULL)) {
        [self.delegate request_POST_FinishValue:dictionary tagNSString:tag];
        
        return;
    }
    
    //********** TEST CONTROLLER END **********//


    [_manager POST:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //隐藏菊花
        [[HTFHudHelper sharedInstance]hideHud];
        
        //成功
        //Accesstoken失效怎么办？答：加上下面代码就好了
        NSLog(@"Response: %@",responseObject);
        NSDictionary *dic = nil;
        if([responseObject isKindOfClass:[NSData class]]){
            dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        }else{
            dic = responseObject;
            
        }
        if ([dic[@"errcode"] intValue] == -2) {
            [[QZCheckAccesstoken sharedAccesstoken] setRequestDelegate:self];
            
            [self checkAccesstokenWithUrlString:urlString andParameters:parameters andTag:tag andStop:stop andSer:ser andIsCache:isCache andImageData:nil andPathKey:nil andRequestWay:POSTAndCache];
            
            //accesstoken失效就重新获取Accesstoken，并且再次进行上次请求
            [[QZCheckAccesstoken sharedAccesstoken] getNewAccesstoken];
            return ;
        }
        
        if(self.delegate && [self.delegate respondsToSelector:@selector(request_POST_FinishValue: tagNSString:)]){
            [self.delegate request_POST_FinishValue:responseObject tagNSString:tag];
            //数据请求成功就缓存起来吧
            if (isCache) {
               [[TMCache sharedCache] setObject:responseObject forKey:caseKey];
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //隐藏菊花
        [[HTFHudHelper sharedInstance]hideHud];
        if (self.delegate && [self.delegate respondsToSelector:@selector(requestFailed:tagNSString:)]) {
            [self.delegate requestFailed:error tagNSString:tag];
        }
        NSLog(@"operation: %@", operation.responseString);
        NSLog(@"Error: %@", error);
    }];
}

/**
 上传图片
 */
-(void)QZRequest_Upload_POST:(NSString *)urlString  parameters:(id)parameters imageData:(NSData *)path pathKey:(NSString *)key tagNSString:(NSString *)tag stopRequest:(BOOL)stop isSerializer:(BOOL)ser
{
    
    if (stop ==YES) {
        [_manager.operationQueue cancelAllOperations];
    }
    self.manager = [AFHTTPRequestOperationManager manager];
    
    //取消自动解析
    if (ser == YES) {
        self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }else{
        _manager.responseSerializer.acceptableContentTypes =[NSSet setWithObjects:@"text/html",@"application/json",nil];//,@"text/plain",
    }
    //菊花
    [[HTFHudHelper sharedInstance]showHudAcitivityOnWindow];
    
    AFHTTPRequestOperation *operation = [_manager POST:urlString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (path) {
            NSDate *now = [NSDate date];
            NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
            [dateFormater setDateFormat:@"yyyyMMddHHmmss"];
            
            NSString *fileName = [NSString stringWithFormat:@"%@.png",[dateFormater stringFromDate:now]];
            
            [formData appendPartWithFileData:path name:key fileName:fileName mimeType:@"image/png"];
        }
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //隐藏菊花
        [[HTFHudHelper sharedInstance]hideHud];
        
        NSLog(@"responseObject:%@",responseObject);
        NSDictionary *dic = nil;
        if([responseObject isKindOfClass:[NSData class]]){
            dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        }else{
            dic = responseObject;
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(request_Upload_POST_FinishValue: tagNSString:)]) {
            [self.delegate request_Upload_POST_FinishValue:responseObject tagNSString:tag];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //隐藏菊花
        [[HTFHudHelper sharedInstance]hideHud];
        
        NSLog(@"Error: %@", error);
        NSLog(@"问题失败%@",operation);
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"operationfailsKey", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil, nil];
        //操作失败,请检查网络
        //The operation fails , check the network
        
        [alertView show];
        if (self.delegate && [self.delegate respondsToSelector:@selector(requestFailed: tagNSString:)]) {
            [self.delegate requestFailed:error tagNSString:tag];
        }
        
    }];
    [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                        long long totalBytesWritten,
                                        long long totalBytesExpectedToWrite) {
        NSLog(@"%lu",(unsigned long)bytesWritten);
        NSLog(@"Wrote %lld/%lld", totalBytesWritten, totalBytesExpectedToWrite);
     //[self.delegate setUploadProgress:totalBytesWritten andMax:totalBytesExpectedToWrite];
        
    }];
}
//上传多张图片
-(void)QZRequest_Uploads_POST :(NSString *)urlString parameters:(id)parameters tagNSString:(NSString *)tag file:(NSDictionary *)fileDic stopRequest:(BOOL)stop isSerializer:(BOOL)ser
{
    if (stop ==YES) {
        [_manager.operationQueue cancelAllOperations];
    }
    self.manager = [AFHTTPRequestOperationManager manager];
    //取消自动解析
    if (ser == YES) {
        self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }else{
        _manager.responseSerializer.acceptableContentTypes =[NSSet setWithObjects:@"text/html",@"text/plain",nil];
    }

    //菊花
    [[HTFHudHelper sharedInstance]showHudAcitivityOnWindow];
    
    AFHTTPRequestOperation *operation =[_manager POST:urlString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        NSDate *now = [NSDate date];
        NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
        [dateFormater setDateFormat:@"yyyyMMddHHmmss"];
        
        
        NSArray *keys = [fileDic allKeys];
        for (NSString *key in keys) {
            NSString *fileName = [NSString stringWithFormat:@"%@%@.png",[dateFormater stringFromDate:now],key];
            [formData appendPartWithFileData:fileDic[key] name:key fileName:fileName mimeType:@"image/png"];
        }
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //隐藏菊花
        [[HTFHudHelper sharedInstance]hideHud];
        
        NSLog(@"问题成功%@",operation);
        if(self.delegate && [self.delegate respondsToSelector:@selector(request_Uploads_POST_FinishValue: tagNSString:)] ){
            [self.delegate request_Uploads_POST_FinishValue:responseObject tagNSString:tag];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //隐藏菊花
        [[HTFHudHelper sharedInstance]hideHud];
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"operationfailsKey", @"") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil, nil];
        [alertView show];
        NSLog(@"Error: %@", error);
        NSLog(@"问题失败%@",operation);
    }];
    //NSData *responseObjectData = responseObject;
    //NSString *aa =  [[NSString alloc]initWithData:responseObjectData encoding:NSUTF8StringEncoding];
    [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                        long long totalBytesWritten,
                                        long long totalBytesExpectedToWrite) {
        NSLog(@"%lu",(unsigned long)bytesWritten);
        NSLog(@"Wrote %lld/%lld", totalBytesWritten, totalBytesExpectedToWrite);
        
        if(self.delegate && [self.delegate respondsToSelector:@selector(setUploadProgress: andMax:)] ){
            [self.delegate setUploadProgress:totalBytesWritten andMax:totalBytesExpectedToWrite];
        }
        
    }];
    
    // 5. Begin!
    [operation start];
}

@end
