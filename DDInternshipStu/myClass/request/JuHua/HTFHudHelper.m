

#import "HTFHudHelper.h"
#import "AppDelegate.h"

#define  _AppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@implementation HTFHudHelper

+ (HTFHudHelper *)sharedInstance {
    static HTFHudHelper *_instance = nil;
    
    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }
    
    return _instance;
}

- (void)showHudAcitivityOnWindow
{
    [self showHudOnWindow:nil image:nil acitivity:YES autoHideTime:6];
}

- (void)showHudOnWindow:(NSString *)caption
                  image:(UIImage *)image
              acitivity:(BOOL)active
           autoHideTime:(NSTimeInterval)time1 {
    
    if (_hud) {
        [_hud hide:NO];
    }
    
    self.hud = [[MBProgressHUD alloc] initWithWindow:_AppDelegate.window];
    [_AppDelegate.window addSubview:self.hud];
    self.hud.labelText = caption;
    self.hud.mode = active ? MBProgressHUDModeIndeterminate : MBProgressHUDModeText;
    self.hud.animationType = MBProgressHUDAnimationFade;
    self.hud.removeFromSuperViewOnHide = YES;
    [self.hud show:YES];
    time1 = 6;
    if (time1 > 0) {
        [self.hud hide:YES afterDelay:time1];
    }
}

- (void)showHudOnView:(UIView *)view
              caption:(NSString *)caption
                image:(UIImage *)image
            acitivity:(BOOL)active
         autoHideTime:(NSTimeInterval)time1 {
    
    if (_hud) {
        [_hud hide:NO];
    }
    
    self.hud = [[MBProgressHUD alloc] initWithView:view];
    [view addSubview:self.hud];
    self.hud.labelText = caption;
    self.hud.mode = active ? MBProgressHUDModeIndeterminate : MBProgressHUDModeText;
    self.hud.animationType = MBProgressHUDAnimationFade;
    [self.hud show:YES];
    if (time1 > 0) {
        [self.hud hide:YES afterDelay:time1];
    }
}

- (void)setCaption:(NSString *)caption {
    self.hud.labelText = caption;
}

- (void)hideHud {
    if (_hud) {
        [_hud hide:YES];
    }
}

- (void)hideHudAfter:(NSTimeInterval)time1 {
    if (_hud) {
        [_hud hide:YES afterDelay:time1];
    }
}

@end
