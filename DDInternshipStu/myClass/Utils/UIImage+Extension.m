//
//  UIImage+Extension.m
//  TextUtil
//
//  Created by zx_04 on 15/8/20.
//  Copyright (c) 2015年 joker. All rights reserved.
//

#import "UIImage+Extension.h"

@implementation UIImage (Extension)

+ (UIImage *)resizableImageWithName:(NSString *)imageName
{
    UIImage *norImage = [UIImage imageNamed:imageName];
    
    UIImage *resizeImg = [norImage resizableImageWithCapInsets:UIEdgeInsetsMake(30, 20, 25, 20)];
    
    return resizeImg;
}

@end
