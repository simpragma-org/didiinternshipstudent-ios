//
//  AppDelegate.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/13.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "AppDelegate.h"
#import "APService.h"
#import "Reachability.h"
#import <CoreLocation/CoreLocation.h>
#import "QZRequest.h"
#import "UMSocial.h"
#import "UMSocialWechatHandler.h"
#import "UMSocialSinaSSOHandler.h"
#import "UMSocialQQHandler.h"
#import "WeiboSDK.h"
#import "MobClick.h"
#import <Bugly/CrashReporter.h>
#import "DDHomeViewController.h"
#import "DDMessageViewController.h"
#import "DDNewOrderViewController.h"
#define set_loc @"index.php?s=/Home/Student/set_loc"
//微博appKey
#define kAppKey     @"2773267246"
//友盟
#define UMengAPPKey @"5642ea5b67e58e6b320025ab"
@interface AppDelegate ()<QZRequestProtocol>{
    QZRequest *_request;
}
@end

@implementation AppDelegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    //检测网络
    [QZCheckNetwork checkNetwork];
    //跳到登录页面
    _loginVc = [[QZLoginViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:_loginVc];
    [nav.navigationBar setBackgroundImage:[UIImage imageNamed:@"1px"] forBarMetrics:UIBarMetricsDefault];
    self.window.rootViewController = nav;
    
    //状态栏颜色变白
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    // 极光推送 Required
    if (IS_IOS8){
        //可以添加自定义categories
        [APService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                                       UIUserNotificationTypeSound |
                                                       UIUserNotificationTypeAlert)
                                           categories:nil];
    } else {
        //categories 必须为nil
        [APService registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                       UIRemoteNotificationTypeSound |
                                                       UIRemoteNotificationTypeAlert)
                                           categories:nil];
    }
    
    // Required
    [APService setupWithOption:launchOptions];
    
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self selector:@selector(networkDidReceiveMessage:) name:kJPFNetworkDidReceiveMessageNotification object:nil];
    //百度地图
    _mapManager = [[BMKMapManager alloc]init];
    // 如果要关注网络及授权验证事件，请设定     generalDelegate参数
    BOOL ret = [_mapManager start:@"2x40Nzhh6jhEgCo2hvK8YNB2"  generalDelegate:nil];
    if (!ret) {
        NSLog(@"manager start failed!");
    }
    //初始化BMKLocationService
    _locService = [[BMKLocationService alloc]init];
    _locService.delegate = self;
    //启动LocationService
    [_locService startUserLocationService];
    //注册友盟分享
    //-------------------------------------------------------------------
    [UMSocialData setAppKey:UMengAPPKey];
    //设置微信AppId、appSecret，分享url
    [UMSocialWechatHandler setWXAppId:@"wxf8848ff36dad9be5" appSecret:@"d4624c36b6795d1d99dcf0547af5443d" url:@"http://www.umeng.com/social"];
    //设置手机QQ 的AppId，Appkey，和分享URL，需要#import "UMSocialQQHandler.h"
    [UMSocialQQHandler setQQWithAppId:@"1104996826" appKey:@"8BY9oi2CXtgVtztG" url:@"http://www.umeng.com/social"];
    [WeiboSDK enableDebugMode:YES];
    [WeiboSDK registerApp:kAppKey];
    [UMSocialSinaSSOHandler openNewSinaSSOWithAppKey:kAppKey RedirectURL:@"http://sns.whalecloud.com/sina2/callback"];
    //-------------------------------------------------------------------
    //注册友盟统计
    [MobClick setAppVersion:XcodeAppVersion]; //参数为NSString * 类型,自定义app版本信息，如果不设置，默认从CFBundleVersion里取
    [MobClick startWithAppkey:UMengAPPKey reportPolicy:(ReportPolicy) REALTIME channelId:nil];
    //将channelId:@"Web" 中的Web 替换为您应用的推广渠道。channelId为nil或@""时，默认会被当作@"App Store"渠道。
    //-------------------------------------------------------------------
    //bugly
    [[CrashReporter sharedInstance] installWithAppId:@"900014123"];
    //-------------------------------------------------------------------
    //判断推送的
    if ([DDUserInfo uid]) {
        [self handleRemotePush];
    }
    return YES;
}
//调用sdk
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    //友盟
    BOOL result = [UMSocialSnsService handleOpenURL:url];
    if (result == FALSE) {
        //调用其他SDK，例如支付宝SDK等
    }
//    if ([sourceApplication isEqualToString:@"com.sina.weibo"]) {
//        [[iToast makeText:@"分享成功"]show];
//    }
    return result;
}
//处理方向变更信息
- (void)didUpdateUserHeading:(BMKUserLocation *)userLocation{
    //NSLog(@"heading is %@",userLocation.heading);
}
//处理位置坐标更新
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation{
    NSLog(@"didUpdateUserLocation lat %f,long %f",userLocation.location.coordinate.latitude,userLocation.location.coordinate.longitude);
    
    [[TMCache sharedCache]setObject:[NSString stringWithFormat:@"%f",userLocation.location.coordinate.latitude] forKey:@"latitude"];
    [[TMCache sharedCache]setObject:[NSString stringWithFormat:@"%f",userLocation.location.coordinate.longitude] forKey:@"longitude"];
    //关闭
//    [_locService stopUserLocationService];
}
//app进入前台执行的方法
- (void)applicationDidBecomeActive:(UIApplication *)application {
    //启动LocationService
//    DDHomeViewController *homeVC = [[DDHomeViewController alloc]init];
//    [homeVC loadCellDate];
    if ([DDUserInfo uid]) {
        [self handleRemotePush];
    }
    [_locService startUserLocationService];
    application.applicationIconBadgeNumber = 0;
    NSString *url = [NSString stringWithFormat:@"%@%@",publicUrl,set_loc];
    if (![DDUserInfo uid]||![DDUserInfo latitude]||![DDUserInfo longitude]) {
        return;
    }
    NSString *log = [DDUserInfo longitude];
    NSString *lat = [DDUserInfo latitude];
    if (![DDUserInfo longitude]) {
        log = @"69.222439";
    }
    if (![DDUserInfo latitude]) {
        lat = @"76.224022";
    }
    NSDictionary *dic = @{@"uid":[DDUserInfo uid],
                          @"longitude":log,
                          @"latitude":lat
                          };
    NSLog(@"%@",dic);
    _request = [[QZRequest alloc]init];
    _request.delegate = self;
    [_request QZRequest_POST:url parameters:dic tagNSString:@"set_loc" stopRequest:NO isSerializer:NO];
}
#pragma mark ------<网络请求>
-(void)request_POST_FinishValue:(id)value tagNSString:(NSString *)ta{
    NSLog(@"value = %@",value);
    NSString *errcode = [value objectForKey:@"status"];
    if ([errcode intValue] == 1) {
        [_locService stopUserLocationService];
    }else{
         [[iToast makeText:[value objectForKey:@"msg"]]show];
    }
}
#pragma mark - Jpush
- (void)networkDidReceiveMessage:(NSNotification *)notification {
    NSDictionary * userInfo = [notification userInfo];
    NSDictionary *extras = [userInfo valueForKey:@"extras"];
    NSString *customizeField1 = [extras valueForKey:@"customizeField1"]; //自定义参数，key是自己定义的
    NSLog(@"Jpush自定义方法，这个方法没有调用，看看可不可以注释掉\n我在AppDelegate里面\n我的自定义参数有：userInfo：%@\ncustomizeField1:%@",userInfo,customizeField1);
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"did Fail To Register For Remote Notifications With Error: %@", error);
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    [APService registerDeviceToken:deviceToken];
    NSLog(@"registrationID  === %@",[APService registrationID]);
    NSString* registrationId = [APService registrationID];
    if (registrationId) {
        [[TMCache sharedCache] setObject:registrationId forKey:@"registrationId"];
        [[TMCache sharedCache] setObject:registrationId forKey:@"device_number"];
    }
}
//App状态为正在前台或者后台运行在此方法里接收消息
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [APService handleRemoteNotification:userInfo];// 处理收到的APNS消息，向服务器上报收到APNS消息
    [self handleTheNotificationInfoWithAPS:userInfo[@"aps"] NotiType:userInfo[@"notiType"]];
    if([userInfo[@"type"] isEqualToString:@"2"]){
        //修改手机号
        DDMessageViewController *messageCenterVC = [[DDMessageViewController alloc]init];
        [self.homeVc.navigationController pushViewController:messageCenterVC animated:YES];
    }else if([userInfo[@"type"] isEqualToString:@"1"]||[userInfo[@"type"] isEqualToString:@"3"]){
        //新的预约
        DDNewOrderViewController *newOrder = [[DDNewOrderViewController alloc]init];
        [self.homeVc.navigationController pushViewController:newOrder animated:YES];
    }else{
        
        
    }
}
//无论app是否在运行消息都在这里接收
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    [APService handleRemoteNotification:userInfo];// 处理收到的APNS消息，向服务器上报收到APNS消息
//    NSLog(@"通知回调userInfo:%@",userInfo);
//    [self handleTheNotificationInfoWithAPS:userInfo[@"aps"] NotiType:userInfo[@"notiType"]];
//    NSString* news = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
////    [[iToast makeText:news] show];
    completionHandler(UIBackgroundFetchResultNewData);
    [self handleTheNotificationInfoWithAPS:userInfo[@"aps"] NotiType:userInfo[@"type"]];
}
//处理推送详情
- (void)handleTheNotificationInfoWithAPS:(NSDictionary *)aps NotiType:(NSString *)type
{
    NSString *alertStr = aps[@"alert"];
    if ([DDUserInfo uid]) {
        UIAlertView *msgAlert = [[UIAlertView alloc]initWithTitle:@"消息提示" message:alertStr delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:@"去看看",nil];
        if (type == nil) {
            type = @"0";
        }
        msgAlert.tag = 222+[type intValue];
        [msgAlert show];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ( alertView.tag == 224) { //消息中心
        if (buttonIndex == 0) { //知道了
            return;
        }else{ //去看看
            if([NSStringFromClass([[_homeVc.navigationController.childViewControllers lastObject] class]) isEqualToString:@"DDMessageViewController"]){
                [[NSNotificationCenter defaultCenter]postNotificationName:@"reflesh" object:nil];
            }else{
                DDMessageViewController *messageCenterVC = [[DDMessageViewController alloc]init];
                [self.homeVc.navigationController pushViewController:messageCenterVC animated:YES];
            }
        }
    } else if (alertView.tag == 223 ||alertView.tag == 225 || alertView.tag == 222) { //所有预约
        if (buttonIndex == 0) { //知道了
            return;
        }else{ //去看看
            if([NSStringFromClass([[_homeVc.navigationController.childViewControllers lastObject] class]) isEqualToString:@"DDNewOrderViewController"]){
                [[NSNotificationCenter defaultCenter]postNotificationName:@"reflesh" object:nil];
            }else{
                DDNewOrderViewController *newOrderVC = [[DDNewOrderViewController alloc]init];
                [self.homeVc.navigationController pushViewController:newOrderVC animated:YES];
            }
        }
    }else{
    
    }
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - 链接后台设置

+ (NSString*)timeKey{
    NSDate* dataNow         = [NSDate date];
    NSTimeZone* zone        = [NSTimeZone systemTimeZone];
    NSInteger interval      = [zone secondsFromGMTForDate:dataNow];
    NSDate* localeData      = [dataNow dateByAddingTimeInterval:interval];
    NSString* chuoTimeStr = [NSString stringWithFormat:@"%ld", (long)[localeData timeIntervalSince1970]];
    return chuoTimeStr;
}
//MD5加密
+ (NSString*)secretMd5Key
{
    NSString* keyStr = @"jD9u3%$#@62i3_=ebjt6sJHsdksd#@66";
    NSString* secrectKey = [[NSString stringWithFormat:@"%@%@",keyStr,[self timeKey]] MD5];
    return secrectKey;
}
+ (NSString *)nowDateTime
{
    NSDate* now = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    comps = [calendar components:unitFlags fromDate:now];
    NSInteger month   = (int)[comps month];
    int day     = (int)[comps day];
    int hour    = (int)[comps hour];
    int min     = (int)[comps minute];
    int sec     = (int)[comps second];
    NSString *time = [NSString stringWithFormat:@"%ld:%d:%d:%d:%d",(long)month,day,hour,min,sec];
    return time;
}
//检测联网状态
+ (BOOL)isNetworkConnected {
    Reachability *r = [Reachability reachabilityWithHostName:@"www.baidu.com"];
    switch ([r currentReachabilityStatus]) {
        case NotReachable:
            return NO;
            break;
        case ReachableViaWWAN:
        case ReachableViaWiFi:
            return YES;
            break;
    }
}
//获得当前时间
+ (NSString *)nowTime
{
    NSDate* now = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    comps = [calendar components:unitFlags fromDate:now];
    int hour = (int)[comps hour];
    int min = (int)[comps minute];
    int sec = (int)[comps second];
    int sum = (hour*3600+min*60+sec)*1000;
    NSString *time = [NSString stringWithFormat:@"%d",sum];
    return time;
}

- (void)handleRemotePush
{
    if (![self isAllowedNotification]) {
        [[TMCache sharedCache] setObject:@(0) forKey:@"isPush"];
        [[TMCache sharedCache] setObject:@"0" forKey:@"isCanNotification"]; //系统推送
        // IOS8新系统需要使用新的代码咯
        if(IS_IOS8) {
            [[UIApplication sharedApplication]registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound|UIUserNotificationTypeAlert|UIUserNotificationTypeBadge) categories:nil]];
            
            [[UIApplication sharedApplication] registerForRemoteNotifications];
            
        }else{
            //这里还是原来的代码
            //注册启用push
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             (UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeBadge)];
        }
        // 推送 Required
        if (IS_IOS8) {
            //可以添加自定义categories
            [APService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                                           UIUserNotificationTypeSound |
                                                           UIUserNotificationTypeAlert)
                                               categories:nil];
        } else {
            //categories 必须为nil
            [APService registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                           UIRemoteNotificationTypeSound |
                                                           UIRemoteNotificationTypeAlert)
                                               categories:nil];
        }
    }else{
        [[TMCache sharedCache] setObject:@"1" forKey:@"isCanNotification"]; //系统推送
    }
}

- (BOOL)isAllowedNotification
{
    if (IS_IOS8) {// system is iOS8 +
        UIUserNotificationSettings *setting = [[UIApplication sharedApplication] currentUserNotificationSettings];
        if (UIUserNotificationTypeNone != setting.types) {
            return YES;
        }
    }else{// iOS7
        UIRemoteNotificationType type = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if(UIRemoteNotificationTypeNone != type)
            return YES;
    }
    return NO;
}


@end
