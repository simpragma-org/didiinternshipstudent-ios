//
//  StubbedControllerViewController.h
//  DDIntershipStu
//
//  Created by Abhijit Paul on 1/12/16.
//  Copyright © 2016 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface StubbedControllerViewController : UIViewController

- (NSDictionary *)get_stubbed_data_urlString:(NSString *)urlString tag:(NSString *)tag;

@end
