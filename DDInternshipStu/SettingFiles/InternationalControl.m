//
//  InternationalControl.m
//  MyInternational
//
//  Created by 何川 on 15/11/13.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "InternationalControl.h"

static NSBundle *bundle = nil;

@implementation InternationalControl

//创建静态变量bundle，以及获取方法bundle（注：此处不要使用getBundle）
+ (NSBundle *)bundle
{
    return bundle;
}

//初始化方法：
/**
 *  userLanguage储存在NSUserDefaults中，首次加载时要检测是否存在，如果不存在的话读AppleLanguages，并赋值给userLanguage
 */
+ (void)initUserLanguage
{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    
    NSString *string = [def valueForKey:@"userLanguage"];
    
    if (string.length == 0) {
        
        //获取系统当前语言版本（中文zh-Hans,英文en）
        
        NSArray *languages = [def objectForKey:@"AppleLanguages"];
        
        NSString *current = [languages objectAtIndex:0];
        
        NSString *currentShort;
        
        if (IS_IOS9) {
            
            long length = [current length] - 3;
            
            currentShort = [current substringToIndex:length];
            
        }else{
            
            currentShort = current;
            
        }
        
        string = currentShort;
        
        [def setValue:currentShort forKey:@"userLanguage"];
        
        [def synchronize];//持久化，不加的话不会保存
    }
    
    //获取文件路径
    NSString *path = [[NSBundle mainBundle] pathForResource:string ofType:@"lproj"];
    
    bundle = [NSBundle bundleWithPath:path];//生成bundle
}

//设置当前语言方法
+ (void)setUserLanguage:(NSString *)language
{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    
    //1.第一步改变bundle的值
    NSString *path = [[NSBundle mainBundle] pathForResource:language ofType:@"lproj"];
    
    bundle = [NSBundle bundleWithPath:path];
    
    //2.持久化
    [def setValue:language forKey:@"userLanguage"];
    
    [def synchronize];
}

//获得当前语言方法
+ (NSString *)userLanguage
{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    
    NSString *string = [def valueForKey:@"userLanguage"];
    
    return string;
}

@end
