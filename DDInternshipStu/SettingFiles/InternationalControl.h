//
//  InternationalControl.h
//  MyInternational
//
//  Created by 何川 on 15/11/13.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface InternationalControl : NSObject

+ (NSBundle *)bundle; //获取当前资源文件

+ (void)initUserLanguage;//初始化语言文件

+ (NSString *)userLanguage;//获取应用当前语言

+ (void)setUserLanguage:(NSString *)language;//设置当前语言

@end
