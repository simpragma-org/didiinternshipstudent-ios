//
//  CourseModel.h
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/25.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "JSONModel.h"

@interface CourseModel : JSONModel
/** id */
@property (nonatomic, copy) NSString *id;
/** 开始时间 */
@property (nonatomic, copy) NSString *start_time;
/** 状态 */
@property (nonatomic, copy) NSString *status;
/** title */
@property (nonatomic, copy) NSString *title;
/** surplus 剩余 */
@property (nonatomic, copy) NSString *surplus;
/** 结束时间 */
@property (nonatomic, copy) NSString *end_time;
/** 时间id 唯一 */
@property (nonatomic, copy) NSString *timetable_id;
/** 时间id 唯一 */
@property (nonatomic, copy) NSString *cur_id;
/** 价格 */
@property (nonatomic, copy) NSString *price;
/** 课程表状态唯一标示 */
@property (nonatomic, copy) NSString *flag;
@end
