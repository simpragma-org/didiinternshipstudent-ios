//
//  DDHomeStateCell.m
//
//
//  Created by HERO-PC on 15/11/17.
//  Copyright © 2015年 何川. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CourseModel.h"
@interface DDHomeStateCell : UICollectionViewCell
/** 状态cell */
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;

@property (weak, nonatomic) IBOutlet UIImageView *lockImage;

-(void)setCellWithModel:(CourseModel *)model;
@end
