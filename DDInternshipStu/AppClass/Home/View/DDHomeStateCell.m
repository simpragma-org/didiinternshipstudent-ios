//
//  DDHomeStateCell.m
//
//
//  Created by HERO-PC on 15/11/17.
//  Copyright © 2015年 何川. All rights reserved.
//

#import "DDHomeStateCell.h"

@implementation DDHomeStateCell

- (void)awakeFromNib {
    // Initialization code
}
-(void)setCellWithModel:(CourseModel *)model{
    //当前时间
//    NSString *nowTime = [self nowTimeChuo];
    if ([model.flag isEqualToString:@"7"]||[model.flag isEqualToString:@"8"]) {
        //时间过期 没有课
        self.lockImage.hidden = YES;
        self.stateLabel.hidden = NO;
        self.userInteractionEnabled = NO;
        self.stateLabel.text =NSLocalizedString(@"ExpiredKey", @"");
        //已过期
        //Expired
        self.stateLabel.textColor = [UIColor colorWithHexString:@"#C0CAD1"];
        self.backgroundColor = [UIColor colorWithHexString:@"#E7EFF3"];
    }else if ([model.flag isEqualToString:@"4"]||[model.flag isEqualToString:@"5"]) {
        //完成未评价的和已评价的
        self.lockImage.hidden = YES;
        self.stateLabel.hidden = NO;
        self.userInteractionEnabled = NO;
        self.stateLabel.text = NSLocalizedString(@"CompletedKey", @"");
        //已完成
        //Completed
        self.stateLabel.textColor = [UIColor colorWithHexString:@"#C0CAD1"];
        self.backgroundColor = [UIColor colorWithHexString:@"#E7EFF3"];
    }else if ([model.flag isEqualToString:@"6"]){
        //有预约 时间在开始结束中间
        self.lockImage.hidden = YES;
        self.stateLabel.hidden = NO;
        self.stateLabel.text =NSLocalizedString(@"BeingInternshipKey", @"");
        //正在实习
        //Being Internship
        self.stateLabel.textColor = COLOR_DEFAULT;
        self.userInteractionEnabled = NO;
        self.backgroundColor = [UIColor whiteColor];
    }else if ([model.flag isEqualToString:@"1"]){
        //未反馈有预约状态
        self.lockImage.hidden = YES;
        self.stateLabel.hidden = NO;
        self.stateLabel.text =NSLocalizedString(@"NoFeedbackKey", @"");
        //No Feedback
        //未反馈
        self.stateLabel.textColor = COLOR_DEFAULT;
        self.userInteractionEnabled = NO;
        self.backgroundColor = [UIColor colorWithHexString:@"#E7EFF3"];
    }else if ([model.flag isEqualToString:@"3"]){
        //未开始的有预约状态
        self.lockImage.hidden = YES;
        self.stateLabel.hidden = NO;
        self.stateLabel.text = NSLocalizedString(@"HaveanappointmentKey", @"");
        //有预约
        //Have an appointment
        self.userInteractionEnabled = YES;
        self.stateLabel.textColor = COLOR_DEFAULT;
        self.backgroundColor = [UIColor colorWithHexString:@"#E7EFF3"];
    }else{
        if ([model.status isEqualToString:@"1"]){
            //可以预约 空闲状态
            self.lockImage.hidden = YES;
            self.stateLabel.hidden = NO;
            self.userInteractionEnabled = YES;
            self.stateLabel.text = NSLocalizedString(@"idleKey", @"");
            //空闲
            //idle
            self.stateLabel.textColor = [UIColor colorWithHexString:@"#70787C"];
            self.backgroundColor = [UIColor colorWithHexString:@"#E7EFF3"];
        }else if([model.status isEqualToString:@"0"]){
            //锁状态
            self.lockImage.hidden = NO;
            self.stateLabel.hidden = YES;
            self.userInteractionEnabled = YES;
            self.backgroundColor = COLOR_GEZI_BACKGROUND;
        }
    }
}
//获取当前时间戳
-(NSString *)nowTimeChuo{
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval a=[dat timeIntervalSince1970];
    NSString *timeString = [NSString stringWithFormat:@"%f", a];
    return timeString;
}
@end
