//
//  DDDDHomeViewController.m
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/17.
//  Copyright © 2015年 何川. All rights reserved.
//

#import "DDHomeViewController.h"
#import "DDHomeStateCell.h"
#import "LLTimeView.h"
#import "DDNewOrderViewController.h"
#import "AppDelegate.h"
#import "CourseModel.h"
#import "DDAppointmentDetailViewController.h"
#import "QZRequest.h"
#define set_timetable @"index.php?s=/Home/Student/set_timetable"
@interface DDHomeViewController ()
{
    UIScrollView * _dayScrollView;
    NSMutableArray *_allCouses;
    NSTimer *_changeTime;
    /** 放选择的course的集合 */
    NSMutableSet *_selectedCourses;
    /** 放7个时间date的*/
    NSMutableArray *_dateArray;
    /** 放选择course的数组 用于请求的数组 */
    NSMutableArray *_selectedCourseArray;
}
@end

@implementation DDHomeViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];//防止卡屏
    self.navigationItem.title = [NSString stringWithFormat:NSLocalizedString(@"Didi Internship", nil)];
    
   [self addBtnOnNavWithTitle:nil andImageName:@"人" andTarget:self andAction:@selector(leftAction:) andFrame:CGRectMake(0, 0, 26, 26) andDirection:LEFT];
    [self addBtnOnNavWithTitle:[NSString stringWithFormat:NSLocalizedString(@"New appointment", nil)] andImageName:nil andTarget:self andAction:@selector(rightAction:) andFrame:CGRectMake(0, 0, 80, 20) andDirection:RIGHT];
    [self setUI];
    //创建CollectionView
    [self createHomeCollectionView];
    
}
//注册collectionView
-(void)createHomeCollectionView{
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.bounces = NO;
    for (int i = 0; i<56; i++) {
        [self registerCellWithNibName:@"DDHomeStateCell" addIdentifier:[NSString stringWithFormat:@"homeCell%d",i] addItemSize:CGSizeMake((WIDTH - 88)/4,(HEIGHT-44-64)/7 - 0.5) andItemSpacing:0.5 andInsetsForSection:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    [self loadData];
}
-(void)viewWillAppear:(BOOL)animated{
    [self loadData];
}
-(void)loadData{
    //请求课程表
    NSString *urlstr = @"http://192.168.2.133:8080/internserver/Home/Common/get_timetable?secret_key=C8AEF3FD988AF5F6";
//    NSString *urlstr = [NSString stringWithFormat:@"%@%@",publicUrl,@"index.php?s=/Home/Student/get_timetable"];
    // ##############
//    NSDictionary *dic = @{@"uid":[DDUserInfo uid]};
    NSDictionary *dic = @{@"uid": @"student1"};
    [self.request QZRequest_POST:urlstr parameters:dic tagNSString:@"get_timetable" stopRequest:NO isSerializer:NO isCache:YES];
    _selectedCourses = [[NSMutableSet alloc]init];
}
#pragma mark requestPool
-(void)request_POST_FinishValue:(id)value tagNSString:(NSString *)tag{
    NSLog(@"value = %@   tag = %@",value,tag);
    if ([tag isEqualToString:@"get_timetable"]) {
        NSString *errcode = [value objectForKey:@"status"];
        if ([errcode intValue] == 1) {
            NSArray *couses_7day = [value objectForKey:@"data"];
            [self.collectionSource removeAllObjects];
            _dateArray = [[NSMutableArray alloc]init];
            for (NSDictionary *couses in couses_7day) {
                for (NSDictionary *couse in couses[@"timeslot"]) {
                    [self.collectionSource addObject:couse];
                }
                [_dateArray addObject:couses[@"date"]];
            }
            [self.collectionView reloadData];
        }else{
            NSString *msg = [value objectForKey:@"msg"];
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Prompt", nil)] message:msg delegate:self cancelButtonTitle:[NSString stringWithFormat:NSLocalizedString(@"OK", nil)] otherButtonTitles:nil];
            [alertView show];
            [self.collectionView reloadData];//remove it afterwards.just for bypass purpose.
        }
    }else if([tag isEqualToString:@"set_timetable"]){
        NSString *errcode = [value objectForKey:@"status"];
        if ([errcode intValue] == 1) {
            [[iToast makeText:[NSString stringWithFormat:NSLocalizedString(@"Set timetable success", nil)]]show];
            [_selectedCourseArray removeAllObjects];
            [_selectedCourses removeAllObjects];
        }else{
            NSString *msg = [value objectForKey:@"msg"];
            NSLog(@"msg: %@", [value objectForKey:@"msg"]);
            [[iToast makeText:msg]show];
        }
    }
}
-(void)setUI{
    UIView * dayView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 44)];
//    if (IS_IOS7 && !IS_IOS8) {
//        dayView.frame = CGRectMake(0, 64, WIDTH, 44);
//    }
    dayView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:dayView];
    //左面按钮
    UIView * leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
//    if (IS_IOS7 && !IS_IOS8) {
//      leftView.frame = CGRectMake(0, 64, 44, 44);
//    }
    UIImageView *leftImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 24, 24)];
    leftImage.image = [UIImage imageNamed:@"arrow1-1"];
    [leftView addSubview:leftImage];
    [dayView addSubview:leftView];
    //右面按钮
    UIView * rightView = [[UIView alloc]initWithFrame:CGRectMake(WIDTH-44,0, 44, 44)];
//    if (IS_IOS7 && !IS_IOS8) {
//        rightView.frame = CGRectMake(WIDTH-44,64, 44, 44);
//    }
    UIImageView *rightImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 24, 24)];
    rightImage.image = [UIImage imageNamed:@"arrow-1"];
    [rightView addSubview:rightImage];
    [dayView addSubview:rightView];
    //左按钮方法
    UITapGestureRecognizer * leftTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(leftTap:)];
    [leftView addGestureRecognizer:leftTap];
    //右按钮方法
    UITapGestureRecognizer * rightTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(rightTap:)];
    [rightView addGestureRecognizer:rightTap];
    _dayScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(44, 0, WIDTH-88, 44)];
//    if (IS_IOS7 && !IS_IOS8) {
//        _dayScrollView.frame = CGRectMake(WIDTH-44,64 + 64, 44, 44);
//    }
    _dayScrollView.bounces = NO;
    _dayScrollView.contentSize = CGSizeMake((WIDTH - 88)*2, 44);
    _dayScrollView.delegate = self;
    _dayScrollView.showsHorizontalScrollIndicator = NO;
    _dayScrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_dayScrollView];
    //七天的时间
    NSArray * dayArray = [self getSevenDay];
    for (int i=0; i<dayArray.count; i++) {
        NSDictionary * dict  = [dayArray objectAtIndex:i];
        UILabel * weekLabel = [[UILabel alloc]initWithFrame:CGRectMake(((WIDTH - 88)/4)*i, 8, ((WIDTH - 88)/4), 9)];
        weekLabel.text = [dict objectForKey:@"week"];
        weekLabel.textColor = COLOR(138, 139, 140);
        weekLabel.font = [UIFont systemFontOfSize:9];
        weekLabel.textAlignment = NSTextAlignmentCenter;
        [_dayScrollView addSubview:weekLabel];
        UILabel * dayLabel = [[UILabel alloc]initWithFrame:CGRectMake(((WIDTH - 88)/4)*i, 18, ((WIDTH - 88)/4), 14)];
        if (i == 0) {
            dayLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Today", nil)];
        }else{
            dayLabel.text = [dict objectForKey:@"day"];
        }
        dayLabel.textColor = COLOR(138, 139, 140);
        dayLabel.font = [UIFont systemFontOfSize:14];
        dayLabel.textAlignment = NSTextAlignmentCenter;
        [_dayScrollView addSubview:dayLabel];
    }
    float mindle;
    mindle = (HEIGHT-44-64)/7;
    NSArray * array = @[@{@"start":@"08:00",@"end":@"10:00"},@{@"start":@"10:00",@"end":@"12:00"},@{@"start":@"12:00",@"end":@"14:00"},@{@"start":@"14:00",@"end":@"16:00"},@{@"start":@"16:00",@"end":@"18:00"},@{@"start":@"18:00",@"end":@"20:00"},@{@"start":@"20:00",@"end":@"22:00"}];
    for (int i=0; i<array.count; i++) {
        NSDictionary * dict = [array objectAtIndex:i];
        LLTimeView * timeView = [[LLTimeView alloc]initWithFrame:CGRectMake(0, 44+mindle*i, 44,mindle-0.5)];
        timeView.backgroundColor = COLOR_DEFAULT;
        timeView.startLabel.text = [dict objectForKey:@"start"];
        timeView.endLable.text = [dict objectForKey:@"end"];
        [self.view addSubview:timeView];
    }
}
//拱主页刷新用的 因为重名了 所以改个名字便于区分。
-(void)loadCellDate{
    QZRequest *homeRequest = [[QZRequest alloc]init];
    homeRequest.delegate = self;
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",publicUrl,@"index.php?s=/Home/Student/get_timetable"];
    NSDictionary *dic = @{@"uid":[DDUserInfo uid]};
   [homeRequest QZRequest_POST:urlstr parameters:dic tagNSString:@"get_timetable" stopRequest:NO isSerializer:NO isCache:YES];
}
//日期和课程表的scroll方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGPoint contentOffSet = scrollView.contentOffset;
    self.collectionView.contentOffset = CGPointMake(contentOffSet.x, 0);
    _dayScrollView.contentOffset = CGPointMake(contentOffSet.x, 0);
}
//处理主页上方的日期方法
- (NSArray *)getSevenDay{
    NSArray * arrWeek=[[NSArray alloc ]initWithObjects:@"Sun.",@"Mon.",@"Tues.",@"Wed.",@"Thur.",@"Fri.",@"Sat.",nil];
    NSMutableArray * dayArray = [[NSMutableArray alloc]init];
    for (int i=0; i<8; i++) {
        NSDate *date = [NSDate date];
        date = [date dateByAddingTimeInterval:60*60*24*i];
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *comps = [[NSDateComponents alloc] init];
        NSInteger unitFlags = NSCalendarUnitYear |
        NSCalendarUnitMonth |
        NSCalendarUnitDay |
        NSCalendarUnitWeekday |
        NSCalendarUnitHour |
        NSCalendarUnitMinute |
        NSCalendarUnitSecond;
        comps = [calendar components:unitFlags fromDate:date];
        NSInteger week = [comps weekday];
        NSInteger month = [comps month];
        NSInteger day = [comps day];
        NSString * timeDay = [NSString stringWithFormat:@"%.2ld.%.2ld",(long)month,(long)day];
        NSString * weekDay = [NSString stringWithFormat:@"%@",[arrWeek objectAtIndex:week-1]];
        NSDictionary * dict = [[NSDictionary alloc]initWithObjectsAndKeys:timeDay,@"day",weekDay,@"week", nil];
        [dayArray addObject:dict];
    }
    return dayArray;
}
//collectionCell内容方法
-(void)loadCell:(UICollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    DDHomeStateCell *homeCell = (DDHomeStateCell *)cell;
    CourseModel *model = [[CourseModel alloc]initWithDictionary:self.collectionSource[indexPath.row] error:nil];
    //去cell的类中判断显示状态
    [homeCell setCellWithModel:model];
}
//点击触发的方法
-(void)actionAtIndexPath:(NSIndexPath *)indexPath{
     NSDictionary *couseDic = self.collectionSource[indexPath.row];
    NSLog(@"couseDic: %@", couseDic[@"flag"]);
    if ([couseDic[@"flag"]isEqualToString:@"0"]) {
        NSLog(@"couseDic: %@", couseDic[@"flag"]);

    }
    //如果不是能点的参数就刷新界面
    if (![couseDic[@"flag"]isEqualToString:@"0"]&&![couseDic[@"flag"]isEqualToString:@"2"]&&![couseDic[@"flag"]isEqualToString:@"3"]) {
        [self.collectionView reloadData];
        return;
    }
    if ([couseDic[@"flag"] isEqualToString:@"3"]) {
        //有预约的状况 点击进入预约详情
        DDAppointmentDetailViewController *appoDetail = [[DDAppointmentDetailViewController alloc]init];
        appoDetail.hrId = couseDic[@"hrid"];
        appoDetail.cur_id = couseDic[@"cur_id"];
        NSString *start_time = couseDic[@"start_time"];
        appoDetail.timeSlotStr =[NSString stringWithFormat:@"%@ %@",[self timeStringWithDate:start_time],couseDic[@"title"]];
        [self.navigationController pushViewController:appoDetail animated:YES];
        return;
    }
    NSMutableDictionary *cellDic = [[NSMutableDictionary alloc]initWithDictionary:self.collectionSource[indexPath.row]];
    DDHomeStateCell *cell = (DDHomeStateCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    if (cell.lockImage.hidden) {
        cell.stateLabel.hidden = YES;
        cell.lockImage.hidden = NO;
        cell.backgroundColor = COLOR_GEZI_BACKGROUND;
        [cellDic setObject:@"0" forKey:@"status"];
    }else{
        cell.lockImage.hidden = YES;
        cell.stateLabel.hidden = NO;
        cell.stateLabel.text = NSLocalizedString(@"avl", @"");
        cell.stateLabel.textColor = [UIColor colorWithHexString:@"#70787C"];
        cell.backgroundColor = [UIColor colorWithHexString:@"#E7EFF3"];
        [cellDic setObject:@"1" forKey:@"status"];
    }
    //替换dataSourse
    if (self.collectionSource) {
        [self.collectionSource replaceObjectAtIndex:indexPath.row withObject:cellDic];
    }
    [_selectedCourses addObject:@(indexPath.row)];
    //每次点击关闭定时器
    [_changeTime invalidate];
    _changeTime = nil;
    //然后重新添加计时器 0.5秒后不操作进行请求
    _changeTime = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(changeStatus) userInfo:nil repeats:NO];
}
//状态统一请求
-(void)changeStatus{
    _selectedCourseArray = [[NSMutableArray alloc]init];
    for (NSString *index in _selectedCourses) {
        NSInteger row = [index integerValue];
        NSDictionary *dic = self.collectionSource[row];
        NSNumber *time_slot = dic[@"id"];
        NSNumber *date = _dateArray[row/7];
        NSString *status = dic[@"status"];
        NSDictionary *selectedCourse = @{@"time_slot":time_slot,
                                         @"status":@([status intValue]),
                                         @"date":date
                                         };
        [_selectedCourseArray addObject:selectedCourse];
    }
    if(_selectedCourseArray.count == 0){
        return;
    }
    //改变的状态进行网络请求
    NSString * courseJson = [self stringTOjson:_selectedCourseArray];
//    NSString *courseJson = [_selectedCourseArray JSONString];
    NSDictionary *dic = @{@"uid":[DDUserInfo uid],
                          @"data":courseJson
                          };
    NSString *url = [NSString stringWithFormat:@"%@%@",publicUrl,set_timetable];
    [self.request QZRequest_POST:url parameters:dic tagNSString:@"set_timetable" stopRequest:YES isSerializer:NO];
}
//时间戳处理
-(NSString *)timeStringWithDate:(NSString *)add_time{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm"];
    NSTimeZone* timeZone = [NSTimeZone systemTimeZone];
    [formatter setTimeZone:timeZone];
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:[add_time floatValue]];
    NSString *startStr = [formatter stringFromDate:startDate];
    NSString *startTimeSlot = [startStr substringToIndex:10];
    return startTimeSlot;
}
//把字典和数组转换成json字符串
-(NSString *)stringTOjson:(id)temps{
    NSData* jsonData =[NSJSONSerialization dataWithJSONObject:temps
                                                      options:NSJSONWritingPrettyPrinted error:nil];
    NSString *strs=[[NSString alloc] initWithData:jsonData
                                         encoding:NSUTF8StringEncoding];
    return strs;
}
//抽屉方法
- (void)leftAction:(UIButton *)btn{
    [_AppDelegate.ddMenu showLeftController:YES];
}
//新的预约按钮
- (void)rightAction:(UIButton *)btn
{
    DDNewOrderViewController *newOrderVc = [[DDNewOrderViewController alloc] init];
    [self.navigationController pushViewController:newOrderVc animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//右按钮
- (void)rightTap:(UITapGestureRecognizer *)tap
{
    [self.collectionView setContentOffset:CGPointMake(WIDTH - 88 - 44, 0) animated:YES];
}
//左按钮
- (void)leftTap:(UITapGestureRecognizer *)tap
{
    [self.collectionView setContentOffset:CGPointMake(0, 0) animated:YES];
}
//关闭主页右划返回
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
