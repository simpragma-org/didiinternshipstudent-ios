

#import "LLTimeView.h"

@implementation LLTimeView

#define DEVICEWIDTH [UIScreen mainScreen].bounds.size.width
#define DEVICEHIGHT [UIScreen mainScreen].bounds.size.height
#define appDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])
//横向比例
#define WidthScale(number) ([UIScreen mainScreen].bounds.size.width/320*(number))
//纵向比例
#define HeightScale(number) ([UIScreen mainScreen].bounds.size.height/480*(number))
//十六进制色值
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = COLOR_DEFAULT;
        self.startLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, HeightScale(20), self.bounds.size.width, HeightScale(10))];
        self.startLabel.font = [UIFont systemFontOfSize:10];
        [self addSubview:self.startLabel];
        self.endLable = [[UILabel alloc]initWithFrame:CGRectMake(0, HeightScale(33), self.bounds.size.width, HeightScale(10))];
        self.endLable.font = [UIFont systemFontOfSize:10];
        [self addSubview:self.endLable];
        self.startLabel.textColor = [UIColor whiteColor];
        self.endLable.textColor = [UIColor whiteColor];
        self.startLabel.textAlignment = NSTextAlignmentCenter;
        self.endLable.textAlignment = NSTextAlignmentCenter;
        UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake((self.bounds.size.width-HeightScale(10))/2, HeightScale(8), HeightScale(11), HeightScale(11))];
        imageView.image = [UIImage imageNamed:@"iconfont-shijian-(1)-1"];
        [self addSubview:imageView];
        UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, HeightScale(27), self.bounds.size.width, HeightScale(10))];
        label.text = @"－";
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor whiteColor];
        
        label.font = [UIFont systemFontOfSize:11];
        [self addSubview:label];
    }
    return self;
}

@end
