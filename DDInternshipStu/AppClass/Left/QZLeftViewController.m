//
//  QZLeftViewController.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/14.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "QZLeftViewController.h"
#import "DDSystemSetViewController.h"
#import "DDinviteFriendViewController.h"
#import "DDSelfInfoViewController.h"
#import "DDMessageViewController.h"
#import "DDLeftHeadViewCell.h"
#import "DDNewOrderViewController.h"
#import "DDLeftViewCell.h"
#import "AppDelegate.h"



@interface QZLeftViewController ()<UIAlertViewDelegate>

@end

@implementation QZLeftViewController{
    DDLeftHeadViewCell *_headView;
    UIWebView *_phoneCallWebView;
}
/**
 *  可能需要重写tableview的initWithFrame方法
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createTableViewWithPlainStyle:YES andSeparatorStyleNone:YES andBackGroudImageName:@"侧边栏背景"];
    
    [self loadData];
    
    [self registCellWithNib:@"DDLeftViewCell" addIdentifier:@"leftCell" addHeight:44];
    
    [self createTableHead];
}

-(void)viewWillAppear:(BOOL)animated
{
    //头像
    if([DDUserInfo headerImage]){
        [_headView.headBtn setBackgroundImage:[DDUserInfo headerImage] forState:UIControlStateNormal];
    }else{
        if ([DDUserInfo headerImageUrl]) {
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:[DDUserInfo headerImageUrl]
                                  options:0
                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                     // progression tracking code
                                 }
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (image) {
                                        // do something with image
                                        [DDUserInfo headerImage];
                                        [_headView.headBtn setBackgroundImage:image forState:UIControlStateNormal];
                                    }
                                }];
        }
    }
}

//创建左侧边栏的头
- (void)createTableHead
{
    DDLeftHeadViewCell *headView = (DDLeftHeadViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"DDLeftHeadViewCell" owner:self options:nil] lastObject];
    [headView.headBtn addTarget:self action:@selector(headBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    headView.frame = CGRectMake(0, 0, WIDTH, 194);
    headView.backgroundColor = [UIColor clearColor];
    headView.nameLabel.text = [DDUserInfo name];
    _headView = headView;
    self.tableView.tableHeaderView = headView;
}

- (void)headBtnAction:(UIButton *)button
{
    //点击头像进入用户个人信息
    DDSelfInfoViewController *selfInfoVc = [[DDSelfInfoViewController alloc] init];
    [_AppDelegate.homeVc.navigationController pushViewController:selfInfoVc animated:YES];
    [_AppDelegate.ddMenu showRootController:YES];
}

//创建数据源
- (void)loadData
{
    NSArray *leftImageArr = @[@"user",@"iconfont-yuyue",@"iconfont-shengriliwu",@"iconfont-commentfill-拷贝",@"iconfont-servicefill",@"iconfont-ordinaryset-拷贝"];
    
    NSArray *titleArr = @[NSLocalizedString(@"PersonalInformationKey", @""),NSLocalizedString(@"All appointmentsKey", @""),NSLocalizedString(@"InviteafriendKey", @""),NSLocalizedString(@"MessageCenterKey", @""),NSLocalizedString(@"CallCenterKey", @""),NSLocalizedString(@"SettingsKey", @"")];
    //[ @ " Personal information " , @ " All appointments " , @ "Invite a friend " @ "message center", @ " call center " , @ "Settings" ]
    //@[@"个人信息",@"所有预约",@"邀请好友",@"消息中心",@"客服中心",@"设置"];
    
    for (int i = 0 ; i < leftImageArr.count; i++) {
        NSDictionary *dic = @{@"leftImage":leftImageArr[i],@"title":titleArr[i]};
        [self.tableSource addObject:dic];
    }
    
    [self.tableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (void)loadCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = self.tableSource[indexPath.row];
    DDLeftViewCell *left = (DDLeftViewCell*)cell;
    left.backgroundColor = [UIColor clearColor];
    left.titleLabel.text = dic[@"title"];
    left.titleLabel.font = [UIFont systemFontOfSize:14];
    left.titleLabel.textColor = COLOR_LEFTLABEL;
    left.leftView.image = [UIImage imageNamed:dic[@"leftImage"]];
}

- (void)actionAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *nextVc;
    switch (indexPath.row) {
        case 0:
        {
            //个人信息
            DDSelfInfoViewController *selfInfoVc = [[DDSelfInfoViewController alloc] init];
            nextVc = selfInfoVc;
        }
            break;
        case 1:
        {
            //所有预约
            DDNewOrderViewController *newOrderVC = [[DDNewOrderViewController alloc]init];
            nextVc = newOrderVC;
        }
            break;
        case 2:
        {
            //邀请好友
            DDinviteFriendViewController *inviteVc = [[DDinviteFriendViewController alloc] init];
            nextVc = inviteVc;
        }
            break;
        case 3:
        {
            //消息中心
            DDMessageViewController *messageVc = [[DDMessageViewController alloc] init];
            nextVc = messageVc;
        }
            break;
        case 4:
        {
            //客服中心
            UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"FriendlyReminderKey", @"") message:NSLocalizedString(@"FriendlyAlertmsgKey", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"OkKey", @"") otherButtonTitles:NSLocalizedString(@"CancelKey", @""), nil];
            [alertView show];
            //友情提示 //Friendly Reminder
            //您是否要拨打滴滴实习客服电话 //Do you want to call the customer service phone drops Internship
            //确定 //OK
            //取消 //Cancel
            return;
        }
            break;
        case 5:
        {
            //设置
            DDSystemSetViewController *sysVc = [[DDSystemSetViewController alloc] init];
            nextVc = sysVc;
        }
            break;
            
        default:
            break;
    }
    if (nextVc) {
        [_AppDelegate.homeVc.navigationController pushViewController:nextVc animated:YES];
        [_AppDelegate.ddMenu showRootController:YES];
    }
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [self CallPhoneWithNum:phoneNumber];
        }
            break;
            
        default:
            break;
    }
}
//拨打电话方法
-(void)CallPhoneWithNum:(NSString *)phoneNum{
    
    [[TMCache sharedCache] setObject:@"1" forKey:@"webView"];
    NSURL *phoneURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneNum]];
    if ( !_phoneCallWebView ) {
        _phoneCallWebView = [[UIWebView alloc]initWithFrame:CGRectZero];// 这个webView只是一个后台的容易 不需要add到页面上来 效果跟方法二一样 但是这个方法是合法的
    }
    [_phoneCallWebView loadRequest:[NSURLRequest requestWithURL:phoneURL]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
