//
//  DDLeftHeadViewCell.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/14.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDLeftHeadViewCell.h"
#import "UIView+nice.h"

@implementation DDLeftHeadViewCell

- (void)awakeFromNib {
    self.backgroundColor = [UIColor clearColor];
    
    CGFloat headCenter = WIDTH/4*3/2;
    CGFloat middle = WIDTH/2-headCenter;
    self.headViewCenter.constant =  -middle;
    self.nameLabelCenter.constant = -middle;
    
    NSDictionary *attributeDic = @{CornerRadius:@(self.headView.frame.size.width/2),BorderColor:[UIColor lightGrayColor], BorderWidth:@(0.8)};
    
    [self.headView RoundedLayerWithAttributeDic:attributeDic];
    [self.headBtn RoundedLayerWithAttributeDic:@{CornerRadius:@(self.headBtn.frame.size.width/2),BorderColor:[UIColor clearColor], BorderWidth:@(0)}];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
