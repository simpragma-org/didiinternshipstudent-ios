//
//  DDLeftViewCell.h
//  DDInternshipStu
//
//  Created by 何川 on 15/11/14.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDLeftViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *leftView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *line1;
@property (weak, nonatomic) IBOutlet UILabel *line2;

// 最开始的Y
@property (nonatomic, assign) CGFloat originY;

@end
