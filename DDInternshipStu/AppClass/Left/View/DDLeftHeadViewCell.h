//
//  DDLeftHeadViewCell.h
//  DDInternshipStu
//
//  Created by 何川 on 15/11/14.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDLeftHeadViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *headView;
@property (weak, nonatomic) IBOutlet UIButton *headBtn;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headViewCenter;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLabelCenter;

@end
