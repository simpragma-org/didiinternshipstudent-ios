//
//  DDLeftViewCell.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/14.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDLeftViewCell.h"

@implementation DDLeftViewCell

- (void)awakeFromNib {
    
    self.line1.backgroundColor = COLORWITH(@"#363D43");
    self.line2.backgroundColor = COLORWITH(@"#1B2227");
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
