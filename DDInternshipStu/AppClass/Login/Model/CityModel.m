//
//  CityModel.m
//  chenzhenCode
//
//  Created by apple on 15/11/25.
//  Copyright © 2015年 wo. All rights reserved.
//

#import "CityModel.h"

@implementation CityModel


+(BOOL)propertyIsOptional:(NSString *)propertyName{
    return YES;
}


-(instancetype)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err{
    
    if (self = [super initWithDictionary:dict error:err]) {

        if (dict[@"district"]) {
            
            NSMutableArray *district = [NSMutableArray array];
            NSArray *array = dict[@"district"];
            
            for (NSDictionary *dic in array) {
                
                CityModel *model = [[CityModel alloc] initWithDictionary:dic error:nil];
                [district addObject:model];
            }
            _district = district;

        }
    }
    return self;
}
@end
