//
//  CityModel.h
//  chenzhenCode
//
//  Created by apple on 15/11/25.
//  Copyright © 2015年 wo. All rights reserved.
//

#import "JSONModel.h"
@class centerModel;
@interface CityModel : JSONModel

@property (nonatomic, copy) NSString *alias;

@property (nonatomic, strong) NSArray *district;

@property (nonatomic, assign) NSInteger id;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, assign) NSInteger pid;

@property (nonatomic, assign) NSInteger sort;
@end
