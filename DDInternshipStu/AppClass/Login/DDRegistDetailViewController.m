//
//  DDRegistDetailViewController.m
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/19.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDRegistDetailViewController.h"
#import "DDRegistDetailHeadView.h"
#import "DDRegistDetailTopCell.h"
#import "QZBottomView.h"
#import "DDSelectAddressViewController.h"
#import "MLInputDodger.h"
#import "QZPickerViewController.h"
#import "DDRegistDetailAddSelCell.h"
#import "DDRegistDetailInputViewController.h"
#import "CityModel.h"
#import "APService.h"

#define  REGISTERURL @"index.php?s=/Home/Student/register"

@interface DDRegistDetailViewController ()<UITextViewDelegate,UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    UITableView *_addressTableView;
    NSMutableArray *_stateArr;
    DDRegistDetailTopCell *_headCell;
    NSString *_showStr;
    NSString *cityName;
    NSString *discName;
    NSInteger cityId;
    NSInteger districtId;
    
}
@property (strong,nonatomic)QZPickerViewController *pickerVc1;

@property (strong,nonatomic)UIPickerView *pickerView;

@property (strong,nonatomic)NSMutableArray *pickerArray;

@property (strong,nonatomic)NSArray *selectPickweArray;
@end

@implementation DDRegistDetailViewController

-(NSMutableArray *)pickerArray{
    if (_pickerArray == nil) {
        _pickerArray = [NSMutableArray array];
    }
    return _pickerArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = [NSString stringWithFormat:NSLocalizedString(@"Personal information", nil)];
    [self addBtnOnNavWithTitle:nil andImageName:@"返回" andTarget:self andAction:@selector(backAction) andFrame:CGRectMake(0, 0, 18, 15) andDirection:LEFT];
    [self createTableViewWithPlainStyle:NO andSeparatorStyleNone:NO andBackGroudImageName:nil];
    [self registCellWithNib:@"DDRegistDetailAddSelCell" addIdentifier:@"addCell" addHeight:44];
    
    self.tableView.separatorStyle  = UITableViewCellSeparatorStyleNone;
    self.tableView.bounces = NO;
    self.tableView.frame = CGRectMake(0, 85, WIDTH, HEIGHT - 85 - 64);
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    //加载头视图
    [self createTableHeaderView];
    [self createHeaderView];
    //创建脚视图
    [self createFooterView];
    //加载pickerVC
    //    [self createPickerVcs];
    [self careatePickerView];
    //加载数据
    [self loadData];
    
}
-(void)loadData{
    [self.tableSource addObject:@[@""]];
}
#pragma mark requestPool
-(void)request_POST_FinishValue:(id)value tagNSString:(NSString *)tag
{
    NSLog(@"value%@,tag:%@",value,tag);
    if ([tag isEqualToString:@"get_district_list"]) {
        NSLog(@"value = %@",value);
        NSString *errcode = [value objectForKey:@"status"];
        if ([errcode intValue] == 1) {
            NSArray *cityDataArr = value[@"data"];
            NSMutableArray *cityArr = [[NSMutableArray alloc]init];
            for (NSDictionary *dic in cityDataArr) {
                CityModel *model = [[CityModel alloc] initWithDictionary:dic error:nil];
                [cityArr addObject:model.name];
                [self.pickerArray addObject:model];
            }
            NSMutableArray * dicAllName = [[NSMutableArray alloc]init];
            for ( int i = 0 ; i<cityDataArr.count; i++) {
                NSMutableArray *dictrictNameArr = [[NSMutableArray alloc]init];
                for (NSDictionary *dic in cityDataArr[i][@"district"]) {
                    CityModel *model3 = [[CityModel alloc]initWithDictionary:dic error:nil];
                    [dictrictNameArr addObject: model3.name];
                }
                [dicAllName addObject:dictrictNameArr];
            }
            CityModel *mModel = _pickerArray[0];
            _selectPickweArray = mModel.district;
            [_pickerView reloadAllComponents];
        }else{
            NSString *msg = [value objectForKey:@"msg"];
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"PromtKey", @"") message:msg delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil];
            //提示
            //Promt
            //确定
            //OK
            [alertView show];
        }
    }else if([tag isEqualToString:@"Register"]){
        if ([value[@"status"] intValue] == 1) {
            [[iToast makeText:NSLocalizedString(@"SuccessKey", @"")] show];
            //注册成功
            //Registration Success

            NSDictionary *userInfo = value[@"data"];
            NSDictionary *dic = @{@"uid":userInfo[@"uid"],
                                  @"name":userInfo[@"name"],
                                  @"mobile":userInfo[@"mobile"],
                                  @"device_number":userInfo[@"device_number"],
                                  @"studentSex":userInfo[@"sex"],
                                  @"studentRead":userInfo[@"degree"],
                                  @"studentAge":userInfo[@"age"],
                                  @"studentSchool":userInfo[@"schoolname"]
                                  };
            [DDUserInfo keepUserMessageWithDic:dic];
            [self handleRemotePush];
            DDRegistDetailInputViewController *inputVC = [[DDRegistDetailInputViewController alloc]init];
            inputVC.uid = value[@"data"][@"uid"];
            [self.navigationController pushViewController:inputVC animated:YES];
        }else{
            [[iToast makeText:[NSString stringWithFormat:@"%@",value[@"msg"]]] show];
        }
    }
}
#pragma mark - UITextField
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    UITextField *nextField;
    if (textField.tag == 2002) {
        [textField resignFirstResponder];
    }else{
        nextField = [self.view viewWithTag:textField.tag+1];
        [nextField becomeFirstResponder];
    }
    return YES;
}
#pragma mark ------<tableView>
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_stateArr[0] intValue]) {
        return 244;
    }else{
        return 44;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DDRegistDetailAddSelCell *addCell = [tableView dequeueReusableCellWithIdentifier:@"addCell"];
    addCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self addPickerViewWithPickerVc:_pickerView AtView:addCell AtState:[_stateArr[0] intValue]];
    UITextField *textField = [self.view viewWithTag:2002];
    [textField resignFirstResponder];
    return addCell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        [_stateArr replaceObjectAtIndex:0 withObject:@(![_stateArr[0] intValue])];
        [self createFooterView];
    }
    [self.tableView reloadData];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.000001;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.000001;
}
//键盘方法
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //距离键盘距离
    self.tableView.shiftHeightAsDodgeViewForMLInputDodger = 50;
    //注册
    [self.tableView registerAsDodgeViewForMLInputDodger];
}
//点击男女方法
-(void)manBtn:(UIButton *)btn{
    if(btn == _headCell.manBtn){
        _headCell.manBtn.selected = !_headCell.manBtn.selected;
        _headCell.womanBtn.selected = !_headCell.womanBtn.selected;
    }
    if (btn == _headCell.womanBtn) {
        _headCell.manBtn.selected = !_headCell.manBtn.selected;
        _headCell.womanBtn.selected = !_headCell.womanBtn.selected;
    }
    if(btn == _headCell.benBtn){
        _headCell.benBtn.selected = !_headCell.benBtn.selected;
        _headCell.yanBtn.selected = !_headCell.yanBtn.selected;
    }
    if (btn == _headCell.yanBtn) {
        _headCell.benBtn.selected = !_headCell.benBtn.selected;
        _headCell.yanBtn.selected = !_headCell.yanBtn.selected;
    }
}
#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == 2001) {
        if ([textField.text integerValue] == 0 && [string integerValue] == 0) {
            return NO;
        }
        if ([[textField.text stringByAppendingString:string] intValue] > 100) {
            return NO;
        }
    }
    
    return YES;
}
//提交按钮
-(void)createFooterView{
    QZBottomView *bottomView = [[[NSBundle mainBundle]loadNibNamed:@"QZBottomView" owner:self options:nil]lastObject];
    bottomView.frame = CGRectMake(0, 0, WIDTH, 102);
    [bottomView.bottomBtn setTitle:[NSString stringWithFormat:NSLocalizedString(@"Next", nil)] forState:UIControlStateNormal];
    [bottomView.bottomBtn addTarget:self action:@selector(bottomBtnAction) forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableFooterView = bottomView;
}
//下一步按钮方法
-(void)bottomBtnAction{
    if ([_headCell.nameTextField.text isEqualToString:@""]) {
        [[iToast makeText:[NSString stringWithFormat:NSLocalizedString(@"Blank name", nil)]]show];
        return;
    }
    if([_headCell.ageTextField.text isEqualToString:@""]){
        [[iToast makeText:NSLocalizedString(@"Age empty", nil)]show];
        return;
    }
    if ([_headCell.schoolTextField.text isEqualToString:@""]) {
        [[iToast makeText:[NSString stringWithFormat:NSLocalizedString(@"Empty school", nil)]]show];
        return;
    }
    
    if(cityId == -1 || districtId == -1||cityId == 0|| districtId == 0){
        [[iToast makeText:[NSString stringWithFormat:NSLocalizedString(@"Empty city", nil)]]show];
        return;
    }
    //获取设备号
    NSString *identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSLog(@"identifier:%@",identifier);
    
    int code  = [self.phoneInfo[@"phoneCode"] intValue];
    int degree;
    if (!_headCell.benBtn.selected) {
        degree = 2;
    }else{
        degree = 1;
    }
    int sex;
    if (!_headCell.manBtn.selected) {
        sex = 2;
    }else{
        sex = 1;
    }
    int age =[_headCell.ageTextField.text intValue];
    NSString *schoolname = _headCell.schoolTextField.text;
    NSString *name = _headCell.nameTextField.text;
    if ([DDUserInfo longitude]==nil ||[DDUserInfo latitude]== nil) {
        [[TMCache sharedCache]setObject:@"69.222439" forKey:@"latitude"];
        [[TMCache sharedCache]setObject:@"76.224022" forKey:@"longitude"];
    }
    NSDictionary *dic = @{@"mobile":self.phoneInfo[@"phoneNumber"],
                          @"name":name,
                          @"degree":@(degree),
                          @"schoolname":schoolname,
                          @"sex":@(sex),
                          @"age":@(age),
                          @"device_number":[DDUserInfo device_number],
                          @"code":@(code),
                          @"cityname":cityName,
                          @"longitude":[DDUserInfo longitude],
                          @"latitude":[DDUserInfo latitude],
                          @"city":@(cityId),
                          @"district":@(districtId)
                          };
    NSLog(@"%@",dic);
    NSString *url = [NSString stringWithFormat:@"%@%@",publicUrl,REGISTERURL];
    [self.request QZRequest_POST:url parameters:dic tagNSString:@"Register" stopRequest:NO isSerializer:NO isCache:NO];
}
//picker方法
-(void)addPickerViewWithPickerVc:(UIPickerView *)pickerVc AtView:(DDRegistDetailAddSelCell *)hyCell AtState:(int)stateValue
{
    if (stateValue) {
        [UIView animateWithDuration:0.25 animations:^{
            _pickerView.frame = CGRectMake(0, 44, WIDTH,200);
            [hyCell.contentView addSubview:_pickerView];
        }];
    }else{
        [UIView animateWithDuration:0.25 animations:^{
            hyCell.addTextField.text = _showStr;
            [pickerVc removeFromSuperview];
        }];
    }
}
-(void)createTableHeaderView{
    DDRegistDetailHeadView *headView = [[[NSBundle mainBundle]loadNibNamed:@"DDRegistDetailHeadView" owner:self options:nil]lastObject];
    headView.frame = CGRectMake(0, 0, WIDTH,85);
    [self.view addSubview:headView];
}
-(void)createHeaderView{
    _headCell = [[[NSBundle mainBundle]loadNibNamed:@"DDRegistDetailTopCell" owner:self options:nil]lastObject];
    _headCell.frame = CGRectMake(0, 0, WIDTH,215);
    _headCell.manBtn.selected = YES;
    _headCell.benBtn.selected = YES;
    _headCell.nameTextField.delegate = self;
    _headCell.ageTextField.delegate = self;
    _headCell.schoolTextField.delegate = self;
    //四个按钮加方法
    [_headCell.manBtn addTarget:self action:@selector(manBtn:) forControlEvents:UIControlEventTouchUpInside];
    [_headCell.womanBtn addTarget:self action:@selector(manBtn:) forControlEvents:UIControlEventTouchUpInside];
    [_headCell.benBtn addTarget:self action:@selector(manBtn:) forControlEvents:UIControlEventTouchUpInside];
    [_headCell.yanBtn addTarget:self action:@selector(manBtn:) forControlEvents:UIControlEventTouchUpInside];
    UIView *view = [[UIView alloc] initWithFrame:[_headCell frame]];
    _headCell.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [view addSubview:_headCell];
    self.tableView.tableHeaderView = view;
}
- (void)backAction
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)careatePickerView{
    
    NSString *urlstr = [NSString stringWithFormat:@"http://c.simpragma.com:18080/internserver/Home/Common/get_district_list?secret_key=C8AEF3FD988AF5F6"];
    [self.request QZRequest_POST:urlstr parameters:nil tagNSString:@"get_district_list" stopRequest:YES isSerializer:NO isCache:YES];
    
    _pickerView = [[UIPickerView alloc] init];
    
    _stateArr = [[NSMutableArray alloc]init];
    for (int i = 0; i < 3; i ++) {
        
        [_stateArr addObject:@(0)];
    }
    
    _pickerView.delegate = self;
    
    _pickerView.dataSource = self;
    
    _pickerView.backgroundColor = [UIColor whiteColor];
    
    //    [self.view addSubview:_pickerView];
    
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    if (_pickerArray.count > 0) {
        return 2;
    }else{
        return 0;
    }
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    if (component == 0) {
        return _pickerArray.count;
    }else{
        return _selectPickweArray.count;
    }
    
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component == 0) {
        CityModel *model = _pickerArray[row];
        return model.name;
    }else{
        CityModel *model = _selectPickweArray[row];
        return  model.name;
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    
    if (component == 0) {
        CityModel *model = _pickerArray[row];
        _selectPickweArray = model.district;
        discName = @"";
        cityId = -1;
        districtId = -1;
        [_pickerView reloadAllComponents];
        [_pickerView selectRow:0 inComponent:1 animated:YES];
    }
    if (component == 0) {
        CityModel *model = _pickerArray[row];
        cityName = model.name;
        cityId = model.id;
    }
    if (component == 1) {
        CityModel *model2 = _selectPickweArray[row];
        discName = model2.name;
        districtId = model2.id;
    }
    if ([cityName isEqualToString:@""]||cityName == nil) {
        CityModel *model2 = _pickerArray[0];
        cityName = model2.name;
        cityId = model2.id;
    }
    if ([discName isEqualToString:@""]) {
        if (_selectPickweArray.count == 0) {
            discName = @"";
            districtId = -1;
        }else{
            CityModel *model3 = _selectPickweArray[0];
            discName = model3.name;
            districtId = model3.id;
        }
    }
    _showStr = [NSString stringWithFormat:@"%@ %@",cityName,discName];
    [[TMCache sharedCache] setObject:cityName forKey:@"city"];
    [[TMCache sharedCache] setObject:discName forKey:@"district"];
}

#pragma mark - 推送判断
- (void)handleRemotePush
{
    if (![self isAllowedNotification]) {
        [[TMCache sharedCache] setObject:@(0) forKey:@"isPush"];
        [[TMCache sharedCache] setObject:@"0" forKey:@"isCanNotification"]; //系统推送
        // IOS8新系统需要使用新的代码咯
        if(IS_IOS8) {
            [[UIApplication sharedApplication]registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound|UIUserNotificationTypeAlert|UIUserNotificationTypeBadge) categories:nil]];
            
            [[UIApplication sharedApplication] registerForRemoteNotifications];
            
        }else{
            //这里还是原来的代码
            //注册启用push
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             (UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeBadge)];
        }
        // 推送 Required
        if (IS_IOS8) {
            //可以添加自定义categories
            [APService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                                           UIUserNotificationTypeSound |
                                                           UIUserNotificationTypeAlert)
                                               categories:nil];
        } else {
            //categories 必须为nil
            [APService registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                           UIRemoteNotificationTypeSound |
                                                           UIRemoteNotificationTypeAlert)
                                               categories:nil];
        }
    }else{
        [[TMCache sharedCache] setObject:@"1" forKey:@"isCanNotification"]; //系统推送
    }
}

- (BOOL)isAllowedNotification
{
    if (IS_IOS8) {// system is iOS8 +
        UIUserNotificationSettings *setting = [[UIApplication sharedApplication] currentUserNotificationSettings];
        if (UIUserNotificationTypeNone != setting.types) {
            return YES;
        }
    }else{// iOS7
        UIRemoteNotificationType type = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if(UIRemoteNotificationTypeNone != type)
            return YES;
    }
    return NO;
}
@end
