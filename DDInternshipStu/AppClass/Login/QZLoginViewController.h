//
//  QZLoginViewController.h
//  DDInternshipStu
//
//  Created by 何川 on 15/10/16.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import "QZBaseViewController.h"

@interface QZLoginViewController : QZBaseViewController
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UIButton *getVerificationButton;
@property (weak, nonatomic) IBOutlet UITableView *backView;
@property (weak, nonatomic) IBOutlet UILabel *logoView;
@property (weak, nonatomic) IBOutlet UITextField *phoneNum;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UILabel *pwdLabel;
@property (weak, nonatomic) IBOutlet UILabel *pnLabel;

- (IBAction)loginAction:(id)sender;
- (IBAction)registAction:(id)sender;
- (IBAction)getCodeAction:(id)sender;

@end
