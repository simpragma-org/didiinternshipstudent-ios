//
//  QZLoginViewController.m
//  DDInternshipStu
//
//  Created by 何川 on 15/10/16.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import "QZLoginViewController.h"
#import "DDHomeViewController.h"
#import "DDMenuController.h"
#import "QZLeftViewController.h"
#import "AppDelegate.h"
#import "DDRegistViewController.h"
#import "DDChangeMobileViewController.h"
#import "UIButton+timer.h"
#import "SQinvationTime.h"
#import "MLInputDodger.h"
#import "DDRegistDetailInputViewController.h"
#import "IntroView.h"
#import "APService.h"

#define LOGINURL @"index.php?s=/Home/Student/login"
#define getCodeUrl @"index.php?s=/Home/Common/get_sms_code"
#define checkCodeUrl @"index.php?s=/Home/Common/check_code"
@interface QZLoginViewController ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>

@end
//登录
@implementation QZLoginViewController
{
    UILabel *seletedLabel;
    NSString *newPhoneNum;
    NSString *_phoneCode;
    IntroView *introView;
}
- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
    self.password.text = @"";
    self.navigationController.navigationBarHidden = YES;
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    if([userDefaults objectForKey:@"FirstLoad"] == nil) {
        [userDefaults setBool:NO forKey:@"FirstLoad"];
        //显示引导页
        //        __weak ViewController *Vc = self;
        __weak typeof(QZLoginViewController) *weakSelf = self;
        
        introView = [[IntroView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds))];
        introView.delegate = self;
        introView.scrollView.delegate = self;
        introView.controlBlock = ^(id object){
            [weakSelf performExit];
        };
        [self.view addSubview:introView];
    }
}
#pragma mark 引导页
- (void)performExit{
    [self hideWithFadeOutDuration:0.5];
}
- (void)hideWithFadeOutDuration:(CGFloat)duration {
    [UIView animateWithDuration:duration animations:^{
        introView.alpha = 0;
    } completion:nil];
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView{
    CGFloat pageWidth = self.view.frame.size.width;
    int page = floor((aScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    introView.pageControl.currentPage = page;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //搭建UI
    [self makeUI];
    //缓存登录
    [self checkAsstoken];
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hiddenKeyBoard)];
    [self.view addGestureRecognizer:tap];
}
- (void)hiddenKeyBoard{
    [self.phoneNum resignFirstResponder];
    [self.password resignFirstResponder];
}
-(void)checkAsstoken{
    //如果有uid直接进入主页
    if ([DDUserInfo uid]) {
        [self loginToHome];
    }
}
//创建UI
-(void)makeUI{
    self.navigationController.navigationBarHidden = YES;
    self.logoView.text = [NSString stringWithFormat:NSLocalizedString(@"Didi Internship", nil)];
    self.pnLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Enter your phone number", nil)];
    self.pwdLabel.text = [NSString stringWithFormat:NSLocalizedString(@"please enter verification code", nil)];
    [self.getVerificationButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"get verification code", nil)] forState:UIControlStateNormal];
    [self.loginBtn setTitle:[NSString stringWithFormat:NSLocalizedString(@"log in", nil)] forState:UIControlStateNormal];
    [self.registerButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"register", nil)] forState:UIControlStateNormal];
    
    NSDictionary *attributeDic = @{CornerRadius:@(_phoneNum.frame.size.height/2.0),BorderColor:COLOR_DEFAULT,BorderWidth:@(0.1)};
    [_phoneNum RoundedLayerWithAttributeDic:attributeDic];
    [_password RoundedLayerWithAttributeDic:attributeDic];
    //    [_loginBtn RoundedLayerWithAttributeDic:btnAttributeDic];
    //    [_logoView RoundedLayerWithAttributeDic:logoAtttibuteDic];
    CGRect frame = _phoneNum.frame;
    frame.size.width = 18.0f;
    UIView *leftviewUp = [[UIView alloc] initWithFrame:frame];
    _phoneNum.leftViewMode = UITextFieldViewModeAlways;
    _phoneNum.leftView = leftviewUp;
    UIView *leftviewDown = [[UIView alloc] initWithFrame:frame];
    _password.leftViewMode = UITextFieldViewModeAlways;
    _password.leftView = leftviewDown;
    _phoneNum.delegate = self;
    _password.delegate = self;
    [_phoneNum addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_password addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    //表格背景图片
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:self.view.frame];
    imageView.image = [UIImage imageNamed:@"底图"];
    self.backView.backgroundView = imageView;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (IS_IOS8) {
        //距离键盘距离
        _backView.shiftHeightAsDodgeViewForMLInputDodger = 30;
        //注册
        [_backView registerAsDodgeViewForMLInputDodger];
    }
}
#pragma mark textFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == _phoneNum) {
        _pnLabel.hidden = YES;
    }else{
        _pwdLabel.hidden = YES;
    }
    return YES;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField.text.length == 0) {
        if (textField == _phoneNum) {
            _pnLabel.hidden = NO;
        }else{
            _pwdLabel.hidden = NO;
        }
    }
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)loginAction:(id)sender {
//    if (!(_phoneNum.text.length && _password.text.length)) {
//        [[iToast makeText:[NSString stringWithFormat:NSLocalizedString(@"User name or password can not be empty", nil)]]show];
//        return;
//    }
//        if (![_password.text isEqualToString:_phoneCode]) {
//            [[iToast makeText:[NSString stringWithFormat:NSLocalizedString(@"Incorrect verification code", nil)]]show];
//            return;
//        }
        if (![DDUserInfo longitude]||![DDUserInfo latitude]) {
            [[iToast makeText:[NSString stringWithFormat:NSLocalizedString(@"We did not get to the coordinates", nil)]]show];
            return;
        }
    NSString *urlstr = @"http://192.168.2.133:8080/internserver/Home/Common/check_code?secret_key=C8AEF3FD988AF5F6";
   // NSString *urlstr = [NSString stringWithFormat:@"%@%@",publicUrl,checkCodeUrl];
    NSDictionary *dic = @{@"mobile"               :_phoneNum.text,
                          @"code"                 :_password.text,
                          };
    [self.request QZRequest_POST:urlstr parameters:dic tagNSString:@"check_code" stopRequest:NO isSerializer:NO];
    
}
-(void)login{
    
    NSString *urlstr = @"http://192.168.2.133:8080/internserver/Home/Common/login?secret_key=C8AEF3FD988AF5F6";
    //NSString *urlstr = [NSString stringWithFormat:@"%@%@",publicUrl,LOGINURL];
    NSDictionary *dic = @{@"mobile"               :@"9999999999",
                          @"code"                 :_password.text,
                          @"device_number"        :[DDUserInfo device_number],
                          @"longitude"            :[DDUserInfo longitude],
                          @"latitude"             :[DDUserInfo latitude],
                          };
    [self.request QZRequest_POST:urlstr parameters:dic tagNSString:@"login" stopRequest:NO isSerializer:NO];
}
#pragma mark requestPool
-(void)request_POST_FinishValue:(id)value tagNSString:(NSString *)tag
{
    NSLog(@"value = %@ tag = %@",value,tag);
    NSString *errcode = [value objectForKey:@"status"];//errcode是__NSCFNumber类型
    if ([tag isEqualToString:@"login"]) {
        if ([errcode intValue] == 1) {
            NSDictionary *userInfo = value[@"data"];
            NSLog(@"%@",userInfo[@"mobile"]);
            //成功就缓存起来吧
            NSDictionary *dic = @{
//                                  @"uid":@"123456",
//                                  @"name":@"123456",
//                                  @"mobile":@"123456",
//                                  @"device_number":@"123456",
//                                  @"studentSex":@"123456",
//                                  @"studentRead":@"123456",
//                                  @"studentAge":@"123456",
//                                  @"studentSchool":@"123456",
//                                  @"headerImageUrl":@"123456",
                                  @"uid":userInfo[@"uid"],
                                  @"name":userInfo[@"name"],
                                  @"mobile":userInfo[@"mobile"],
                                  @"device_number":userInfo[@"device_number"],
                                  @"studentSex":userInfo[@"sex"],
                                  @"studentRead":userInfo[@"degree"],
                                  @"studentAge":userInfo[@"age"],
                                  @"studentSchool":userInfo[@"schoolname"],
                                  @"headerImageUrl":userInfo[@"avatar"],
                                  };
            NSLog(@"%@",dic);
            [DDUserInfo keepUserMessageWithDic:dic];
            if ([DDUserInfo headerImageUrl]) {
                SDWebImageManager *manager = [SDWebImageManager sharedManager];
                [manager downloadImageWithURL:[DDUserInfo headerImageUrl]
                                      options:0
                                     progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                         // progression tracking code
                                     }
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                        if (image) {
                                            // do something with image
                                            [DDUserInfo headerImage];
                                        }
                                    }];
            }
            //如果info_status为1 进入简历界面
            if ([userInfo[@"info_status"]intValue] == 1) {
                [[iToast makeText:[NSString stringWithFormat:NSLocalizedString(@"Not Filled resume Info", nil)]]show];
                DDRegistDetailInputViewController *registDetailVC = [[DDRegistDetailInputViewController alloc]init];
                registDetailVC.uid = userInfo[@"uid"];
                UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:registDetailVC];
                [self presentViewController:nav animated:YES completion:nil];
                return;
            }
            [self handleRemotePush];
            [[iToast makeText:[NSString stringWithFormat:NSLocalizedString(@"login successful", nil)]]show];
            [self loginToHome];
        }else{
            NSString *msg = [value objectForKey:@"msg"];
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"prompt" message:msg delegate:self cancelButtonTitle:@"determine" otherButtonTitles:nil];
            [alertView show];
        }
    }else if ([tag isEqualToString:@"get_sms_code"]){
        //验证码
        if ([value[@"status"] intValue] == 1) {
            //打印验证码
            NSLog(@"code:%@",value[@"data"][@"code"]);
            _phoneCode = [NSString stringWithFormat:@"%@",value[@"data"][@"code"]];
            NSLog(@"%@",_phoneCode);
        }else{
            [[iToast makeText:[NSString stringWithFormat:@"%@",value[@"msg"]]] show];
        }
    }else if ([tag isEqualToString:@"check_code"]){
        //验证手机号密码唯一性
        if ([value[@"status"] intValue] == 1) {
            //成功登录
            [self login];
        }else{
            [[iToast makeText:[NSString stringWithFormat:@"%@",value[@"msg"]]] show];
        }
    }
}
-(void)loginToHome
{
    //跳转到首页
    DDHomeViewController *homeVc = [[DDHomeViewController alloc]init];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:homeVc];
    //    [nav.navigationBar setBackgroundImage:[UIImage imageNamed:@"navgationImage"] forBarMetrics:UIBarMetricsDefault];
    nav.navigationBar.shadowImage = [UIImage imageNamed:@"分割线.png"];
    DDMenuController *ddmenu = [[DDMenuController alloc] initWithRootViewController:nav];
    QZLeftViewController* leftVC = [[QZLeftViewController alloc]init];
    [ddmenu setLeftViewController:leftVC];
    _AppDelegate.ddMenu = ddmenu;
    _AppDelegate.homeVc = homeVc;
    [self presentViewController:_AppDelegate.ddMenu animated:YES completion:nil];
}

//注册按钮
- (IBAction)registAction:(id)sender {
    DDRegistViewController *registVc = [[DDRegistViewController alloc] init];
    [self.navigationController pushViewController:registVc animated:YES];
}
//获取验证码
- (IBAction)getCodeAction:(id)sender {
   NSLog(@"%@",newPhoneNum);
    if ([newPhoneNum isEqualToString:@""] || newPhoneNum == nil) {
        [[iToast makeText:[NSString stringWithFormat:NSLocalizedString(@"No phone number", nil)]] show];
        return;
    }
    if ([self isValidateMobile:newPhoneNum]) {
        [sender addTimer];//加上倒计时
    }else{
        [[iToast makeText:[NSString stringWithFormat:NSLocalizedString(@"Wrong number", nil)]] show];
        return;
    }
    [_password becomeFirstResponder];
    //开始获取验证码
    NSString *url = @"http://c.simpragma.com:18080/internserver/Home/Common/get_sms_code?secret_key=C8AEF3FD988AF5F6&mobile=9999999999";
    
    //NSString *url = [NSString stringWithFormat:@"%@%@",publicUrl,getCodeUrl];
    NSDictionary *parmDic = @{@"mobile":newPhoneNum,@"group":@2};
    NSLog(@"%@",parmDic)
    [self.request QZRequest_POST:url parameters:parmDic tagNSString:@"get_sms_code" stopRequest:YES isSerializer:NO];
}
#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == 500) {
        newPhoneNum = [textField.text stringByAppendingString:string];
    }
    return YES;
}
- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField.tag == 500) {
        //手机号
        if ([textField.text length]>11) {
            _phoneNum.text = [textField.text substringToIndex:11];
        }
    }
    if (textField.tag == 501) {
        //验证码
        if ([textField.text length]>6) {
            _password.text = [textField.text substringToIndex:6];
        }
    }
}
#pragma mark - 推送判断
- (void)handleRemotePush
{
    if (![self isAllowedNotification]) {
        [[TMCache sharedCache] setObject:@(0) forKey:@"isPush"];
        [[TMCache sharedCache] setObject:@"0" forKey:@"isCanNotification"]; //系统推送
        // IOS8新系统需要使用新的代码咯
        if(IS_IOS8) {
            [[UIApplication sharedApplication]registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound|UIUserNotificationTypeAlert|UIUserNotificationTypeBadge) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
            
        }else{
            //这里还是原来的代码
            //注册启用push
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             (UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeBadge)];
        }
        // 推送 Required
        if (IS_IOS8) {
            //可以添加自定义categories
            [APService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                                           UIUserNotificationTypeSound |
                                                           UIUserNotificationTypeAlert)
                                               categories:nil];
        } else {
            //categories 必须为nil
            [APService registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                           UIRemoteNotificationTypeSound |
                                                           UIRemoteNotificationTypeAlert)
                                               categories:nil];
        }
    }else{
        [[TMCache sharedCache] setObject:@"1" forKey:@"isCanNotification"]; //系统推送
    }
}

- (BOOL)isAllowedNotification
{
    if (IS_IOS8) {// system is iOS8 +
        UIUserNotificationSettings *setting = [[UIApplication sharedApplication] currentUserNotificationSettings];
        if (UIUserNotificationTypeNone != setting.types) {
            return YES;
        }
    }else{// iOS7
        UIRemoteNotificationType type = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if(UIRemoteNotificationTypeNone != type)
            return YES;
    }
    return NO;
}

/*手机号码验证*/
-(BOOL) isValidateMobile:(NSString *)mobile{
    NSString *phoneRegex = @"^1[3|4|5|7|8]\\d{9}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:mobile];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 0;
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

@end
