//
//  IntroView.h
//  SwipeBegin
//
//  Created by 何川 on 15/12/10.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroView : UIView

@property (assign,nonatomic) id delegate;
@property (copy,nonatomic)void (^controlBlock)(id object);
@property (strong,nonatomic) UIPageControl *pageControl;
@property (strong,nonatomic) UIScrollView *scrollView;

@end
