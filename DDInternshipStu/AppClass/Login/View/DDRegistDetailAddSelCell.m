//
//  DDRegistDetailAddSelCell.m
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/24.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDRegistDetailAddSelCell.h"

@implementation DDRegistDetailAddSelCell

- (void)awakeFromNib {
    // Initialization code
    self.leftLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Scope", nil)];
    self.addTextField.placeholder = [NSString stringWithFormat:NSLocalizedString(@"enter scope", nil)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
