//
//  DDRegistDetailTopCell.m
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/19.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDRegistDetailTopCell.h"

@implementation DDRegistDetailTopCell

- (void)awakeFromNib {
    // Initialization code
    self.nameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"name", nil)];
    self.genderLabel.text = [NSString stringWithFormat:NSLocalizedString(@"gender", nil)];
    self.readingLabel.text = [NSString stringWithFormat:NSLocalizedString(@"reading", nil)];
    self.ageLabel.text = [NSString stringWithFormat:NSLocalizedString(@"age", nil)];
    self.schoolLabel.text = [NSString stringWithFormat:NSLocalizedString(@"school", nil)];
    self.nameTextField.placeholder = [NSString stringWithFormat:NSLocalizedString(@"enter name", nil)];
    self.ageTextField.placeholder = [NSString stringWithFormat:NSLocalizedString(@"enter age", nil)];
    self.schoolTextField.placeholder = [NSString stringWithFormat:NSLocalizedString(@"enter school", nil)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
