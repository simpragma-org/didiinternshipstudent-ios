//
//  DDRegistDetailAddSelCell.h
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/24.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDRegistDetailAddSelCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UITextField *addTextField;

//遮挡的view
@property (weak, nonatomic) IBOutlet UIView *shaView;

@end
