//
//  DDRegistDetailHeadView.m
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/19.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDRegistDetailHeadView.h"

@implementation DDRegistDetailHeadView

- (void)awakeFromNib {
    // Initialization code
    self.headerTitleLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Tip", nil)];
    self.leftLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Personal Information", nil)];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
