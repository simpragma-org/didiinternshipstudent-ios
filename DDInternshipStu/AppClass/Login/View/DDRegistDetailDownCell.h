//
//  DDRegistDetailDownCell.h
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/19.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDRegistDetailDownCell : UITableViewCell
/** 标题 */
@property (weak, nonatomic) IBOutlet UILabel *titleName;

/** 虚框内容 */
@property (weak, nonatomic) IBOutlet UILabel *remandLabel;


@property (weak, nonatomic) IBOutlet UITextView *textInput;
@end
