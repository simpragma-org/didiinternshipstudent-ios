//
//  DDRegistDetailDownCell.m
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/19.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDRegistDetailDownCell.h"
#import "UIView+nice.h"
@implementation DDRegistDetailDownCell

- (void)awakeFromNib {
    [self.textInput RoundedLayerWithCornerRadius:5 andBorderColor:[UIColor colorWithHexString:@"#EBEBEB"] andBorderWidth:1];
    _titleName.text = NSLocalizedString(@"ScopeOfWorkKey", @"");
    //工作范围
    //Scope of work
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
