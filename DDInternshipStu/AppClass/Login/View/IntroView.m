//
//  IntroView.m
//  SwipeBegin
//
//  Created by 何川 on 15/12/10.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "IntroView.h"

@implementation IntroView

@synthesize scrollView;
@synthesize pageControl;

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        scrollView = [[UIScrollView alloc] initWithFrame:frame];
        scrollView.backgroundColor = [UIColor whiteColor];
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.showsVerticalScrollIndicator = NO;
        scrollView.delegate = self.delegate;
        scrollView.contentSize = CGSizeMake(WIDTH*2, screenHeight);
        scrollView.bounces = NO;
        scrollView.pagingEnabled = YES;
        for (int i=0; i<2; i++) {
            UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i*WIDTH, 0, WIDTH, screenHeight)];
            imageView.contentMode = UIViewContentModeScaleToFill;
            imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"q-%d.png",i+1]];
            [scrollView addSubview:imageView];
            if (i == 1) {
                UIButton* start = [UIButton buttonWithType:UIButtonTypeCustom];
                [start setBackgroundColor:[UIColor whiteColor]];
                start.frame = CGRectMake(WIDTH+WIDTH/5.0+5, HEIGHT/4.0*3-20, WIDTH/5.0*3-10, 44);
                start.alpha = 1;
                start.layer.cornerRadius = 7;
                start.layer.borderWidth = 0;
                [start setTitleColor:COLOR_DEFAULT forState:UIControlStateNormal];
                [start addTarget:self action:@selector(closeView:) forControlEvents:UIControlEventTouchUpInside];
                [start setTitle:[NSString stringWithFormat:NSLocalizedString(@"Go ahead", nil)] forState:UIControlStateNormal];
                [scrollView addSubview:start];
            }
        }
        [self addSubview:scrollView];
        pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake((WIDTH-52)/2, HEIGHT-45, 52, 36)];
        pageControl.numberOfPages = 2;
        pageControl.currentPage = 0;
        pageControl.currentPageIndicatorTintColor = [UIColor colorWithHexString:@"#06D0E0"];
        pageControl.pageIndicatorTintColor = [UIColor colorWithHexString:@"#F0F0F0"];
        [pageControl setHidden:NO];
        [self addSubview:pageControl];
    }
    return self;
}
- (void)closeView:(UIButton*)btn
{
    if (_controlBlock) {
        self.controlBlock(pageControl);
    }
}

@end

