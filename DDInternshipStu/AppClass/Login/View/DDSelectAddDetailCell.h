//
//  DDSelectAddDetailCell.h
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/21.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDSelectAddDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
/** 选中箭头 */
@property (weak, nonatomic) IBOutlet UIImageView *selectedArrow;

@end
