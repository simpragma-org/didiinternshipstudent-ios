//
//  DDRegistDetailHeadView.h
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/19.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDRegistDetailHeadView : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@end
