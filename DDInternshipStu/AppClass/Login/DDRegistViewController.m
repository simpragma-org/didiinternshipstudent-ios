//
//  DDRegistViewController.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/17.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDRegistViewController.h"
#import "DDRegistDetailViewController.h"
#import "UIButton+timer.h"
#import "QZBottomView.h"
#import "MobileCell.h"
#import "MLInputDodger.h"

#define check_code @"index.php?s=/Home/Common/check_code"
#define getCodeUrl @"index.php?s=/Home/Common/get_sms_code"
@interface DDRegistViewController ()<UITextFieldDelegate>
{
    NSString *newPhoneNum;//新手机号
    NSString *inputPhoneNum;//输入的手机号
    NSString *phoneCode;//验证码
    NSString *tempPhoneNum;//临时保存的手机号，用于突发情况
    NSString *inputPhoneCode;//输入的验证码
}
@end

@implementation DDRegistViewController

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
    phoneCode = @"234565";//NEED TO REMOVE IT AFTER BYPASSING TO NEXT PAGE WHILE FINAL COMMIT
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = [NSString stringWithFormat:NSLocalizedString(@"register", nil)];
    [self addBtnOnNavWithTitle:nil andImageName:@"返回" andTarget:self andAction:@selector(backAction) andFrame:CGRectMake(0, 0, 18, 15) andDirection:LEFT];
    
    
    [self createTableViewWithPlainStyle:NO andSeparatorStyleNone:NO andBackGroudImageName:nil];
    
    [self registCellWithNib:NSStringFromClass([MobileCell class]) addIdentifier:NSStringFromClass([MobileCell class]) addHeight:44];
    //创建脚视图
    [self createFooterView];
    
    [self loadData];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UITapGestureRecognizer * touchBegin = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(toucheBegin:)];
    [self.tableView addGestureRecognizer:touchBegin];
    
}
- (void)toucheBegin:(UITapGestureRecognizer *)tap
{
    for (int i = 500; i<502; i++) {
        UITextField *textField = [self.view viewWithTag:i];
        [textField resignFirstResponder];
    }
}
#pragma mark - 数据
-(void)loadData
{
    [self.tableSource addObject:@[[NSString stringWithFormat:NSLocalizedString(@"mobile number", nil)],[NSString stringWithFormat:NSLocalizedString(@"verification code", nil)]]];
    [self.tableView reloadData];
}
//提交按钮事件
- (void)bottomBtnAction
{
    UITextField *textField;
    for (int i = 500; i<502; i++) {
        textField = [self.tableView viewWithTag:i];
        if (i == 500) {
            if (![self isValidateMobile:textField.text]) {
                [[iToast makeText:[NSString stringWithFormat:NSLocalizedString(@"Incomplete Input", nil)]] show];
                return;
            }else{
                inputPhoneNum = textField.text;
            }
        }else{
            if ([textField.text isEqualToString:@""] || textField.text == nil) {
                [[iToast makeText:[NSString stringWithFormat:NSLocalizedString(@"Empty verification", nil)]] show];
                return;
            }else{
                inputPhoneCode = textField.text;
            }
        }
        [textField resignFirstResponder];
    }

    NSLog(@"newPhoneNum:%@,phoneCode:%@",newPhoneNum,phoneCode);
    
    //验证
    NSString *url = [NSString stringWithFormat:@"%@%@",publicUrl,check_code];
    NSDictionary *parmDic = @{@"mobile":inputPhoneNum,
                              @"code":inputPhoneCode,
                              @"group":@(2)
                              };
    [self.request QZRequest_POST:url parameters:parmDic tagNSString:@"check_code" stopRequest:NO isSerializer:NO];
}
- (void)loadCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    MobileCell *mobCell = (MobileCell *)cell;
    mobCell.titleName.text = self.tableSource[indexPath.section][indexPath.row];
    mobCell.inputField.delegate = self;
    mobCell.titleName.textColor = COLOR_FONT_REGIST;
    mobCell.inputField.returnKeyType = UIReturnKeyDone;
    mobCell.yesBtn.hidden = YES;
    mobCell.noBtn.hidden = YES;
    mobCell.inputField.tag = indexPath.row + 500;
     [mobCell.inputField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    if (indexPath.row == 0) {
        mobCell.inputField.keyboardType = UIKeyboardTypePhonePad;
        mobCell.inputField.placeholder = [NSString stringWithFormat:NSLocalizedString(@"Enter your phone number", nil)];
    }else if (indexPath.row == 1) {
        mobCell.inputField.keyboardType = UIKeyboardTypeNumberPad;
        mobCell.inputFieldRight.constant = 105;
        mobCell.inputField.placeholder = [NSString stringWithFormat:NSLocalizedString(@"Enter confirmation code", nil)];
        UIButton *getCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        getCodeBtn.frame = CGRectMake(WIDTH-96, 7, 82, 30);
        [getCodeBtn setBackgroundImage:[UIImage imageNamed:@"注册背景"] forState:UIControlStateNormal];
        [getCodeBtn setTitle:[NSString stringWithFormat:NSLocalizedString(@"get verification code", nil)] forState:UIControlStateNormal];
        [getCodeBtn setTitleColor:COLOR_DEFAULT forState:UIControlStateNormal];
        getCodeBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [getCodeBtn addTarget:self action:@selector(getBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [mobCell addSubview:getCodeBtn];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 20;
    }else{
        return 0.000001;
    }
}
//创建脚视图
-(void)createFooterView
{
    NSString *subTitle = [NSString stringWithFormat:NSLocalizedString(@"Next", nil)];
    QZBottomView *bottomView = [[[NSBundle mainBundle]loadNibNamed:@"QZBottomView" owner:self options:nil]lastObject];
    bottomView.frame = CGRectMake(0, 0, WIDTH, 102);
    [bottomView.bottomBtn setTitle:subTitle forState:UIControlStateNormal];
    [bottomView.bottomBtn addTarget:self action:@selector(bottomBtnAction) forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableFooterView = bottomView;
}
#pragma mark - 获取验证码
-(void)getBtnAction:(UIButton *)btn
{
    UITextField *numTextField = [self.view viewWithTag:501];
    if (![numTextField isFirstResponder]) {
        [numTextField becomeFirstResponder];
    }
    if ([newPhoneNum isEqualToString:@""] || newPhoneNum == nil) {
        [[iToast makeText:[NSString stringWithFormat:NSLocalizedString(@"No phone number", nil)]] show];
        return;
    }
    if ([self isValidateMobile:newPhoneNum]) {
        [btn addTimer];//加上倒计时
    }else{
        [[iToast makeText:[NSString stringWithFormat:NSLocalizedString(@"Wrong number", nil)]] show];
        return;
    }
    //开始获取验证码
    NSString *url = [NSString stringWithFormat:@"http://c.simpragma.com:18080/internserver/Home/Common/get_sms_code?secret_key=C8AEF3FD988AF5F6&mobile=9999999999"];
    NSDictionary *parmDic = @{@"mobile":newPhoneNum};
    [self.request QZRequest_POST:url parameters:parmDic tagNSString:@"get_sms_code" stopRequest:YES isSerializer:NO];
//    短信验证成为第一响应者
    NSArray *viewArr = [[btn superview]subviews];
    for (UIView *midView in viewArr) {
        for (UIView *subview in [midView subviews]) {
            if ([[subview class] isSubclassOfClass:[UITextField class]]) {
                [(UITextField*)subview becomeFirstResponder];
            }
        }
    }
}
#pragma mark ------<网络请求>
- (void)request_POST_FinishValue:(id)value tagNSString:(NSString *)tag
{
    NSLog(@"value:%@ tag:%@",value,tag);
    if ([tag isEqualToString:@"get_sms_code"]) { //验证码
        if ([value[@"status"] intValue] == 1) {
            //打印验证码
            NSLog(@"code:%@",value[@"data"][@"code"]);
            phoneCode = [NSString stringWithFormat:@"%@",value[@"data"][@"code"]];
        }else{
            [[iToast makeText:[NSString stringWithFormat:@"%@",value[@"msg"]]] show];
        }
    }else if([tag isEqualToString:@"check_code"]){
        DDRegistDetailViewController *registDetailVc = [[DDRegistDetailViewController alloc] init];
                    registDetailVc.phoneInfo = @{@"phoneNumber":newPhoneNum,
                                                 @"phoneCode":phoneCode};
                    [self.navigationController pushViewController:registDetailVc animated:YES];//uncomment from next line and delete all the above lines while final push.For bypass purpose
//        if ([value[@"status"] intValue] == 1) {
//            DDRegistDetailViewController *registDetailVc = [[DDRegistDetailViewController alloc] init];
//            registDetailVc.phoneInfo = @{@"phoneNumber":newPhoneNum,
//                                         @"phoneCode":phoneCode};
//            [self.navigationController pushViewController:registDetailVc animated:YES];
//        }else{
//            [[iToast makeText:[NSString stringWithFormat:@"%@",value[@"msg"]]] show];
//        }
    }
    
}
//键盘方法
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //距离键盘距离
    self.tableView.shiftHeightAsDodgeViewForMLInputDodger = 50;
    //注册
    [self.tableView registerAsDodgeViewForMLInputDodger];
}
#pragma mark - UITextFieldDelegate
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    if (textField.tag == 500) {
//        newPhoneNum = [textField.text stringByAppendingString:string];
//    }
//    return YES;
//}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag == 500) {
        newPhoneNum = textField.text;
    }
}
- (void)textFieldDidChange:(UITextField *)textField
{
    switch (textField.tag) {
        case 500:
        {
            if ([textField.text length]>11) {
                textField.text = [textField.text substringToIndex:11];
            }
        }
            break;
        case 501:
        {
            if ([textField.text length]>6) {
                textField.text = [textField.text substringToIndex:6];
            }
        }
            break;
        default:
            break;
        }
}
/*手机号码验证*/
-(BOOL) isValidateMobile:(NSString *)mobile
{
    NSString *phoneRegex = @"^1[3|4|5|7|8]\\d{9}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:mobile];
}
- (void)backAction
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
