//
//  DDRegistDetailInputViewController.m
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/24.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDRegistDetailInputViewController.h"
#import "DDRegistDetailDownCell.h"
#import "QZBottomView.h"
#import "DDRegistDetailHeadView.h"
#import "MLInputDodger.h"
#import "QZLoginViewController.h"
#import "DDMenuController.h"
#import "DDHomeViewController.h"
#import "QZLeftViewController.h"
#import "AppDelegate.h"
#define RESUME @"index.php?s=/Home/Student/set_resume"

@interface DDRegistDetailInputViewController ()<UITextViewDelegate,UITextFieldDelegate>
{
    DDRegistDetailDownCell *_inputCell;
    /** 自我介绍 */
    NSString *_introduction;
    /** 专业 */
    NSString *_major;
    /** 自我介绍 */
    NSString *_intern;
    /** 技能 */
    NSString *_skill;
}
@end

@implementation DDRegistDetailInputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"个人简历";
    [self addBtnOnNavWithTitle:nil andImageName:@"返回" andTarget:self andAction:@selector(backAction) andFrame:CGRectMake(0, 0, 18, 15) andDirection:LEFT];
    [self createTableViewWithPlainStyle:NO andSeparatorStyleNone:NO andBackGroudImageName:nil];
    for (int i = 0; i < 4; i ++) {
        [self registCellWithNib:@"DDRegistDetailDownCell" addIdentifier:[NSString stringWithFormat:@"cell%ld",(long)i] addHeight:110];
    }
    self.tableView.separatorStyle  = UITableViewCellSeparatorStyleNone;
    self.tableView.bounces = NO;
    self.tableView.frame = CGRectMake(0, 85, WIDTH, HEIGHT - 85 - 64);
    self.tableView.backgroundColor = [UIColor whiteColor];
    _introduction = @"";
    _major = @"";
    _intern = @"";
    _skill = @"";
    //加载头视图
    [self createTableHeaderView];
    //创建脚视图
    [self createFooterView];
    //加载数据
    [self loadData];
}
-(void)loadData{
    [self.tableSource addObject:@[@"自我介绍",@"专业",@"社团/实习经历",@"技能/奖项"]];
}
#pragma mark ------<tableView>
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 110;

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{   NSString *CellIdentifier = [NSString stringWithFormat:@"cell%ld",(long)indexPath.row];
    _inputCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(!_inputCell){
        _inputCell = [[DDRegistDetailDownCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    _inputCell.selectionStyle = UITableViewCellSelectionStyleNone;
    _inputCell.textInput.delegate = self;
    _inputCell.textInput.tag = 3000 + indexPath.row;
    _inputCell.titleName.text= self.tableSource[indexPath.section][indexPath.row];
    _inputCell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row == 0) {
        _inputCell.remandLabel.text = @"请输入您的自我介绍";
    }else if (indexPath.row == 1){
        _inputCell.remandLabel.text = @"请输入您的专业（如：软件工程）";
    }else if (indexPath.row == 2){
        _inputCell.remandLabel.text = @"请输入您的社团经历或者实习经历";
    }else if (indexPath.row == 3){
        _inputCell.remandLabel.text = @"请输入您的专业技能以及获得的奖励";
    }

    return _inputCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.000001;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.000001;
}
#pragma mark - textView delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    UIView *fatherView = [textView superview];
    DDRegistDetailDownCell *cell = (DDRegistDetailDownCell *)[fatherView superview];
    cell.remandLabel.hidden = YES;
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@""] ) {
        UIView *fatherView = [textView superview];
        DDRegistDetailDownCell *cell = (DDRegistDetailDownCell *)[fatherView superview];
        cell.remandLabel.hidden = NO;
        return YES;
    }else{
        if (textView.tag == 3000) {
            _introduction = textView.text;
        }else if(textView.tag == 3001){
            _major = textView.text;
        }else if(textView.tag == 3002){
            _intern = textView.text;
        }else if(textView.tag == 3003){
            _skill = textView.text;
        }
        return YES;
    }
    return YES;
}
//键盘方法
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //距离键盘距离
    self.tableView.shiftHeightAsDodgeViewForMLInputDodger = 50;
    //注册
    [self.tableView registerAsDodgeViewForMLInputDodger];
}
-(void)createFooterView{
    QZBottomView *bottomView = [[[NSBundle mainBundle]loadNibNamed:@"QZBottomView" owner:self options:nil]lastObject];
    bottomView.frame = CGRectMake(0, 0, WIDTH, 102);
    [bottomView.bottomBtn setTitle:@"提交" forState:UIControlStateNormal];
    [bottomView.bottomBtn addTarget:self action:@selector(bottomBtnAction) forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableFooterView = bottomView;
}
//提交按钮方法
-(void)bottomBtnAction{
    if ([_introduction isEqualToString:@""]||[_major isEqualToString:@""]||[_intern isEqualToString:@""]||[_skill isEqualToString:@""]) {
        [[iToast makeText:NSLocalizedString(@"Incomplete Biography", @"")]show];//Incomplete Biography
        return;
    }
    NSDictionary *dic = @{@"student_id":self.uid,
                          @"introduction":_introduction,
                          @"major":_major,
                          @"intern":_intern,
                          @"skill":_skill};
    NSLog(@"dadadadd%@",dic);
    NSString *url = [NSString stringWithFormat:@"%@%@",publicUrl,RESUME];
    [self.request QZRequest_POST:url parameters:dic tagNSString:@"resume" stopRequest:YES isSerializer:NO isCache:NO];  
}
#pragma mark requestPool
-(void)request_POST_FinishValue:(id)value tagNSString:(NSString *)tag
{
    NSLog(@"value%@,tag:%@",value,tag);
    NSString *errcode = [value objectForKey:@"status"];
    if ([errcode intValue] == 1) {
        [[iToast makeText:NSLocalizedString(@"Resume fill in success", @"")]show];
        [[TMCache sharedCache] setObject:self.uid forKey:@"uid"];
        [self loginToHome];
    }else{
        [[iToast makeText:[NSString stringWithFormat:@"%@",value[@"msg"]]] show];
    }
}
-(void)loginToHome
{
    //跳转到首页
    DDHomeViewController *homeVc = [[DDHomeViewController alloc]init];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:homeVc];
    //    [nav.navigationBar setBackgroundImage:[UIImage imageNamed:@"navgationImage"] forBarMetrics:UIBarMetricsDefault];
    nav.navigationBar.shadowImage = [UIImage imageNamed:@"分割线.png"];
    DDMenuController *ddmenu = [[DDMenuController alloc] initWithRootViewController:nav];
    QZLeftViewController* leftVC = [[QZLeftViewController alloc]init];
    [ddmenu setLeftViewController:leftVC];
    _AppDelegate.ddMenu = ddmenu;
    _AppDelegate.homeVc = homeVc;
    [self presentViewController:_AppDelegate.ddMenu animated:YES completion:nil];
}
//头视图
-(void)createTableHeaderView{
    DDRegistDetailHeadView *headView = [[[NSBundle mainBundle]loadNibNamed:@"DDRegistDetailHeadView" owner:self options:nil]lastObject];
    headView.leftLabel.text = @"请填写实习简历";
    headView.frame = CGRectMake(0, 0, WIDTH,85);
    [self.view addSubview:headView];
}
- (void)backAction
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
