//
//  DDinviteFriendViewController.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/16.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDinviteFriendViewController.h"
#import "UMSocial.h"
#import "AppDelegate.h"
#import "QRCodeGenerator.h"
#import "InviteHeadCell.h"
#import "InviteMiddleCell.h"
#import "InviteButtomCell.h"
#import "InviteShareCell.h"
#import "MLInputDodger.h"
#import "DDBase64.h"
#import "WeiboSDK.h"
#import "AppDelegate.h"
#import "PublicClass.h"

#define sms_invite @"index.php?s=/Home/Student/sms_invite"
#define GetWEBInviteQR @"http://sx.jushenqi.com//QS_" //二维码内-网址
#define ShareInviteURL @"http://sx.jushenqi.com//InVh" //分享的网址
#define kRedirectURI   @"http://www.sina.com"
@interface DDinviteFriendViewController ()<UMSocialUIDelegate,UITextFieldDelegate,UIActionSheetDelegate>

@end

@implementation DDinviteFriendViewController
{
    NSString *newPhoneNum; // 邀请的手机号
    NSString *inviteCode;  // 用户对应邀请码
    UIActionSheet *_sheet;
    InviteMiddleCell *middleCell;
    InviteHeadCell *headCell;
    NSAttributedString *_contentStr; //文本内容
    NSString *_base64UID;   // base64加密的UID
    NSString *_shareURL;    // 分享的链接
    NSString *_shareString; // 分享的文字
    NSString *_shareTitle;  // 分享的标题
    UIImage  *_shareImage;  // 分享的图片
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _base64UID = [DDBase64 base64StringFromText:[DDUserInfo uid]];
        _shareURL  = [NSString stringWithFormat:@"%@%@",ShareInviteURL,_base64UID];
        _shareTitle  = NSLocalizedString(@"opportunitiesKey", @"");
        //滴滴一下，实习机会马上到！
        //Drops it, internship opportunities immediately to !
        
        _shareString = NSLocalizedString(@"ShareStringKey", @"");
        //Yes, this is your long-awaited pieces of practice, we are not kidding ! \ n no longer need to squeeze his head to look for opportunities to break , waiting for the big brother to invite you to drink tea ! \ n never fun practice mode , it relied on four years of college!
        //是的，这就是你期盼已久的滴滴实习，我们绝对不是在开玩笑！\n不再需要挤破脑袋去找机会，静待大佬邀你喝下午茶！\n从来没有过的好玩实习模式，大学四年就靠它啦！
        
        _shareImage = [UIImage imageNamed:@"icon"];
    }
    return self;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //距离键盘距离
    self.tableView.shiftHeightAsDodgeViewForMLInputDodger = 50;
    //注册
    [self.tableView registerAsDodgeViewForMLInputDodger];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"FriendKey", @"");
    //邀请好友
    //Invite a friend
    
    [self addBtnOnNavWithTitle:nil andImageName:@"返回" andTarget:self andAction:@selector(backAction) andFrame:CGRectMake(0, 0, 18, 15) andDirection:LEFT];
    
    [self createTableViewWithPlainStyle:NO andSeparatorStyleNone:NO andBackGroudImageName:nil];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([InviteHeadCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([InviteHeadCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([InviteMiddleCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([InviteMiddleCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([InviteShareCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([InviteShareCell class])];

    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([InviteButtomCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([InviteButtomCell class])];

    
    [self loadData];
    
    [self getInviteCode];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - 二维码
//获取二维码推荐码---网络链接
- (void)getInviteCode
{
    if ([DDUserInfo codeImage] == nil) {
        //存储二维码图片
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",GetWEBInviteQR,[DDUserInfo uid]]]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    // do something with image
                                    [[TMCache sharedCache] setObject:image forKey:@"codeImage"];
                                }
                            }];
    }
    
    //邀请好友奖励规则
    NSDictionary *parmArticleDic = @{@"id":@1};
    //NSString *url = [NSString stringWithFormat:@"%@%@",publicUrl,getArticleUrl];
    NSString *url = [NSString stringWithFormat:@"http://c.simpragma.com:18080/internserver/Home/Common/get_article?secret_key=C8AEF3FD988AF5F6"];
    [self.request QZRequest_POST:url parameters:parmArticleDic tagNSString:@"get_article" stopRequest:YES isSerializer:NO isCache:YES];
}
- (void)loadData
{
    [self.tableSource addObject:@[@""]];
    [self.tableSource addObject:@[@""]];
    [self.tableSource addObject:@[@""]];
//    [self.tableSource addObject:@[@""]];
    [self.tableView reloadData];
}

#pragma mark - UITableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            //短信推荐
            headCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([InviteHeadCell class]) forIndexPath:indexPath];
            headCell.textField.delegate = self;
            headCell.textField.tag = 500;
            [headCell.submitBtn addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
            headCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return headCell;
        }
            break;
        case 1:
        {
            //社交分享
            InviteShareCell *shareCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([InviteShareCell class]) forIndexPath:indexPath];
            for (UIButton *btn in shareCell.shareBtns) {
                [btn addTarget:self action:@selector(ShareInformation:) forControlEvents:UIControlEventTouchUpInside];
            }
            shareCell.selectionStyle = UITableViewCellSelectionStyleNone;
            return shareCell;
        }
            break;
        case 2:
        {
            //扫码推荐
            middleCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([InviteMiddleCell class]) forIndexPath:indexPath];
            middleCell.selectionStyle = UITableViewCellSelectionStyleNone;
            if ([DDUserInfo codeImage] != nil) {
                [middleCell.QRCodeImage setImage:[DDUserInfo codeImage]];
            }else{
                [middleCell.QRCodeImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",GetWEBInviteQR,[DDUserInfo uid]]]];
            }
            return middleCell;
        }
            break;
//        case 3:
//        {
//            //邀请好友奖励规则
//            InviteButtomCell *bottomCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([InviteButtomCell class]) forIndexPath:indexPath];
//            [bottomCell.actionBtn addTarget:self action:@selector(inviteRuleAction:) forControlEvents:UIControlEventTouchUpInside];
//            bottomCell.selectionStyle = UITableViewCellSelectionStyleNone;
//            return bottomCell;
//        }
//            break;
            
        default:
            return nil;
            break;
    }

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            //短信推荐
            return 130;
        }
            break;
        case 1:
        {
            //社交分享
            return 110;
            
        }
            break;
        case 2:
        {
            //扫码推荐
            return 170;
        }
            break;
        case 3:
        {
            //邀请好友规则
            return 44;
        }
            break;
            
        default:
            return 0.000001;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 3) {
        return 20;
    }else{
        return 0.00001;
    }
}

#pragma mark - 所有点击事件
//提交手机号
- (void)submitAction:(UIButton *)sender
{
    if ([self isValidateMobile:newPhoneNum]) {
        //提交手机号发送网络请求
    }else{
        if ([newPhoneNum isEqualToString:@""]) {
            [[iToast makeText:NSLocalizedString(@"EnterPhoneNumberkey", @"")] show];
            //Please enter the phone number
            //请先输入手机号
            return;
        }else{
            [[iToast makeText:NSLocalizedString(@"IncorrectKey", @"")] show];
            //Enter your phone number is incorrect
            //您输入的手机号不正确
            return;
        }
    }
    NSString *url = [NSString stringWithFormat:@"%@%@",publicUrl,sms_invite];
    NSDictionary *dic = @{@"uid":[DDUserInfo uid],
                        @"mobile":newPhoneNum};
    [self.request QZRequest_POST:url parameters:dic tagNSString:@"sms_invite" stopRequest:YES isSerializer:NO];
    
}
#pragma mark ------<网络请求>
-(void)request_POST_FinishValue:(id)value tagNSString:(NSString *)tag
{
    
    NSLog(@"value%@,tag:%@",value,tag);
    if ([tag isEqualToString:@"sms_invite"]) {
        NSString *errcode = [value objectForKey:@"status"];
        if ([errcode intValue] == 1) {
            
            [[iToast makeText:@"SentKey"]show];
            //SMS sent successfully
            //短信发送成功
            headCell.textField.text = @"";
            
        }else{
            NSString *msg = [value objectForKey:@"msg"];
            [[iToast makeText:msg]show];
        }
    }
    if ([tag isEqualToString:@"get_article"]) {
        if ([value[@"status"] intValue] == 1) {
            _contentStr = [PublicClass handleArticleWithAttributeStringFromHTML:value[@"data"][@"content"]];
        }
    }
}
- (void)ShareInformation:(UIButton*)sender
{
    NSString* str;
    switch (sender.tag) {
            
        case 1:
        {
            str= NSLocalizedString(@"MicroKey", @"");
            //Micro- letter
            //微信
            [self shareWeChat];
        }
            break;
        case 2:
        {
            str = NSLocalizedString(@"CircleKey", @"");
            //Circle of friends
            //朋友圈
            [self shareFriend];
        }
            break;
        case 3:
        {
            str = NSLocalizedString(@"QQspaceKey", @"");
            //QQ space
            //QQ空间
            [self shareQzone];
        }
            break;
        case 4:
        {
            str = NSLocalizedString(@"WeiboKey", @"");
            [self sharSina];
            //Weibo
            //微博
        }
            break;
        default:
            break;
    }
    
}
#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == 500) {
        newPhoneNum = [textField.text stringByAppendingString:string];
    }
    return YES;
}

/*手机号码验证*/
-(BOOL)isValidateMobile:(NSString *)mobile
{
    NSString *phoneRegex = @"^1[3|4|5|7|8]\\d{9}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:mobile];
}

#pragma mark - 分享方法
// 微信
- (void)shareWeChat {

    [UMSocialData defaultData].extConfig.wechatSessionData.url = _shareURL;
    //使用UMShareToWechatSession,UMShareToWechatTimeline 分别代表微信好友、微信朋友圈
    [UMSocialData defaultData].extConfig.wechatSessionData.title = _shareTitle;
    [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToWechatSession] content:_shareString image:_shareImage location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response){
        if (response.responseCode == UMSResponseCodeSuccess) {
            [[iToast makeText:NSLocalizedString(@"shareSuccessKey", @"")] show];
            //分享成功
            //Share Success
        }
    }];
}
#pragma mark 朋友圈
// 朋友圈
- (void)shareFriend {
    //UMShareToWechatTimeline 代表分享到微信朋友圈
    [UMSocialData defaultData].extConfig.wechatTimelineData.url = _shareURL;
    
    [UMSocialData defaultData].extConfig.wechatTimelineData.title = _shareTitle;
    [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToWechatTimeline] content:_shareString image:_shareImage location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *response){
        if (response.responseCode == UMSResponseCodeSuccess) {
            [[iToast makeText:NSLocalizedString(@"shareSuccessKey", @"")]show];
        }
    }];
}
//qq空间
- (void)shareQzone {
    //判断是否授权
    //注意：分享到微信好友、微信朋友圈、微信收藏、QQ空间、QQ好友、来往好友、来往朋友圈、易信好友、易信朋友圈、Facebook、Twitter、Instagram等平台需要参考各自的集成方法
    [UMSocialAccountManager isOauthAndTokenNotExpired:UMShareToQzone];
    
    [UMSocialData defaultData].extConfig.qzoneData.url = _shareURL;
    UMSocialAccountEntity *qzoneAccount = [[UMSocialAccountEntity alloc] initWithPlatformName:UMShareToQzone];
    qzoneAccount.usid = @"your usid";
    qzoneAccount.accessToken = @"your accesstoken";
    //同步用户信息
    [UMSocialAccountManager postSnsAccount:qzoneAccount completion:^(UMSocialResponseEntity *response){
        if (response.responseCode == UMSResponseCodeSuccess) {
            //在本地缓存设置得到的账户信息
            [UMSocialAccountManager setSnsAccount:qzoneAccount];
            //进入你自定义的分享内容编辑页面或者使用我们的内容编辑页面
        }}];
    [UMSocialData defaultData].extConfig.qzoneData.title = _shareTitle;
    
    [[UMSocialDataService defaultDataService]  postSNSWithTypes:@[UMShareToQzone] content:_shareString image:_shareImage location:nil urlResource:nil presentedController:self completion:^(UMSocialResponseEntity *shareResponse){
        if (shareResponse.responseCode == UMSResponseCodeSuccess) {
            [[iToast makeText:NSLocalizedString(@"shareSuccessKey", @"")] show];
        }
    }];
    [UMSocialData openLog:YES];
}
// 新浪分享
- (void)sharSina{

    AppDelegate *myDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    WBAuthorizeRequest *authRequest = [WBAuthorizeRequest request];
    authRequest.redirectURI = kRedirectURI;
    authRequest.scope = @"all";
    
    WBSendMessageToWeiboRequest *request = [WBSendMessageToWeiboRequest requestWithMessage:[self messageToShare] authInfo:authRequest access_token:myDelegate.wbtoken];
    /**
     *  userInfo 只是把你的 运行的环境信息带给服务器
     */
    request.userInfo = @{@"ShareMessageFrom": @"QZPatEduKnoViewController",
                         @"Other_Info_1": [NSNumber numberWithInt:123],
                         @"Other_Info_2": @[@"obj1", @"obj2"],
                         @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
    request.shouldOpenWeiboAppInstallPageIfNotInstalled = NO;
    
    [WeiboSDK sendRequest:request];
}
#pragma mark 新浪微博分享 点击事件
- (WBMessageObject *)messageToShare
{
    //媒体和图片只能选择一个，我们这个没有媒体  只有图片
    
    WBMessageObject *message = [WBMessageObject message];
    
    NSString *conter = [NSString stringWithFormat:@"%@:%@",_shareString,_shareURL];
    //文字
    message.text = NSLocalizedString(conter, nil);
    //图片
    WBImageObject *image = [WBImageObject object];
    
    image.imageData = UIImageJPEGRepresentation(_shareImage,1.0);
    message.imageObject = image;
    
    //媒体;
    
    return message;
}
-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response
{
    //根据`responseCode`得到发送结果,如果分享成功
    if(response.responseCode == UMSResponseCodeSuccess)
    {
        //得到分享到的微博平台名
        NSLog(@"share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"shareSuccessKey", @"") message:NSLocalizedString(@"ShareSccessMsgTitileKey", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil];
        //Thank you for sharing
        //感谢您的分享
        [alert show];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
