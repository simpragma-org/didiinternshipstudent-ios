//
//  InviteShareCell.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/17.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "InviteShareCell.h"

@implementation InviteShareCell

- (void)awakeFromNib {
    
    _titleLabel.text = NSLocalizedString(@"SocialSharingKey", @"");
    //Social sharing
   //社交分享
    _shareBtns = [[NSMutableArray alloc]init];
    CGFloat space = (WIDTH-100-160)/3;
    NSArray *imageNames = @[@"微信",@"朋友圈",@"qq空间",@"微博"];
    for (int i = 0; i < 4; i ++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(50+(40+space)*i, 50, 40, 40);
        if (i == 3) {
            btn.frame = CGRectMake(50+(40+space)*3, 50, 40, 40);
        }
        [btn setBackgroundImage:[UIImage imageNamed:imageNames[i]] forState:UIControlStateNormal];
        [_shareBtns addObject:btn];
        btn.tag = i+1;
        [self.contentView addSubview:btn];
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
