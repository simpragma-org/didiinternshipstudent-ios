//
//  InviteHeadCell.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/17.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "InviteHeadCell.h"
#import "UIView+nice.h"

@implementation InviteHeadCell

- (void)awakeFromNib {
    //短信推荐
    //SMS Recommended
    _titleLabel.text = NSLocalizedString(@"SMSRecommendedKey", @"");
    
    _textField.placeholder = NSLocalizedString(@"VerificationKey", @"");
    //请准确填写号码，方便我们核查
    // Please fill in numbers to facilitate our verification
    [_submitBtn setTitle:NSLocalizedString(@"SubmitKey", @"") forState:UIControlStateNormal];
    self.textField.layer.borderWidth    = 0.5f;
    self.textField.layer.borderColor    =[UIColor colorWithHexString:@"#ececec"].CGColor;
    self.textField.layer.cornerRadius    = 3;
    self.textField.layer.masksToBounds   = YES;CGRect frame = [self.textField frame];
    frame.size.width = 7.0f;
    UIView *leftviewPhone = [[UIView alloc] initWithFrame:frame];
    self.textField.leftViewMode = UITextFieldViewModeAlways;
    self.textField.leftView = leftviewPhone;
    self.textField.leftView.userInteractionEnabled = NO;
    
    NSDictionary *attributeDic = @{CornerRadius:@(self.submitBtn.frame.size.height/2),BorderColor:[UIColor clearColor],BorderWidth:@(0)};
    [self.submitBtn RoundedLayerWithAttributeDic:attributeDic];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
