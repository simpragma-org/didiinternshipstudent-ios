//
//  InviteMiddleCell.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/17.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "InviteMiddleCell.h"

@implementation InviteMiddleCell

- (void)awakeFromNib {
    // Initialization code
    _titleLabel.text = NSLocalizedString(@"ScanKey", @"");
    //扫码推荐
    //Recommended scan code
    _showLabel.text = NSLocalizedString(@"microLetterKey", @"");
    //被推荐好友通过微信扫码,即可注册
    //Friends recommended sweeping through micro-letter code , can be registered
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
