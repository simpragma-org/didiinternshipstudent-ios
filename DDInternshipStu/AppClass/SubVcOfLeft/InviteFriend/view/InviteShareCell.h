//
//  InviteShareCell.h
//  DDInternshipStu
//
//  Created by 何川 on 15/11/17.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteShareCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong,nonatomic) NSMutableArray *shareBtns;

@end
