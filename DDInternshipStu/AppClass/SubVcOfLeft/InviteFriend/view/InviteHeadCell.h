//
//  InviteHeadCell.h
//  DDInternshipStu
//
//  Created by 何川 on 15/11/17.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteHeadCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@end
