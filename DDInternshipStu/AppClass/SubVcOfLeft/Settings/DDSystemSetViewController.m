//
//  DDSystemSetViewController.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/16.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDSystemSetViewController.h"
#import "DDSystemSetCell.h"
#import "QZBottomView.h"
#import "QZSwitch.h"
#import "DDAdviceUsViewController.h"
#import "DDAboutUsViewController.h"
#import "DDChangeMobileViewController.h"
#import "AppDelegate.h"
#import "APService.h"
#define logoutURL @"index.php?s=/Home/Student/logout"
@interface DDSystemSetViewController ()<UIAlertViewDelegate>

@end

@implementation DDSystemSetViewController
{
    UIView *switchView;
    UIWebView *_phoneCallWebView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"SetUpKey", @"");
    
    [self addBtnOnNavWithTitle:nil andImageName:@"返回" andTarget:self andAction:@selector(backAction) andFrame:CGRectMake(0, 0, 18, 15) andDirection:LEFT];

    [self createTableViewWithPlainStyle:NO andSeparatorStyleNone:NO andBackGroudImageName:nil];

    [self registCellWithNib:@"DDSystemSetCell" addIdentifier:@"sysCell" addHeight:50];
    
    [self createFooterView];
    
    [self loadData];
}
#pragma mark - 返回、退出
- (void)backAction
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)createFooterView
{
    QZBottomView *bottomView = [[[NSBundle mainBundle]loadNibNamed:@"QZBottomView" owner:self options:nil]lastObject];
    bottomView.frame = CGRectMake(0, 0, WIDTH, 102);
    [bottomView.bottomBtn setTitle:NSLocalizedString(@"signoutKey", @"") forState:UIControlStateNormal];
    
    [bottomView.bottomBtn addTarget:self action:@selector(bottomBtnAction) forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableFooterView = bottomView;
}

//退出登录
-(void)bottomBtnAction
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"PromptKey", @"") message:NSLocalizedString(@"Are you sure you want to quit it drops internship ?", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"CancelKey", @"") otherButtonTitles:NSLocalizedString(@"OK to exit", @""), nil];
    alert.tag = 123;
//    //清除缓存
//    [DDUserInfo cleanAllUserMessage];
    [alert show];
}
- (void)request_POST_FinishValue:(id)value tagNSString:(NSString *)tag
{
    NSLog(@"value:%@,tag:%@",value,tag);
    [[iToast makeText:[NSString stringWithFormat:@"%@",value[@"msg"]]] show];
    if ([value[@"status"] intValue] == 1) {
        //退出
        [DDUserInfo cleanAllUserMessage];
        [_AppDelegate.ddMenu dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"poToRoot" object:nil];
    }else{
        [[iToast makeText:[NSString stringWithFormat:@"%@",value[@"msg"]]]show];
    }
}

#pragma mark - 数据
-(void)loadData
{
    [self.tableSource addObject:@[NSLocalizedString(@"To receive new messages .", @"")]];
    [self.tableSource addObject:@[NSLocalizedString(@"Account modify", @"")]];
    [self.tableSource addObject:@[NSLocalizedString(@"Contact Customer Service", @""),NSLocalizedString(@"On the bit Internship", @""),NSLocalizedString(@"Feedback", @"")]];
    
    [self.tableView reloadData];
}

#pragma mark - UITableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.textColor = COLORWITH(@"#232323");
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    if (indexPath.section != 0) {
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }else{
        [cell setAccessoryType:UITableViewCellAccessoryNone];
        switchView = [[UIView alloc] initWithFrame:CGRectMake(WIDTH-68, 7, 55, 14)];
        switchView.backgroundColor = [UIColor clearColor];
        QZSwitchStyle style = QZSwitchStyleNoBorder;
        QZSwitch *switch0 = [[QZSwitch alloc] initWithFrame:CGRectMake(0, 0, 55, 14)];
        
        switch0.style = style;
        switch0.onTintColor = COLOR_DEFAULT;
        switch0.tintColor = COLORWITH(@"#CCCCCC");
        switch0.thumbTintColor = [UIColor whiteColor];
        [switch0 addTarget:self action:@selector(changeBtn:) forControlEvents:UIControlEventValueChanged];
        switch0.on = [DDUserInfo isPush]; //默认状态设置
        [switchView addSubview:switch0];
        [cell addSubview:switchView];
    }
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (indexPath.section == 2) {
        DDSystemSetCell *sysCell = [tableView dequeueReusableCellWithIdentifier:@"sysCell" forIndexPath:indexPath];
        sysCell.selectionStyle = UITableViewCellSelectionStyleNone;
        sysCell.leftLabel.text = self.tableSource[indexPath.section][indexPath.row];
        sysCell.leftLabel.textColor = COLORWITH(@"#232323");
        sysCell.leftLabel.font = [UIFont systemFontOfSize:14];
        [sysCell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        if (indexPath.row == 2) {
            sysCell.line.hidden = YES;
        }
        return sysCell;
    }
    cell.textLabel.text = self.tableSource[indexPath.section][indexPath.row];
    
    return cell;
}

- (void)changeBtn:(QZSwitch *)sender
{
    if (![DDUserInfo isCanNotification]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Push notification is not open .", @"") message:NSLocalizedString(@"Please Didi practice open message push permissions: Phone Settings - > Notifications - > Didi Internship HR ( allow notification )", @"") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", @""), nil];
        [alert show];
        [sender setOn:NO];
        return;
    }else{
        //系统的可以
        if (sender.isOn == YES) {
            [sender setOn:YES];
            [APService registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                           UIRemoteNotificationTypeSound |
                                                           UIRemoteNotificationTypeAlert)
                                               categories:nil];//开启推送
            [[TMCache sharedCache] setObject:@(1) forKey:@"isPush"];
            [[iToast makeText:NSLocalizedString(@"On Push", @"")] show];
        }else{
            [sender setOn:NO];
            [[UIApplication sharedApplication] unregisterForRemoteNotifications];//关闭推送
            [[TMCache sharedCache] setObject:@(0) forKey:@"isPush"];
            [[iToast makeText:NSLocalizedString(@"Close push", @"")] show];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 2) {
        return 10;
    } else {
        return 0.000001;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

#pragma mark - UITableViewDelegate
- (void)actionAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 1:
        {
            //修改账号
            DDChangeMobileViewController *changeVc = [[DDChangeMobileViewController alloc] init];
            changeVc.changeMobileDelegateVc = self;
            changeVc.typeTitle = @"1";
            [self.navigationController pushViewController:changeVc animated:YES];
            return;
        }
            break;
        case 2:
        {
            if (indexPath.row == 0) {
                //联系客服
                [self CallPhoneWithNum:phoneNumber];
                return;
            } else if(indexPath.row == 1) {
                //关于滴滴实习
                DDAboutUsViewController *aboutVc = [[DDAboutUsViewController alloc] init];
                [self.navigationController pushViewController:aboutVc animated:YES];
                return;
            } else {
                //意见反馈
                DDAdviceUsViewController *adviceVc = [[DDAdviceUsViewController alloc] init];
                [self.navigationController pushViewController:adviceVc animated:YES];
            }
        }
            break;
            
        default: //接收推送，不处理
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 1:
        {
            [self logout];
        }
            break;
            
        default:
            break;
    }
}
//退出的方法
-(void)logout{
    NSString *url = [NSString stringWithFormat:@"%@%@",publicUrl,logoutURL];
    NSDictionary *parmDic = @{@"uid":[DDUserInfo uid]};
    [self.request QZRequest_POST:url parameters:parmDic tagNSString:@"logout" stopRequest:YES isSerializer:NO];
}
//拨打电话方法
-(void)CallPhoneWithNum:(NSString *)phoneNum{
    [[TMCache sharedCache] setObject:@"1" forKey:@"webView"];
    NSURL *phoneURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneNum]];
    if ( !_phoneCallWebView ) {
        _phoneCallWebView = [[UIWebView alloc]initWithFrame:CGRectZero];// 这个webView只是一个后台的容易 不需要add到页面上来 效果跟方法二一样 但是这个方法是合法的
    }
    [_phoneCallWebView loadRequest:[NSURLRequest requestWithURL:phoneURL]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
