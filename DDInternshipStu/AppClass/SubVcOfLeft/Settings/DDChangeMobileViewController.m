//
//  DDChangeMobileViewController.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/16.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDChangeMobileViewController.h"
#import "UIButton+timer.h"
#import "QZBottomView.h"
#import "MobileCell.h"
#import "DDSystemSetViewController.h"
#import "DDHomeViewController.h"
#import "AppDelegate.h"
#import "QZLeftViewController.h"
#define getCodeUrl @"index.php?s=/Home/Common/get_sms_code"
#define set_mobile @"index.php?s=/Home/Student/set_mobile"
#define checkCodeUrl @"index.php?s=/Home/Common/check_code"
@interface DDChangeMobileViewController ()<UITextFieldDelegate>

@end

@implementation DDChangeMobileViewController
{
    NSString *newPhoneNum;//新手机号
    NSString *inputPhoneNum;//输入的手机号
    NSString *tempPhoneNum;//临时保存的手机号，用于突发情况
    NSString *phoneCode;//验证码
    NSString *inputPhoneCode;//输入的验证码
    UITextField *_codeTextField;//验证码输入框
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Modify the phone number", @"");
    
    [self addBtnOnNavWithTitle:nil andImageName:@"返回" andTarget:self andAction:@selector(backAction) andFrame:CGRectMake(0, 0, 18, 15) andDirection:LEFT];
    
    [self createTableViewWithPlainStyle:NO andSeparatorStyleNone:NO andBackGroudImageName:nil];
    
    [self registCellWithNib:NSStringFromClass([MobileCell class]) addIdentifier:NSStringFromClass([MobileCell class]) addHeight:44];
    
    [self createFooterView];
    
    [self loadData];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UITapGestureRecognizer * touchBegin = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(toucheBegin:)];
    [self.tableView addGestureRecognizer:touchBegin];
}

- (void)toucheBegin:(UITapGestureRecognizer *)tap
{
    for (int i = 500; i<502; i++) {
        UITextField *textField = [self.view viewWithTag:i];
        [textField resignFirstResponder];
    }
}

-(void)createFooterView
{
    NSString *subTitle;
    if ([self.typeTitle isEqualToString:@"2"]) {
        subTitle = NSLocalizedString(@"submit", @"");
    }else{
        subTitle = NSLocalizedString(@"Next", @"");
    }
    QZBottomView *bottomView = [[[NSBundle mainBundle]loadNibNamed:@"QZBottomView" owner:self options:nil]lastObject];
    bottomView.frame = CGRectMake(0, 0, WIDTH, 102);
    [bottomView.bottomBtn setTitle:subTitle forState:UIControlStateNormal];
    [bottomView.bottomBtn addTarget:self action:@selector(bottomBtnAction) forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableFooterView = bottomView;
}

#pragma mark - 数据
-(void)loadData
{
    if ([self.typeTitle isEqualToString:@"2"]) {
        [self.tableSource addObject:@[NSLocalizedString(@"New phone number", @""),NSLocalizedString(@"Verification code", @"")]];
    }else{
        [self.tableSource addObject:@[NSLocalizedString(@"Original phone number .", @""),NSLocalizedString(@"Verification code", @"")]];
    }
    [self.tableView reloadData];
}

- (void)loadCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    MobileCell *mobCell = (MobileCell *)cell;
    mobCell.yesBtn.hidden = YES;
    mobCell.noBtn.hidden = YES;
    mobCell.titleName.text = self.tableSource[indexPath.section][indexPath.row];
    mobCell.inputField.keyboardType = UIKeyboardTypePhonePad;
    mobCell.inputField.returnKeyType = UIReturnKeyDone;
    mobCell.inputField.delegate = self;
    mobCell.inputField.tag = indexPath.row + 500;
    [mobCell.inputField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    //验证原手机号时
    if (indexPath.row == 0 && [self.typeTitle isEqualToString:@"1"]) {
        mobCell.inputField.text = [DDUserInfo mobile];
        mobCell.inputField.userInteractionEnabled = NO;
    }
    mobCell.titleName.textColor = COLOR_FONT_REGIST;
    if (indexPath.row == 1) {
        mobCell.line.hidden = YES;
        mobCell.inputField.keyboardType = UIKeyboardTypeNumberPad;
        mobCell.inputFieldRight.constant = 105;
        mobCell.inputField.placeholder = NSLocalizedString(@"Enter confirmation code", @"");
        _codeTextField = mobCell.inputField;
        UIButton *getCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        getCodeBtn.frame = CGRectMake(WIDTH-96, 7, 82, 30);
        [getCodeBtn setBackgroundImage:[UIImage imageNamed:@"注册背景"] forState:UIControlStateNormal];
        [getCodeBtn setTitle:NSLocalizedString(@"get verification code", @"") forState:UIControlStateNormal];
        [getCodeBtn setTitleColor:COLOR_DEFAULT forState:UIControlStateNormal];
        getCodeBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [getCodeBtn addTarget:self action:@selector(getBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [mobCell addSubview:getCodeBtn];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 20;
    }else{
        return 0.000001;
    }
}

#pragma mark - 获取验证码
-(void)getBtnAction:(UIButton *)btn
{
    
    UITextField *numTextField = [self.view viewWithTag:501];
    if (![numTextField isFirstResponder]) {
        [numTextField becomeFirstResponder];
    }
    if ([self.typeTitle isEqualToString:@"1"]) {
        newPhoneNum = [DDUserInfo mobile];
    }
    if ([newPhoneNum isEqualToString:@""] || newPhoneNum == nil) {
        [[iToast makeText:NSLocalizedString(@"Sorry, you do not enter a phone number", @"")] show];
        return;
    }
    if ([self isValidateMobile:newPhoneNum]) {
        [btn addTimer];//加上倒计时
        tempPhoneNum = newPhoneNum;
    }else{
        [[iToast makeText:NSLocalizedString(@"Sorry , you entered an incorrect phone number ", @"")] show];
        return;
    }
    //获取验证码
    NSString *url = [NSString stringWithFormat:@"http://c.simpragma.com:18080/internserver/Home/Common/get_sms_code?secret_key=C8AEF3FD988AF5F6&mobile=9999999999"];
    int uid = [[DDUserInfo uid] intValue];
    NSDictionary *parmDic = @{@"uid":@(uid),
                              @"mobile":newPhoneNum,
                              @"group":@2};
    
    [self.request QZRequest_POST:url parameters:parmDic tagNSString:@"get_sms_code" stopRequest:YES isSerializer:NO];
    // 短信验证成为第一响应者
    [_codeTextField becomeFirstResponder];
    NSArray *viewArr = [[btn superview]subviews];
    for (UIView *subview in viewArr) {
        if ([[subview class] isSubclassOfClass:[UITextField class]]) {
            [(UITextField*)subview becomeFirstResponder];
        }
    }
}
#pragma mark ------<网络请求>
- (void)request_POST_FinishValue:(id)value tagNSString:(NSString *)tag
{
    NSLog(@"value:%@ tag:%@",value,tag);
    if ([tag isEqualToString:@"get_sms_code"]) {
        //验证码
        if ([value[@"status"] intValue] == 1) {
            //打印验证码
            NSLog(@"code:%@",value[@"data"][@"code"]);
            phoneCode = [NSString stringWithFormat:@"%@",value[@"data"][@"code"]];
        }else{
            [[iToast makeText:[NSString stringWithFormat:@"%@",value[@"msg"]]] show];
        }
    }else if ([tag isEqualToString:@"set_mobile"]) {
        //修改手机号
        [[iToast makeText:[NSString stringWithFormat:@"%@",value[@"msg"]]] show];
        if ([value[@"status"] intValue] == 1) {
            [[iToast makeText:NSLocalizedString(@"Modify phone number success", @"")] show];
            [[TMCache sharedCache] setObject:tempPhoneNum forKey:@"mobile"];
            [self performSelector:@selector(gotoSystemVC) withObject:nil afterDelay:1.2f];
        }else{
            [[iToast makeText:[NSString stringWithFormat:@"%@",value[@"msg"]]] show];
        }
    }else if ([tag isEqualToString:@"check_code"]){
        if ([value[@"status"] intValue] == 1) {
            [self changeMobile];
        }else{
            [[iToast makeText:[NSString stringWithFormat:@"%@",value[@"msg"]]] show];
        }
    }
}
//跳转到首页
-(void)gotoSystemVC{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
//提交申请
-(void)bottomBtnAction
{
    UITextField *textField;
    for (int i = 500; i<502; i++) {
        textField = [self.tableView viewWithTag:i];
        if (i == 500) {
            if (![self isValidateMobile:textField.text]) {
                [[iToast makeText:NSLocalizedString(@"Your input is not complete .", @"")] show];
                return;
            }else{
                inputPhoneNum = textField.text;
            }
        }else{
            if ([textField.text isEqualToString:@""] || textField.text == nil) {
                [[iToast makeText:NSLocalizedString(@"You have not entered verification code", @"")] show];
                return;
            }else{
                inputPhoneCode = textField.text;
            }
        }
    }
    /**
     *  tempPhoneNum 和 inputPhoneCode 是最终的参数
     */
    if (phoneCode != inputPhoneCode) {
        [[iToast makeText:NSLocalizedString(@"The code you entered is incorrect .", @"")] show];
        return;
    }
    NSString *url = [NSString stringWithFormat:@"%@%@",publicUrl,checkCodeUrl];
    NSDictionary *parmDic;
    if ([self.typeTitle isEqualToString:@"1"]) {
        parmDic = @{ @"mobile" :tempPhoneNum,
                     @"code"   :inputPhoneCode,
                     };
    }else{
        parmDic = @{ @"mobile" :tempPhoneNum,
                     @"code"   :inputPhoneCode,
                     @"group"  :@2
                     };
    }
    [self.request QZRequest_POST:url parameters:parmDic tagNSString:@"check_code" stopRequest:YES isSerializer:NO];
   
    
}
//修改手机号
-(void)changeMobile{
    if ([self.typeTitle isEqualToString:@"1"]) {
        DDChangeMobileViewController *newPhoneVc = [[DDChangeMobileViewController alloc] init];
        newPhoneVc.changeMobileDelegateVc = self.changeMobileDelegateVc;
        newPhoneVc.sendPhoneNumBlock = self.sendPhoneNumBlock;
        newPhoneVc.titleName = NSLocalizedString(@"Modify the phone number", @"");
        newPhoneVc.typeTitle = @"2";
        [self.navigationController pushViewController:newPhoneVc animated:YES];
    }else{
        NSString *url = [NSString stringWithFormat:@"%@%@",publicUrl,set_mobile];
        NSDictionary *parmDic = @{@"uid"    :[DDUserInfo uid],
                                  @"mobile" :tempPhoneNum,
                                  @"code"   :inputPhoneCode};
        [self.request QZRequest_POST:url parameters:parmDic tagNSString:@"set_mobile" stopRequest:YES isSerializer:NO];
    }
}
- (void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField.tag == 500) {
        newPhoneNum = [textField.text stringByAppendingString:string];
    }
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag == 500) {
        newPhoneNum = textField.text;
    }
}
- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField.tag == 500) {
        //手机号
        if ([textField.text length]>11) {
            textField.text = [textField.text substringToIndex:11];
        }
    }
    if (textField.tag == 501) {
        //验证码
        if ([textField.text length]>6) {
            textField.text = [textField.text substringToIndex:6];
        }
    }
}
/*手机号码验证*/
-(BOOL) isValidateMobile:(NSString *)mobile
{
    NSString *phoneRegex = @"^1[3|4|5|7|8]\\d{9}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:mobile];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
