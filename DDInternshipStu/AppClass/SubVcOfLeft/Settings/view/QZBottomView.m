
//
//  QZBottomView.m
//  DDInternshipStu
//
//  Created by 何川 on 15/8/25.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import "QZBottomView.h"
#import "UIView+nice.h"

@implementation QZBottomView

- (void)awakeFromNib {
    self.backgroundColor = [UIColor clearColor];
    [_bottomBtn setTitle:NSLocalizedString(@"Enter confirmation code", @"") forState:UIControlStateNormal];
    NSDictionary *attributeDic = @{CornerRadius:@(self.bottomBtn.frame.size.height/2),BorderColor:[UIColor clearColor],BorderWidth:@(0)};
    [self.bottomBtn RoundedLayerWithAttributeDic:attributeDic];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
