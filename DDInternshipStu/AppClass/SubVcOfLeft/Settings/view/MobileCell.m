//
//  MobileCell.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/16.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "MobileCell.h"

@implementation MobileCell

- (void)awakeFromNib {
    // Initialization code
    _titleName.text = NSLocalizedString(@"NewNumberKey", @"");
    _inputField.placeholder = NSLocalizedString(@"EnterKey", "");
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
