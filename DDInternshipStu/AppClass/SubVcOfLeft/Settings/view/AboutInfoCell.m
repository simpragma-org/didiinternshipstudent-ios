//
//  AboutInfoCell.m
//  DDInternshipHR
//
//  Created by 高继鹏 on 15/11/26.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "AboutInfoCell.h"

@implementation AboutInfoCell

- (void)awakeFromNib {
    self.backgroundColor = COLOR_DEFAULT;
    self.aboutLabel.textColor = [UIColor whiteColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
