//
//  AboutInfoCell.h
//  DDInternshipHR
//
//  Created by 高继鹏 on 15/11/26.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *aboutLabel;

@end
