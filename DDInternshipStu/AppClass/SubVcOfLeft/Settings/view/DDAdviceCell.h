//
//  DDAdviceCell.h
//  DDInternshipStu
//
//  Created by 何川 on 15/11/16.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDAdviceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *adviceView;
@property (weak, nonatomic) IBOutlet UILabel *showLabel;

@end
