//
//  MobileCell.h
//  DDInternshipStu
//
//  Created by 何川 on 15/11/16.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MobileCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleName;
@property (weak, nonatomic) IBOutlet UITextField *inputField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputFieldRight;
@property (weak, nonatomic) IBOutlet UILabel *line;
/** yes按钮 */
@property (weak, nonatomic) IBOutlet UIButton *yesBtn;
/** no按钮 */
@property (weak, nonatomic) IBOutlet UIButton *noBtn;

@end
