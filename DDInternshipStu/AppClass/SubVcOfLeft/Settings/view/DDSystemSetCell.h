//
//  DDSystemSetCell.h
//  DDInternshipStu
//
//  Created by 何川 on 15/11/16.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDSystemSetCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *line;

@end
