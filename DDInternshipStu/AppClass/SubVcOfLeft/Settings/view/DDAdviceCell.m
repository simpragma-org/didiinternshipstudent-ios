//
//  DDAdviceCell.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/16.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDAdviceCell.h"
#import "UIView+nice.h"

@implementation DDAdviceCell

- (void)awakeFromNib {
    _showLabel.text = NSLocalizedString(@"ShowKey", @"");
    [self.adviceView RoundedLayerWithCornerRadius:4 andBorderColor:[UIColor colorWithHexString:@"#EAEBEC"] andBorderWidth:1];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
