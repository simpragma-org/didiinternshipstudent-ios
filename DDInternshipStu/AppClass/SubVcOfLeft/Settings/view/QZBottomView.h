//
//  QZBottomView.h
//  DDInternshipStu
//
//  Created by 何川 on 15/8/25.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QZBottomView : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *bottomBtn;

@end
