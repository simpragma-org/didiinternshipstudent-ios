//
//  DDChangeMobileViewController.h
//  DDInternshipStu
//
//  Created by 何川 on 15/11/16.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "QZTableViewController.h"

typedef void (^sendNewPhoneNumBlock)(NSString* newPhone);

@interface DDChangeMobileViewController : QZTableViewController

@property (nonatomic, retain) UIViewController *changeMobileDelegateVc;
@property (nonatomic, copy) NSString *titleName;
@property (nonatomic, copy) NSString *typeTitle; //修改手机号的步骤
@property (nonatomic, copy) sendNewPhoneNumBlock sendPhoneNumBlock;
@end
