//
//  DDAdviceUsViewController.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/16.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDAdviceUsViewController.h"
#import "QZBottomView.h"
#import "DDAdviceCell.h"
#import "MLInputDodger.h"

@interface DDAdviceUsViewController ()<UITextViewDelegate>
{
    NSString *_inputText;

}
@end

@implementation DDAdviceUsViewController
{
    DDAdviceCell *adviceCell;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //距离键盘距离
    self.tableView.shiftHeightAsDodgeViewForMLInputDodger = 50;
    //注册
    [self.tableView registerAsDodgeViewForMLInputDodger];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"Feedback", @"");
    
    [self addBtnOnNavWithTitle:nil andImageName:@"返回" andTarget:self andAction:@selector(backAction) andFrame:CGRectMake(0, 0, 18, 15) andDirection:LEFT];
    
    [self createTableViewWithPlainStyle:NO andSeparatorStyleNone:NO andBackGroudImageName:nil];
    
    [self registCellWithNib:NSStringFromClass([DDAdviceCell class]) addIdentifier:NSStringFromClass([DDAdviceCell class]) addHeight:140];
    
    [self loadData];
    
    [self createFooterView];
    
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
}

-(void)createFooterView
{
    QZBottomView *bottomView = [[[NSBundle mainBundle]loadNibNamed:@"QZBottomView" owner:self options:nil]lastObject];
    bottomView.frame = CGRectMake(0, 0, WIDTH, 102);
    [bottomView.bottomBtn setTitle:NSLocalizedString(@"submit", @"") forState:UIControlStateNormal];
    [bottomView.bottomBtn addTarget:self action:@selector(bottomBtnAction) forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableFooterView = bottomView;
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadData
{
    [self.tableSource addObject:@[NSLocalizedString(@"Enter your suggestions and comments .", @"")]];
    [self.tableView reloadData];
}
#pragma mark - UITableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    adviceCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DDAdviceCell class]) forIndexPath:indexPath];
    adviceCell.selectionStyle = UITableViewCellSelectionStyleNone;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    adviceCell.adviceView.delegate = self;
    return adviceCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}

#pragma mark - textView delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    adviceCell.showLabel.hidden = YES;
    return YES;
}
- (void)textViewDidChange:(UITextView *)textView{
    _inputText = textView.text;

}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@""] ) {
        adviceCell.showLabel.hidden = NO;
        return YES;
    }
    return YES;
}
#pragma mark - 意见反馈的提交
- (void)bottomBtnAction{
    if ([adviceCell.adviceView.text isEqualToString:@""]) {
        [[iToast makeText:NSLocalizedString(@"Feedback information can not be empty ", @"")] show];
        return;
    }
    [self requestData];
}
-(void)requestData
{
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",publicUrl,@"index.php?s=/Home/Student/add_advise"];
    NSDictionary *dic = @{@"uid":[DDUserInfo uid],@"message":_inputText};
    NSLog(@"%@",dic);
    [self.request QZRequest_POST:urlstr parameters:dic tagNSString:@"Student/add_advise" stopRequest:YES isSerializer:NO];
}
#pragma mark requestPool
-(void)request_POST_FinishValue:(id)value tagNSString:(NSString *)tag
{
    NSLog(@"value = %@",value);
    NSString *errcode = [value objectForKey:@"status"];
    if ([errcode intValue] == 1) {
        [[iToast makeText:NSLocalizedString(@"Thank you for your feedback .", @"")] show];
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        NSString *msg = [value objectForKey:@"msg"];
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"prompt", @"") message:msg delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil];
        [alertView show];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
