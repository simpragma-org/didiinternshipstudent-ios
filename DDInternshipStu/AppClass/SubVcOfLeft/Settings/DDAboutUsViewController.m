//
//  DDAboutUsViewController.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/16.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDAboutUsViewController.h"
#import "PublicClass.h"
#import "AboutInfoCell.h"
#import "AppDelegate.h"

@interface DDAboutUsViewController ()

@end

@implementation DDAboutUsViewController
{
    NSAttributedString *contentStr; //文本内容
    AboutInfoCell *aboutCell;
    //显示内容的webview
    UIWebView *_webView;
    CGFloat cellHeight;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"about us", @"");
    self.view.backgroundColor = COLOR_DEFAULT;
    cellHeight = HEIGHT;
    
    [self getArticle];
    [self addBtnOnNavWithTitle:nil andImageName:@"返回" andTarget:self andAction:@selector(backAction) andFrame:CGRectMake(0, 0, 18, 15) andDirection:LEFT];
    
    [self createTableViewWithPlainStyle:YES andSeparatorStyleNone:NO andBackGroudImageName:nil];
    self.tableView.backgroundColor = COLOR_DEFAULT;
    
    if (![AppDelegate isNetworkConnected] && [[TMCache sharedCache] objectForKey:@"aboutUsString"]) {
        CGSize titleSize = [[[TMCache sharedCache] objectForKey:@"aboutUsString"] boundingRectWithSize:CGSizeMake(WIDTH-28, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11]} context:nil].size;
        cellHeight = titleSize.height;
    }
    [self registCellWithNib:NSStringFromClass([AboutInfoCell class]) addIdentifier:NSStringFromClass([AboutInfoCell class]) addHeight:cellHeight];
    
    [self.tableSource addObject:@[@""]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView reloadData];
    
    //顶部横线
    UILabel *topLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 0.5)];
    topLine.backgroundColor = COLOR_HEADAROUND;
    [self.view addSubview:topLine];
}
//获取当页文本
- (void)getArticle{
    NSDictionary *parmDic = @{@"id":@1,
                              @"version_code":@"V1.0"
                              };
    //NSString *url = [NSString stringWithFormat:@"%@%@",publicUrl,getArticleUrl];
    NSString *url = [NSString stringWithFormat:@"http://c.simpragma.com:18080/internserver/Home/Common/get_article?secret_key=C8AEF3FD988AF5F6"];
    [self.request QZRequest_POST:url parameters:parmDic tagNSString:@"get_article" stopRequest:YES isSerializer:NO isCache:YES];
}
- (void)request_POST_FinishValue:(id)value tagNSString:(NSString *)tag
{
    NSLog(@"value:%@,tag:%@",value,tag);
    if ([value[@"status"] intValue] == 1) {
        [[TMCache sharedCache] setObject:value[@"data"][@"content"] forKey:@"aboutUsString"];
        contentStr = [PublicClass handleArticleWithAttributeStringFromHTML:value[@"data"][@"content"]];
        aboutCell.aboutLabel.attributedText = contentStr;
        aboutCell.aboutLabel.textColor = [UIColor whiteColor];
        
        NSString *countSizeOfContent = value[@"data"][@"content"];
        CGSize titleSize = [countSizeOfContent boundingRectWithSize:CGSizeMake(WIDTH-28, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:9]} context:nil].size;
        cellHeight = titleSize.height-20;
        [self.tableView reloadData];
        
    }else{
        [[iToast makeText:NSLocalizedString(@"Sorry , failed to load. Please try again !", @"")] show];
    }
}

- (void)loadCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    aboutCell = (AboutInfoCell *)cell;
    if (![AppDelegate isNetworkConnected]) {
        contentStr = [PublicClass handleArticleWithAttributeStringFromHTML:[[TMCache sharedCache] objectForKey:@"aboutUsString"]];
        aboutCell.aboutLabel.attributedText = contentStr;
        aboutCell.aboutLabel.textColor = [UIColor whiteColor];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end