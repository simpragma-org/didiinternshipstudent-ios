//
//  DDMessageModel.h
//  DDIntershipStu
//
//  Created by HERO-PC on 15/11/29.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "JSONModel.h"

@interface DDMessageModel : JSONModel
@property (strong,nonatomic)NSString *id;//消息ID
@property (strong,nonatomic)NSString *type;//消息类型
@property (strong,nonatomic)NSString *title;//消息标题
@property (strong,nonatomic)NSString *content;//消息内容
@property (strong,nonatomic)NSString *create_time;//时间戳
@end
