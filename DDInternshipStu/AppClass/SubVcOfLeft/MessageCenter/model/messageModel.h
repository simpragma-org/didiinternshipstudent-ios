//
//  messageModel.h
//  shixi
//
//  Created by 何川 on 15/7/29.
//  Copyright (c) 2015年 shixi.Team. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface messageModel : NSObject

@property (strong,nonatomic)NSString *id;//消息ID
@property (strong,nonatomic)NSString *type;//消息类型
@property (strong,nonatomic)NSString *title;//消息标题
@property (strong,nonatomic)NSString *content;//消息内容
@property (strong,nonatomic)NSString *create_time;//时间戳

@end
