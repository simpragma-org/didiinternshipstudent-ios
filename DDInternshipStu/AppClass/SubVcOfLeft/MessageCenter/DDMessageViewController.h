//
//  DDMessageViewController.h
//  DDInternshipStu
//
//  Created by 何川 on 15/11/16.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "QZTableViewController.h"

@interface DDMessageViewController : QZTableViewController
@property (retain, nonatomic) UITableView *_tableView;
@end
