//
//  DDMessageCenterCell.h
//  DDInternshipStu
//
//  Created by 何川 on 15/11/16.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "messageModel.h"

@interface DDMessageCenterCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *titleImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentLabelHeight;
@property (weak, nonatomic) IBOutlet UILabel *line;
/** 时间label */
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

-(void)setCellWithModel:(messageModel *)model;

@end
