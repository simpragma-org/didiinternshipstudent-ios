//
//  DDMessageCenterCell.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/16.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDMessageCenterCell.h"

@implementation DDMessageCenterCell

-(void)setCellWithModel:(messageModel *)model
{
    self.titleLabel.text = model.title;
    [self setIntroductionText:model.content];
    self.line.backgroundColor = COLOR_LINE;
}
//赋值 and 自动换行,计算出cell的高度
-(void)setIntroductionText:(NSString*)text{
    //获得当前cell高度
    CGRect frame = [self frame];
    //文本赋值
    self.contentLabel.text = text;
    //设置label的最大行数
    self.contentLabel.numberOfLines = 10;
    
    CGSize size = CGSizeMake(235,1200); //设置一个行高上限
    NSDictionary *attribute = @{NSFontAttributeName: self.contentLabel.font};
    CGSize labelSize = [self.contentLabel.text boundingRectWithSize:size options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    
    int width = WIDTH;
    switch (width) {
        case 320:// 5s以下
        {
            self.contentLabelHeight.constant = labelSize.height+15;
            //计算出自适应的高度
            frame.size.height = labelSize.height+45;
        }
            break;
        case 375:// 6
        {
            self.contentLabelHeight.constant = labelSize.height+15;
            //计算出自适应的高度
            frame.size.height = labelSize.height+60;
        }
            break;
        case 414:// 6plus
        {
            self.contentLabelHeight.constant = labelSize.height+15;
            //计算出自适应的高度
            frame.size.height = labelSize.height+60;
        }
            break;
        default:
            break;
    }
    
    self.frame = frame;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
