//
//  DDMessageViewController.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/16.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDMessageViewController.h"
#import "DDMessageModel.h"
#import "DDMessageCenterCell.h"

#define notificationURL @"index.php?s=/Home/Student/get_notifications"

@interface DDMessageViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation DDMessageViewController
{
    DDMessageCenterCell *messageCell;
    NSArray *titleImageArr;
    NSMutableArray *_tableSource;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"NewsKey", @"");
    //消息
    //News
    
    [self getMessageNotification];
    
    [self addBtnOnNavWithTitle:nil andImageName:@"返回" andTarget:self andAction:@selector(backAction) andFrame:CGRectMake(0, 0, 18, 15) andDirection:LEFT];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT) style:UITableViewStylePlain];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor colorWithHexString:@"#EFEFF4"];
    [self.view addSubview:self.tableView];
    //    [self registCellWithNib:NSStringFromClass([DDMessageCenterCell class]) addIdentifier:NSStringFromClass([DDMessageCenterCell class]) addHeight:60];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([DDMessageCenterCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([DDMessageCenterCell class])];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 10)];
    headerView.backgroundColor = [UIColor colorWithHexString:@"#EFEFF4"];
    self.tableView.tableHeaderView = headerView;
    self.tableView.frame = CGRectMake(0, 0 , WIDTH, HEIGHT - 64);
    [self loadData];
    
}

#pragma mark - 加载数据
- (void)getMessageNotification
{
    NSString *url = [NSString stringWithFormat:@"%@%@",publicUrl,notificationURL];
    NSDictionary *parmDic = @{@"uid":[DDUserInfo uid]};
    [self.request QZRequest_POST:url parameters:parmDic tagNSString:@"get_notifications" stopRequest:YES isSerializer:NO isCache:YES];
}
#pragma mark ------<网络请求>
- (void)request_POST_FinishValue:(id)value tagNSString:(NSString *)tag
{
    NSLog(@"value:%@,tag:%@",value,tag);
    
    //############################
    
//    NSDictionary *data1 = @{ @"id" : @"1234",
//                            @"type" : @"2",
//                            @"title" : @"Go for it",
//                            @"content" : @"I am a human",
//                            @"create_time" : @"2016-01-16T05:00:00Z"
//                            };
//    
//    
//    NSArray *keys = [data1 allKeys];
//    NSArray *values = [data1 allValues];
//    NSArray *data = [[NSArray alloc] initWithObjects:data1,nil];
//    
//    NSDictionary *value1 = @{ @"data" : data,
//                             @"msg" : @"successful",
//                             @"status" : @"1"
//                             };
   
    /*
    
    "data":["id":"1234" , "type":"2" , "title":"Go for it" , "content":"I am a human" , "create_time":"2016-01-16T05:00:00Z"] , "msg":"successful" , "status":"1"
    ]
    
   */
    
   // NSString *json = @"{\"data\":[\"id\":\"1234\" , \"type\":\"2\" , \"title\":\"Go for it\" , \"content\":\"I am a human\" , \"create_time\":\"2016-01-16T05:00:00Z\"] , \"msg\":\"successful\" , \"status\":\"1\"}";
    NSString *json = @"{\"data\":[{\"id\":\"1234\" , \"type\":\"0\" , \"title\":\"Go for it\" , \"content\":\"I am a human\" , \"create_time\":\"2016-01-16T05:00:00Z\"},{\"id\":\"1234\" , \"type\":\"1\" , \"title\":\"Go for it\" , \"content\":\"I am a human\" , \"create_time\":\"2016-01-16T05:00:00Z\"}] , \"msg\":\"successful\" , \"status\":\"1\"}";
    NSError *error;
    NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"data: %@", data);

    NSDictionary *value1 = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
    NSLog(@"value1: %@", value1);
    
    
    if ([value1[@"status"] intValue] == 1) {
        [_tableSource removeAllObjects];
        
        if ([value1[@"data"] isKindOfClass:[NSArray class]]) {
            NSArray *mesDataArr = value1[@"data"];
            for (NSDictionary *mDic in mesDataArr) {
                DDMessageModel *model = [[DDMessageModel alloc] init];
                [model setValuesForKeysWithDictionary:mDic];
                [_tableSource addObject:model];
            }
            [[TMCache sharedCache] setObject:mesDataArr forKey:@"message"];
        }
        [self.tableView reloadData];
    }else{
        [[iToast makeText:[NSString stringWithFormat:@"%@",value1[@"msg"]]] show];
    }
}
- (void)loadData
{
    _tableSource = [[NSMutableArray alloc]init];
    titleImageArr = [[NSArray alloc] init];
    titleImageArr = @[[UIImage imageNamed:@"提醒"],[UIImage imageNamed:@"通知"]];
}

- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableview
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0000001;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0000001;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    messageCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DDMessageCenterCell class])];
    messageCell.selectionStyle = UITableViewCellSelectionStyleNone;
    messageModel *model = _tableSource[indexPath.row];
    messageCell.titleImage.image = titleImageArr[[model.type intValue]];
    messageCell.timeLabel.text = [self timeStringWithDate:model.create_time];
    [messageCell setCellWithModel:model];
    return messageCell;
}
//时间戳处理
-(NSString *)timeStringWithDate:(NSString *)add_time{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm"];
    NSTimeZone* timeZone = [NSTimeZone systemTimeZone];
    [formatter setTimeZone:timeZone];
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:[add_time floatValue]];
    NSString *startStr = [formatter stringFromDate:startDate];
    //当前时间对比
    NSDate *now = [NSDate date];
    NSString *nowStr  = [NSString stringWithFormat:@"%@",now];
    NSRange range = {5,5};
    NSString *nowDay = [nowStr substringWithRange:range];
    NSString *addDay = [startStr substringWithRange:range];
    NSString *startTimeSlot;
    if ([nowDay isEqualToString:addDay]) {
        //今天就只显示时间
        NSRange range = {11,5};
        startTimeSlot = [startStr substringWithRange:range];
    }else{
        //不是今天显示月日
        NSRange range = {5,5};
        startTimeSlot = [startStr substringWithRange:range];
    }
    return startTimeSlot;
}
#pragma mark - tableView delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tableSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    messageCell = (DDMessageCenterCell *)[self tableView:self.tableView cellForRowAtIndexPath:indexPath];
    return messageCell.frame.size.height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
