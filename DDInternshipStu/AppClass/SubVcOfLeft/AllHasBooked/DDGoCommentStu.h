//
//  DDGoCommentStu.h
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/19.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "QZTableViewController.h"

@interface DDGoCommentStu : QZTableViewController
/** hrID */
@property (nonatomic, copy) NSString *hrId;
/** 预约id */
@property (nonatomic, copy) NSString *cur_id;
/** 公司名字 */
@property (nonatomic, copy) NSString *company;
/** 订单id */
@property (nonatomic, copy) NSString *od_id;
@end
