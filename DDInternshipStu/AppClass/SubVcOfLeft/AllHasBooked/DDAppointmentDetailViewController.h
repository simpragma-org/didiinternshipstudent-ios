//
//  DDAppointmentDetailViewController.h
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/18.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "QZTableViewController.h"

@interface DDAppointmentDetailViewController : QZTableViewController

/** hrid */
@property (nonatomic, copy) NSString *hrId;
/** 预约id */
@property (nonatomic, copy) NSString *cur_id;
/** 时间title */
@property (nonatomic, copy) NSString *timeSlotStr;
@end
