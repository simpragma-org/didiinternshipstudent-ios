//
//  DDGoCommentStu.m
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/19.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDGoCommentStu.h"
#import "DDAppointmentDetailEvaCell.h"
#import "QZBottomView.h"
#define getComment @"index.php?s=/Home/Student/Comment"
//提交标签的接口
#define set_review @"index.php?s=/Home/Student/set_review"
//提交按钮的接口
#define update_curr_status @"index.php?s=/Home/Student/update_curr_status"

@interface DDGoCommentStu ()
{
    UILabel *_companyName;
    NSInteger _tagCount;
    NSMutableArray *_tagNumArray;    
}
@end

@implementation DDGoCommentStu

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"EvalutionStringKey", "");
    //评价
    //Evaluation
    
    [self addBtnOnNavWithTitle:nil andImageName:@"返回" andTarget:self andAction:@selector(backAction) andFrame:CGRectMake(0, 0, 18, 15) andDirection:LEFT];
    //注册tableView
    [self createTableViewWithPlainStyle:NO andSeparatorStyleNone:NO andBackGroudImageName:nil];
    [self registCellWithNib:@"DDAppointmentDetailEvaCell" addIdentifier:@"symCell" addHeight:54];
    self.tableView.separatorStyle  = UITableViewCellSeparatorStyleNone;
    self.tableView.bounces = NO;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.frame = CGRectMake(0, 60, WIDTH, HEIGHT - 20);
    _tagNumArray = [[NSMutableArray alloc]init];
    //评论数量初始化
    _tagCount = 0;
    //加载头视图
    [self createheadView];
    [self createFooterView];
    //加载数据
    [self loadDate];
}
-(void)loadDate{
    [self.tableSource addObject:@[@"",@"",@"",@"",@""]];
    NSString *url2 = [NSString stringWithFormat:@"%@%@",publicUrl,getComment];
    NSDictionary *dic2 = @{
                           @"hid":@([self.hrId intValue])
                           };
    [self.request QZRequest_POST:url2 parameters:dic2 tagNSString:@"getComment" stopRequest:NO isSerializer:NO];
}
#pragma mark ------<网络请求>
-(void)request_POST_FinishValue:(id)value tagNSString:(NSString *)tag{
    NSLog(@"%@   %@",value,tag);
    if([tag isEqualToString:@"getComment"]){
        NSString *errcode = [value objectForKey:@"status"];
        if ([errcode intValue] == 1) {
            NSArray *dic = value[@"data"];
            NSMutableArray * numberArr = [[NSMutableArray alloc]init];
            for (NSDictionary *comDic in dic) {
                for (NSDictionary *tagDic in comDic[@"tags"]) {
                    //获取了15个标签的数字
                    [numberArr addObject:tagDic[@"number"]];
                }
            }
            for (int i = 0; i<15; i+=3) {
                NSArray * array = [numberArr objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(i, 3)]];
                [_tagNumArray addObject:array];
            }
            for (int i = 0; i<5; i++) {
                DDAppointmentDetailEvaCell *appoEvaCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                    NSArray *arr =_tagNumArray[i];
                for (int i = 0; i<3; i++) {
                    UILabel *label = (UILabel *)appoEvaCell.tagNumLabel[i];
                    if([arr[i] intValue]>99){
                        label.text = @"99+";
                    }else{
                        label.text = arr[i];
                    }
                }
            }
            [self.tableView reloadData];
        }else{
            [[iToast makeText:[NSString stringWithFormat:@"%@",[value objectForKey:@"msg"]]]show];
        }
    }else if([tag isEqualToString:@"set_review"]){
        NSString *errcode = [value objectForKey:@"status"];
        if ([errcode intValue] == 1) {
            [[iToast makeText:NSLocalizedString(@"CommentsKey", @"")]show];
            //评论标签成功
            //Comments Tags Success
            
        }else{
            [[iToast makeText:[NSString stringWithFormat:@"%@",[value objectForKey:@"msg"]]]show];
        }
    }else if([tag isEqualToString:@"update_curr_status"]){
        NSString *errcode = [value objectForKey:@"status"];
        if ([errcode intValue] == 1) {
            [[iToast makeText:NSLocalizedString(@"EvaluationtagKey", @"")]show];
           // Evaluation of Success
            // 评价成功
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [[iToast makeText:[NSString stringWithFormat:@"%@",[value objectForKey:@"msg"]]]show];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
#pragma mark ------<tableView>
-(void)loadCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    DDAppointmentDetailEvaCell *appoEvaCell = (DDAppointmentDetailEvaCell *)cell;
    cell.tag = 30000 + indexPath.row;
    if (indexPath.row == 0) {
        appoEvaCell.leftImage.image = [UIImage imageNamed:@"描述-相符"];
    }else if (indexPath.row == 1){
        appoEvaCell.leftImage.image = [UIImage imageNamed:@"工资"];
    }else if (indexPath.row == 2){
        appoEvaCell.leftImage.image = [UIImage imageNamed:@"雇主"];
    }else if (indexPath.row == 3){
        appoEvaCell.leftImage.image = [UIImage imageNamed:@"工作-环境"];
    }else if (indexPath.row == 4){
        appoEvaCell.leftImage.image = [UIImage imageNamed:@"工作-内容-0"];
    }
    int i = 0;
    for (UIView *view in appoEvaCell.tagViewArray) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectLabel:)];
        view.userInteractionEnabled = YES;
        [view addGestureRecognizer:tap];
        view.tag = 1000 + i;
        i++;
    }

}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.000001;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.000001;
}
//tap方法
-(void)selectLabel:(UITapGestureRecognizer *)tap{
    DDAppointmentDetailEvaCell *appoEvaCell = (DDAppointmentDetailEvaCell *)[[tap.view superview]superview];
    int index = tap.view.tag%1000;
    UIImageView * bgImage= appoEvaCell.tagBgImageView[index];
    bgImage.image = [UIImage imageNamed:@"标签蓝色框"];
    UILabel *tagLabel = appoEvaCell.tagLabel[index];
    tagLabel.textColor = [UIColor whiteColor];
    UILabel *tagNumLabel = appoEvaCell.tagNumLabel[index];
    tagNumLabel.textColor = [UIColor whiteColor];
    appoEvaCell.userInteractionEnabled = NO;
    [self.tableView reloadData];
    _tagCount++;
    //点击标签请求
    NSInteger content_id = appoEvaCell.tag - 30000;
    NSString *url = [NSString stringWithFormat: @"%@%@",publicUrl,set_review];
    NSDictionary *dic = @{
                          @"hid":self.hrId,
                          @"student_id":[DDUserInfo uid],
                          @"curriculum_id":self.cur_id,
                          @"content_id":@(content_id + 1),
                          @"tag_id":@(index+1)
                          };
    [self.request QZRequest_POST:url parameters:dic tagNSString:@"set_review" stopRequest:NO isSerializer:NO];
}
//脚视图
-(void)createFooterView{
    QZBottomView *bottomView = [[[NSBundle mainBundle]loadNibNamed:@"QZBottomView" owner:self options:nil]lastObject];
    bottomView.frame = CGRectMake(0, 0, WIDTH, 102);
    [bottomView.bottomBtn setTitle:NSLocalizedString(@"SubmitKey", @"") forState:UIControlStateNormal];
    //提交
    //Submit
    [bottomView.bottomBtn addTarget:self action:@selector(bottomBtnAction) forControlEvents:UIControlEventTouchUpInside];
    self.tableView.tableFooterView = bottomView;
}
//提交按钮方法
-(void)bottomBtnAction{
    
    if (_tagCount != 5) {
        [[iToast makeText:NSLocalizedString(@"AllCommentsKey", @"")]show];
        //Please submit all comments
        // 请全部评论再提交
        return;
    }
    NSString *url = [NSString stringWithFormat: @"%@%@",publicUrl,update_curr_status];
    NSDictionary *dic = @{@"od_id":self.od_id,
                          };
    [self.request QZRequest_POST:url parameters:dic tagNSString:@"update_curr_status" stopRequest:NO isSerializer:NO];   
}
//加载头视图
-(void)createheadView{
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 60)];
    headView.backgroundColor = [UIColor whiteColor];
    //左上角的图片
    UIImageView *headImage = [[UIImageView alloc]initWithFrame:CGRectMake(12, 20, 14, 14)];
    headImage.image = [UIImage imageNamed:@"iconfont-gongsizuzhi"];
    [headView addSubview:headImage];
    //公司名字
    _companyName = [[UILabel alloc]initWithFrame:CGRectMake(40, 20, WIDTH - 40, 14)];
    _companyName.font = [UIFont systemFontOfSize:14];
    _companyName.text = self.company;
    [headView addSubview:_companyName];
    [self.view addSubview:headView];
}
#pragma mark - 返回
- (void)backAction
{
    if (_tagCount>0) {
        [[iToast makeText:NSLocalizedString(@"AllCommentsKey", @"")]show];
        return;
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
