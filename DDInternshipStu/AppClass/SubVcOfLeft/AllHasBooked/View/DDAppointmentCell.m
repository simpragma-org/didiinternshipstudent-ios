//
//  DDAppointmentCell.m
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/17.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDAppointmentCell.h"

@implementation DDAppointmentCell

- (void)awakeFromNib {
    // Initialization code
    _companyName.text = NSLocalizedString(@"CompanyKey", @"");
    //北京慧摩尔科技有限公司
    //Moore Technology Co., Ltd. Beijing Hui
    _positionLabel.text = NSLocalizedString(@"PositionKey", @"");
    //会计
    //Accounting
    _addressLabel.text = NSLocalizedString(@"AddressKey", @"");
    //北京望京SOHO5层
    // Beijing Wangjing SOHO5 layer
    [_agreeBtn setTitle:NSLocalizedString(@"AcceptKey", @"") forState:UIControlStateNormal];
   //接受
    //Accept
    [_overEvaBtn setTitle:NSLocalizedString(@"RatedKey", @"") forState:UIControlStateNormal];
    //已评价
    //Rate
    [_disagreeBtn setTitle:NSLocalizedString(@"RefuseKey", @"") forState:UIControlStateNormal];
    //拒绝
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)cellStatusWithModel:(CourseListModel *)model{
    self.nameLabel.text = model.name;
    self.companyName.text = model.company;
    self.addressLabel.text = model.company_address;
   [self.headImage sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:[UIImage imageNamed:@"默认头像"]];
    self.positionLabel.text = model.post;
    //圆形头像
    self.headImage.layer.masksToBounds = YES;
    self.headImage.layer.cornerRadius = 25;
    NSString *dayStr = [self timeStringWithStartTime:model.start_time andEndTime:model.end_time];
    self.timeLabel.text =[NSString stringWithFormat:@"%@ %@",[dayStr substringToIndex:10],[self timeStringWithTimeSlot:model.time_slot]];
    
    
    if ([model.type isEqualToString:@"1"]||[model.type intValue] == 1) {
        //今日类型
        self.agreeView.hidden = YES;
        if ([model.flag isEqualToString:@"6"]){
            //进行中
            self.evaBtn.hidden = YES;
            self.overEvaBtn.hidden = YES;
            self.stateBtn.hidden = NO;
            [self.stateBtn setTitle:@"进行中" forState:UIControlStateNormal];
            [self.stateBtn setBackgroundImage:[UIImage imageNamed:@"进行中"] forState:UIControlStateNormal];
        }else if([model.flag isEqualToString:@"3"]){
            //未开始
            self.evaBtn.hidden = YES;
            self.overEvaBtn.hidden = YES;
            self.stateBtn.hidden = NO;
            [self.stateBtn setTitle:@"未开始" forState:UIControlStateNormal];
            [self.stateBtn setBackgroundImage:[UIImage imageNamed:@"未开始"] forState:UIControlStateNormal];
        }else{
            //数据错误就显示这个
            self.overEvaBtn.hidden = YES;
            self.evaBtn.hidden = YES;
            self.stateBtn.hidden = YES;
        
        }
    }else if ([model.type isEqualToString:@"2"]||[model.type intValue] == 2){
        //新的预约
        self.overEvaBtn.hidden = YES;
        self.evaBtn.hidden = YES;
        self.agreeView.hidden = NO;
        self.stateBtn.hidden = YES;
    }else if ([model.type isEqualToString:@"3"]||[model.type intValue] == 3){
        //完成预约
        self.agreeView.hidden = YES;
        if ([model.flag isEqualToString:@"8"]||[model.flag isEqualToString:@"2"]) {
            //有预约拒绝了
            self.overEvaBtn.hidden = YES;
            self.evaBtn.hidden = YES;
            self.stateBtn.hidden = NO;
            [self.stateBtn setTitle:NSLocalizedString(@"RejectedKey", @"") forState:UIControlStateNormal];
            //已拒绝
            //Rejected
            [self.stateBtn setBackgroundImage:[UIImage imageNamed:@"已拒绝"] forState:UIControlStateNormal];
        }else if ([model.flag isEqualToString:@"4"]){
            //已完成未评价
            self.stateBtn.hidden = NO;
            self.evaBtn.hidden = NO;
            self.overEvaBtn.hidden = YES;
            [self.stateBtn setTitle:NSLocalizedString(@"CompletedKey", @"") forState:UIControlStateNormal];
            //已完成
            //Completed
            [self.stateBtn setBackgroundImage:[UIImage imageNamed:@"已完成"] forState:UIControlStateNormal];
        }else if ([model.flag isEqualToString:@"5"]){
            //已完成已评价
            self.stateBtn.hidden = NO;
            self.evaBtn.hidden = YES;
            self.overEvaBtn.hidden = NO;
            [self.stateBtn setTitle:NSLocalizedString(@"CompletedKey", @"") forState:UIControlStateNormal];
            //已完成
            //
            [self.stateBtn setBackgroundImage:[UIImage imageNamed:@"已完成"] forState:UIControlStateNormal];
        }else if ([model.flag isEqualToString:@"7"]){
            self.evaBtn.hidden = YES;
            self.stateBtn.hidden = NO;
            self.overEvaBtn.hidden = YES;
            [self.stateBtn setTitle:NSLocalizedString(@"ExpiredKey", @"") forState:UIControlStateNormal];
            //已过期
            //Expired
            [self.stateBtn setBackgroundImage:[UIImage imageNamed:@"已完成"] forState:UIControlStateNormal];
        }else{
            //已完成
            self.stateBtn.hidden = NO;
            self.evaBtn.hidden = YES;
            self.overEvaBtn.hidden = YES;
            [self.stateBtn setTitle:NSLocalizedString(@"CompletedKey", @"") forState:UIControlStateNormal];
            //已完成
            //Completed
            [self.stateBtn setBackgroundImage:[UIImage imageNamed:@"已完成"] forState:UIControlStateNormal];
        }
    }
}
//获取当前时间戳
-(NSString *)nowTimeChuo{
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval a=[dat timeIntervalSince1970];
    NSString *timeString = [NSString stringWithFormat:@"%f", a];
    return timeString;
}

//处理时间戳的方法
-(NSString *)timeStringWithStartTime:(NSString *)start_time andEndTime:(NSString *)end_time{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm"];
    NSTimeZone* timeZone = [NSTimeZone systemTimeZone];
    [formatter setTimeZone:timeZone];
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:[start_time floatValue]];
    NSString *startStr = [formatter stringFromDate:startDate];
    NSString *startTimeSlot = [startStr substringToIndex:16];
    NSRange range = {11,2};
    NSString *startHour = [startStr substringWithRange:range];
    NSString *endHour = [NSString stringWithFormat:@"%.2d",[startHour intValue]+2] ;
    NSString *timeSlot = [NSString stringWithFormat:@"%@-%@:00",startTimeSlot,endHour];
    return timeSlot;
}
-(NSString *)timeStringWithTimeSlot:(NSString *)time_slot{
    switch ([time_slot intValue]) {
        case 1:
            return @"8:00-10:00";
            break;
        case 2:
            return @"10:00-12:00";
            break;
        case 3:
            return @"12:00-14:00";
            break;
        case 4:
            return @"14:00-16:00";
            break;
        case 5:
            return @"16:00-18:00";
            break;
        case 6:
            return @"18:00-20:00";
            break;
        case 7:
            return @"20:00-22:00";
            break;
        default:
            return @"";
            break;
    }


}
@end
