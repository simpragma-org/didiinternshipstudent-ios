//
//  DDAppointmentCell.h
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/17.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CourseListModel.h"
@interface DDAppointmentCell : UITableViewCell
/** 头像 */
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
/** 名字 */
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
/** 公司名字 */
@property (weak, nonatomic) IBOutlet UILabel *companyName;
/** 时间 */
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
/** 职位 */
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;
/** 地址 */
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
/** 状态按钮 */
@property (weak, nonatomic) IBOutlet UIButton *stateBtn;
/** 接受视图 */
@property (weak, nonatomic) IBOutlet UIView *agreeView;
/** 接受按钮 */
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;
/** 拒绝按钮 */
@property (weak, nonatomic) IBOutlet UIButton *disagreeBtn;
/** 去评价按钮 */
@property (weak, nonatomic) IBOutlet UIButton *evaBtn;
/** 已评价按钮 */
@property (weak, nonatomic) IBOutlet UIButton *overEvaBtn;


-(void)cellStatusWithModel:(CourseListModel *)model;
@end
