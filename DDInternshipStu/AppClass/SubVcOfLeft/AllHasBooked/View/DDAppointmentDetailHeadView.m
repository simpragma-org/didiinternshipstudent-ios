//
//  DDAppointmentDetailHeadView.m
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/18.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDAppointmentDetailHeadView.h"

@implementation DDAppointmentDetailHeadView

- (void)awakeFromNib {

    self.headView.layer.cornerRadius = self.headView.frame.size.width / 2;
    self.headView.clipsToBounds = YES;
    self.headImage.layer.cornerRadius = self.headImage.frame.size.width / 2;
    self.headImage.clipsToBounds = YES;
    //工作内容的label调整
    CGSize size = CGSizeMake(self.workContent.frame.size.width,100000000); //设置一个行高上限
    NSDictionary *attribute = @{NSFontAttributeName: self.workContent.font};
    CGSize labelSize = [self.workContent.text boundingRectWithSize:size options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    self.workContentHeight.constant = labelSize.height;
    //地址内容label调整
    CGSize size2 = CGSizeMake(self.addressLabel.frame.size.width,100000000);
    NSDictionary *attribute2 = @{NSFontAttributeName: self.addressLabel.font};
    CGSize labelSize2 = [self.addressLabel.text boundingRectWithSize:size2 options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute2 context:nil].size;
    self.addressHeight.constant = labelSize2.height;

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
