//
//  DDAppointmentDetailEvaCell.h
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/18.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDAppointmentDetailEvaCell : UITableViewCell
/** 左侧原型图片 */
@property (weak, nonatomic) IBOutlet UIImageView *leftImage;
/** 标签视图 */
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *tagViewArray;
/** 标签图片 */
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *tagBgImageView;
/** 标签label */
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *tagLabel;
/** 标签数字label */
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *tagNumLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tagNumLabelWidth;






///** 标签view1 */
//@property (weak, nonatomic) IBOutlet UIView *labelView1;
//@property (weak, nonatomic) IBOutlet UIView *labelView2;
//@property (weak, nonatomic) IBOutlet UIView *labelView3;
///** 标签内的ImageView */
//@property (weak, nonatomic) IBOutlet UIImageView *blueLabel1;
//@property (weak, nonatomic) IBOutlet UIImageView *blueLabel2;
//@property (weak, nonatomic) IBOutlet UIImageView *blueLabel3;
///** 满意程度的label */
//@property (nonatomic, strong) UILabel *label1;
//@property (nonatomic, strong) UILabel *label2;
//@property (nonatomic, strong) UILabel *label3;
///** 后面的数字label */
//@property (nonatomic, strong) UILabel *numLabel1;
//@property (nonatomic, strong) UILabel *numLabel2;
//@property (nonatomic, strong) UILabel *numLabel3;
@end
