//
//  DDAppointmentDetailHeadView.h
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/18.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDAppointmentDetailHeadView : UITableViewCell
/** 头像视图 */
@property (weak, nonatomic) IBOutlet UIView *headView;
/** 头像图片 */
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
/** 时间 */
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
/** 职位 */
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;
/** 职位描述 */
@property (weak, nonatomic) IBOutlet UILabel *workContent;
/** 地址 */
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
/** 姓名 */
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
/** 公司 */
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
/** 电话按钮 */
@property (weak, nonatomic) IBOutlet UIButton *phoneBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *workContentHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressHeight;

@end
