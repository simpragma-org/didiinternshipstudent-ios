//
//  CourseListModel.h
//  DDIntershipStu
//
//  Created by HERO-PC on 15/12/1.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "JSONModel.h"

@interface CourseListModel : JSONModel
/** hr 头像 */
@property (nonatomic, copy) NSString *avatar;
/** hr 公司名 */
@property (nonatomic, copy) NSString *company;
/** 公司地址 */
@property (nonatomic, copy) NSString *company_address;
/** 天的date */
@property (nonatomic, copy) NSString *date;
/** 开始时间 */
@property (nonatomic, copy) NSString *start_time;
/** end_time */
@property (nonatomic, copy) NSString *end_time;
/** 是否反馈 */
@property (nonatomic, copy) NSString *feedback;
/** hr的name */
@property (nonatomic, copy) NSString *name;
/** hr id */
@property (nonatomic, copy) NSString *hrid;
/** 预约id */
@property (nonatomic, copy) NSString *id;
/** 时间段 */
@property (nonatomic, copy) NSString *time_slot;
/** 预约状态 */
@property (nonatomic, copy) NSString *status;
/** 类型 */
@property (nonatomic, copy) NSString *type;
/** 预约的状态 另一个参数 */
@property (nonatomic, copy) NSString *flag;
/** 职位 */
@property (nonatomic, copy) NSString *post;
/** 订单id */
@property (nonatomic, copy) NSString *od_id;
@end
