//
//  DDNewOrderViewController.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/17.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDNewOrderViewController.h"
#import "DDAppointmentCell.h"
#import "DDAppointmentDetailViewController.h"
#import "DDGoCommentStu.h"
#import "CourseListModel.h"
#import "NoListShowView.h"
#define course_list @"index.php?s=/Home/Student/course_list"
#define feedback @"index.php?s=/Home/Student/feedback"
@interface DDNewOrderViewController ()
{
    UIButton *_todayBtn;
    UIButton *_notFinishBtn;
    UIButton *_finishBtn;
    NSString *_url;
    NSString *total_page; //所有页面数
    long _page;
    NSString *_selectType;
    NSString *total_rows; //所有行数
    long _size; //页面传输的数量
    NoListShowView *_noListView;
}
@end

@implementation DDNewOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"ReservationKey", @"");
    //All reservation
    //所有预约
    //
    [self addBtnOnNavWithTitle:nil andImageName:@"返回" andTarget:self andAction:@selector(backAction) andFrame:CGRectMake(0, 0, 18, 15) andDirection:LEFT];
    //注册tableView
    [self createTableViewWithPlainStyle:NO andSeparatorStyleNone:NO andBackGroudImageName:nil];
    [self registCellWithNib:@"DDAppointmentCell" addIdentifier:@"appointmentCell" addHeight:105];
    self.tableView.frame = CGRectMake(0, 44, WIDTH, HEIGHT - 44 - 64);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //网络请求url
    //_url = [NSString stringWithFormat:@"%@%@",publicUrl,course_list];
    _url = [NSString stringWithFormat:@"http://192.168.2.133:8080/internserver/Home/Student/course_list/?secret_key=C8AEF3FD988AF5F6"];
    _size = 10;
    _page = 1;
    _selectType = @"2";
    total_page = @"1";
    //上面三个按钮
    [self addRefreshWithRefreshAnimationWithImages:nil];
    [self createHeadBtn];
//    [self requestData];
}
- (void)viewWillAppear:(BOOL)animated{
    _page = 1;
    self.page = 1;
    [self requestData];
}
//请求分页数据
- (void)requestData{
    if (self.page <= [total_page intValue]) {
        //NSDictionary *dic = @{@"uid":[DDUserInfo uid],
//        @"type":_selectType,
//        @"pagesize":@(_size),
//        @"p":@(self.page)
//    };

        NSDictionary *dic = @{@"uid":@"student1",
                              @"type":_selectType,
                              @"pagesize":@(_size),
                              @"p":@(self.page)
                              };
        [self.request QZRequest_POST:_url parameters:dic tagNSString:@"course_list" stopRequest:YES isSerializer:NO isCache:YES];
    }else{
        [[iToast makeText:NSLocalizedString(@"MyselfKey", @"")] show];
        //This is all the data myself
        // 这是所有的数据喽
        [self.tableView reloadData];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.000001;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.000001;
}
#pragma mark ------<tableView>
-(void)loadCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    DDAppointmentCell *appoCell = (DDAppointmentCell *)cell;
    CourseListModel *courseListModel = self.tableSource[indexPath.section][indexPath.row];
    [appoCell cellStatusWithModel:courseListModel];
    //评价按钮
    appoCell.evaBtn.tag = 3000+indexPath.row;
    [appoCell.evaBtn addTarget:self action:@selector(goEva:) forControlEvents:UIControlEventTouchUpInside];
    //同意按钮
    appoCell.agreeBtn.tag = 222;
    [appoCell.agreeBtn addTarget:self action:@selector(agree:) forControlEvents:UIControlEventTouchUpInside];
    //拒绝按钮
    appoCell.disagreeBtn.tag = 333;
    [appoCell.disagreeBtn addTarget:self action:@selector(agree:) forControlEvents:UIControlEventTouchUpInside];
}
//点击预约进入的
-(void)actionAtIndexPath:(NSIndexPath *)indexPath{
    DDAppointmentDetailViewController *appoDetail = [[DDAppointmentDetailViewController alloc]init];
    DDAppointmentCell *cell = (DDAppointmentCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    CourseListModel *courseListModel = self.tableSource[indexPath.section][indexPath.row];
    appoDetail.hrId = courseListModel.hrid;
    appoDetail.cur_id = courseListModel.id;
    appoDetail.timeSlotStr = cell.timeLabel.text;
    [self.navigationController pushViewController:appoDetail animated:YES];
}
//同意拒绝按钮的请求
-(void)agree:(UIButton *)btn{
//    NSInteger index = [self.tableView indexPathForCell:((DDAppointmentCell*)[[[btn superview] superview]superview])].row;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:((DDAppointmentCell*)[[[btn superview] superview]superview])];
    CourseListModel *courseListModel = self.tableSource[indexPath.section][indexPath.row];
    NSString *url = [ NSString stringWithFormat:@"%@%@",publicUrl,feedback];
    NSDictionary *dic;
    if (btn.tag == 222) {
        //同意预约
        dic = @{@"cur_id":courseListModel.id,
                @"hrid":courseListModel.hrid,
                @"type":@(1)
                };
    }else{
        //拒绝预约
        dic = @{@"cur_id"   :courseListModel.id,
                @"hrid"     :courseListModel.hrid,
                @"type"     :@(2)
                };
    }
    [self.request QZRequest_POST:url parameters:dic tagNSString:@"feedback" stopRequest:YES isSerializer:NO];
}
//评价
-(void)goEva:(UIButton *)btn{
//    NSInteger index = btn.tag - 3000;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:((DDAppointmentCell*)[[btn superview]superview])];
    CourseListModel *courseListModel = self.tableSource[indexPath.section][indexPath.row];
    //去评价按钮跳转
    DDGoCommentStu *commentVC = [[DDGoCommentStu alloc]init];
    commentVC.hrId =courseListModel.hrid;
    commentVC.cur_id = courseListModel.id;
    commentVC.company = courseListModel.company;
    commentVC.od_id = courseListModel.od_id;
    [self.navigationController pushViewController:commentVC animated:YES];
}
-(void)createHeadBtn{
    //没数据的视图
    _noListView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([NoListShowView class]) owner:self options:nil] lastObject];
    _noListView.frame = CGRectMake(0, 40, WIDTH, HEIGHT-66);
    [self.view addSubview:_noListView];
    _noListView.hidden = YES;
    //上面的三个按钮
    UIView * bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 44)];
    bgView.backgroundColor =COLOR_DEFAULT;
    _todayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _todayBtn.frame = CGRectMake(0, 0,WIDTH/3, 44);
    [_todayBtn setTitle:NSLocalizedString(@"AppointmentKey", @"") forState:UIControlStateNormal];
    //今日预约
    //Today's appointment
    _todayBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [_todayBtn setTitleColor:COLOR_WHITE_HONT forState:UIControlStateNormal];
    [_todayBtn addTarget:self action:@selector(todayBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:_todayBtn];
    
    _notFinishBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _notFinishBtn.frame = CGRectMake(WIDTH/3, 0,WIDTH/3, 44);
    [_notFinishBtn setTitle:NSLocalizedString(@"NewAppointmentsKey", @"") forState:UIControlStateNormal];
    //New appointments
    //新的预约
    [_notFinishBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _notFinishBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [_notFinishBtn addTarget:self action:@selector(notFinishBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:_notFinishBtn];
    
    _finishBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _finishBtn.frame = CGRectMake(WIDTH/3*2, 0,WIDTH/3, 44);
    [_finishBtn setTitle:NSLocalizedString(@"CompleteReservationKey", @"") forState:UIControlStateNormal];
    
    //Complete the reservation
    // 完成预约
    [_finishBtn setTitleColor:COLOR_WHITE_HONT forState:UIControlStateNormal];
    _finishBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [_finishBtn addTarget:self action:@selector(finishBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:_finishBtn];
    //按钮旁边两条线
    for (int i = 0 ; i<2; i++) {
        UILabel *lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(WIDTH/3*(i+1), 8, 0.5, 44-16)];
        lineLabel.backgroundColor = COLOR_WHITE_HONT;
        [bgView addSubview:lineLabel];
    }
    [self.view addSubview:bgView];

}
- (void)todayBtnClick:(UIButton *)btn{
    //清空数据源
    [self.tableSource removeAllObjects];
    [self.tableView reloadData];
    self.page = 1;
    //今日按钮方法
    [_todayBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_notFinishBtn setTitleColor:COLOR_WHITE_HONT forState:UIControlStateNormal];
    [_finishBtn setTitleColor:COLOR_WHITE_HONT forState:UIControlStateNormal];
    _selectType = @"1";
    [self requestData];
}
- (void)notFinishBtnClick:(UIButton *)btn{
    //清空数据源
    [self.tableSource removeAllObjects];
    [self.tableView reloadData];
    self.page = 1;
    //新的预约按钮
    [_todayBtn setTitleColor:COLOR_WHITE_HONT forState:UIControlStateNormal];
    [_notFinishBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_finishBtn setTitleColor:COLOR_WHITE_HONT forState:UIControlStateNormal];
    _selectType = @"2";
    [self requestData];
}
- (void)finishBtnClick:(UIButton *)btn
{
    //清空数据源
    [self.tableSource removeAllObjects];
    [self.tableView reloadData];
    self.page = 1;
    //完成预约按钮
    _page = 1;
    [_todayBtn setTitleColor:COLOR_WHITE_HONT forState:UIControlStateNormal];
    [_notFinishBtn setTitleColor:COLOR_WHITE_HONT forState:UIControlStateNormal];
    [_finishBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _selectType = @"3";
    [self requestData];
}
#pragma mark ------<网络请求处理>
-(void)request_POST_FinishValue:(id)value tagNSString:(NSString *)tag{
    NSLog(@"value = %@ tag = %@",value,tag);
    //***to do
//    if (test_stub_mode) {
//        value = get_stubbed_data(tag) check(uid);
//    }
    if ([tag isEqualToString:@"course_list"]) {
        NSString *errcode = [value objectForKey:@"status"];
        _noListView.hidden = YES;
        if ([errcode intValue] == 1) {
            total_page = value[@"total_page"];
            total_rows = value[@"total_rows"];
            if (_page == 1||self.page == 1) {
                [self.tableSource removeAllObjects];
            }
            NSMutableArray *courseArr = [[NSMutableArray alloc]init];
            if([value[@"data"] count]>0){
                NSArray *dataArr = value[@"data"];
                for (NSDictionary *dic in dataArr) {
                    CourseListModel *model = [[CourseListModel alloc]initWithDictionary:dic error:nil];
                    [courseArr addObject:model];
                }
            }
            _page++;
            //刷新页面
            [self.tableSource addObject:courseArr];
            [self.tableView reloadData];
        }else{
            [[iToast makeText:[NSString stringWithFormat:@"%@",[value objectForKey:@"msg"]]]show];
            _noListView.hidden = NO;
        }
    }else if([tag isEqualToString:@"feedback"]){
        NSString *errcode = [value objectForKey:@"status"];
        if ([errcode intValue] == 1) {
            //刷新页面
            [[iToast makeText:NSLocalizedString(@"FeedbackKey",@"")]show];
            //Appointment Feedback Success
            //预约反馈成功
            self.page = 1;
            [self requestData];
            [self.tableView reloadData];
        }else{
            [[iToast makeText:[NSString stringWithFormat:@"%@",[value objectForKey:@"msg"]]]show];
        }
    }
}


#pragma mark - 返回
- (void)backAction
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
