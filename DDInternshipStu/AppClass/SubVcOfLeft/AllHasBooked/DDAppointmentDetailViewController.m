//
//  DDAppointmentDetailViewController.m
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/18.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDAppointmentDetailViewController.h"
#import "DDAppointmentDetailHeadView.h"
#import "DDAppointmentDetailEvaCell.h"

#define get_internInfo @"index.php?s=/Home/Student/get_internship_info"
#define getComment @"index.php?s=/Home/Student/Comment"
@interface DDAppointmentDetailViewController ()
{
    DDAppointmentDetailHeadView *headView;
    NSString *_phone;
    UIWebView *_phoneCallWebView;
    NSMutableArray *_tagNumArray;
}
@end

@implementation DDAppointmentDetailViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"InternShipKey", @"");
    //Internship details
    //实习详情
    [self addBtnOnNavWithTitle:nil andImageName:@"返回" andTarget:self andAction:@selector(backAction) andFrame:CGRectMake(0, 0, 18, 15) andDirection:LEFT];
    
    [self createTableViewWithPlainStyle:NO andSeparatorStyleNone:NO andBackGroudImageName:nil];
    [self registCellWithNib:@"DDAppointmentDetailEvaCell" addIdentifier:@"symCell" addHeight:54];
    self.tableView.separatorStyle  = UITableViewCellSeparatorStyleNone;
//    self.tableView.bounces = NO;
    self.tableView.backgroundColor = [UIColor whiteColor];
    _tagNumArray = [[NSMutableArray alloc]init];
    //加载头视图
    [self createTableHeaderView];
    //创建表头视图
    [self createTableLittleView];
    //加载数据
    [self loadDate];
}
-(void)loadDate{
    self.tableView.frame = CGRectMake(0, 230 , WIDTH, HEIGHT - 230 - 64);
    [self.tableSource addObject:@[@"",@"",@"",@"",@""]];
    NSString *url2 = [NSString stringWithFormat:@"%@%@",publicUrl,getComment];
    NSDictionary *dic2 = @{ @"hid":@([self.hrId intValue])
                          };
    [self.request QZRequest_POST:url2 parameters:dic2 tagNSString:@"getComment" stopRequest:NO isSerializer:NO];
}
#pragma mark ------<网络请求>

-(void)request_POST_FinishValue:(id)value tagNSString:(NSString *)tag{
    NSLog(@"%@   %@",value,tag);
    if ([tag isEqualToString:@"get_internInfo"]) {
        NSString *errcode = [value objectForKey:@"status"];
        if ([errcode intValue] == 1) {
            NSDictionary *dic = value[@"data"];
            headView.positionLabel.text = dic[@"post"];
            headView.workContent.text = dic[@"describe"];
            headView.addressLabel.text = dic[@"company_address"];
            headView.companyNameLabel.text = dic[@"company"];
            headView.nameLabel.text = dic[@"name"];
            _phone = dic[@"mobile"];
            [headView.headImage sd_setImageWithURL:[NSURL URLWithString:dic[@"avatar"]] placeholderImage:[UIImage imageNamed:@"默认头像"]];
        }else{
            [[iToast makeText:[NSString stringWithFormat:@"%@",[value objectForKey:@"msg"]]]show];
        }
    }else if([tag isEqualToString:@"getComment"]){
        NSString *errcode = [value objectForKey:@"status"];
        if ([errcode intValue] == 1) {
            NSArray *dic = value[@"data"];
            NSMutableArray * numberArr = [[NSMutableArray alloc]init];
            for (NSDictionary *comDic in dic) {
                for (NSDictionary *tagDic in comDic[@"tags"]) {
                    //获取了15个标签的数字
                    [numberArr addObject:tagDic[@"number"]];
                }
            }
            for (int i = 0; i<15; i+=3) {
                NSArray * array = [numberArr objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(i, 3)]];       
                [_tagNumArray addObject:array];
            }
            for (int i = 0; i<5; i++) {
                DDAppointmentDetailEvaCell *appoEvaCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                NSArray *arr =_tagNumArray[i];
                for (int i = 0; i<3; i++) {
                    UILabel *label = (UILabel *)appoEvaCell.tagNumLabel[i];
                    if([arr[i] intValue]>99){
                    label.text = @"99+";
                    }else{
                    label.text = arr[i];
                    }
                }
            }
            [self.tableView reloadData];
        }else{
            [[iToast makeText:[NSString stringWithFormat:@"%@",[value objectForKey:@"msg"]]]show];
        }
    }
}
-(void)mobile:(UIButton*)btn{
//点击电话按钮
    NSURL *phoneURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",_phone]];
    if ( !_phoneCallWebView ) {
        _phoneCallWebView = [[UIWebView alloc]initWithFrame:CGRectZero];// 这个webView只是一个后台的容易 不需要add到页面上来 效果跟方法二一样 但是这个方法是合法的
    }
    [_phoneCallWebView loadRequest:[NSURLRequest requestWithURL:phoneURL]];
}
-(void)createTableHeaderView
{
    NSString *url = [NSString stringWithFormat:@"%@%@",publicUrl,get_internInfo];
    NSDictionary *dic = @{@"cur_id":@([self.cur_id intValue]),
                          @"hid":@([self.hrId intValue])
                          };
    [self.request QZRequest_POST:url parameters:dic tagNSString:@"get_internInfo" stopRequest:NO isSerializer:NO];
    headView = [[[NSBundle mainBundle]loadNibNamed:@"DDAppointmentDetailHeadView" owner:self options:nil]lastObject];
    headView.timeLabel.text = self.timeSlotStr;
    headView.frame = CGRectMake(0, 0, WIDTH,200);
    [headView.phoneBtn addTarget:self action:@selector(mobile:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:headView];

}
-(void)createTableLittleView{
    UIView *companyHeadView = [[UIView alloc]initWithFrame:CGRectMake(0, headView.frame.size.height, WIDTH, 30)];
    companyHeadView.backgroundColor = [UIColor whiteColor];
    UIImageView *pingjiaImage = [[UIImageView alloc]initWithFrame:CGRectMake(12, 8, 16, 16)];
    pingjiaImage.image = [UIImage imageNamed:@"iconfont-pingjia"];
    [companyHeadView addSubview:pingjiaImage];
    //标题
    UILabel *companyTitle = [[UILabel alloc]initWithFrame:CGRectMake(35, 8, WIDTH - 38, 16)];
    companyTitle.text = NSLocalizedString(@"CompanyEvalutionKey", @"");
    //公司评价
    //Company Evaluation
    [companyHeadView addSubview:companyTitle];
    [self.view addSubview:companyHeadView];
}
#pragma mark ------<tableView>
-(void)loadCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    DDAppointmentDetailEvaCell *appoEvaCell = (DDAppointmentDetailEvaCell *)cell;
    if (indexPath.row == 0) {
        appoEvaCell.leftImage.image = [UIImage imageNamed:@"描述-相符"];
    }else if (indexPath.row == 1){
        appoEvaCell.leftImage.image = [UIImage imageNamed:@"工资"];
    }else if (indexPath.row == 2){
        appoEvaCell.leftImage.image = [UIImage imageNamed:@"雇主"];
    }else if (indexPath.row == 3){
        appoEvaCell.leftImage.image = [UIImage imageNamed:@"工作-环境"];
    }else if (indexPath.row == 4){
        appoEvaCell.leftImage.image = [UIImage imageNamed:@"工作-内容-0"];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.000001;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.000001;
}
#pragma mark - 返回
- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
