//
//  DDStuResumeTableView.m
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/16.
//  Copyright © 2015年 何川. All rights reserved.
//

#import "DDStuResumeTableView.h"
#import "DDStuResumeHeadView.h"
#import "DDStuResumeContentCell.h"
#import "ResumeInfoCell.h"
@interface DDStuResumeTableView ()
{
    DDStuResumeHeadView *headView;
    ResumeInfoCell *resumeCell;
}
@end

@implementation DDStuResumeTableView

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"InternResumeKey", @"");
    [self addBtnOnNavWithTitle:nil andImageName:@"返回" andTarget:self andAction:@selector(backAction) andFrame:CGRectMake(0, 0, 18, 15) andDirection:LEFT];
    //创建头视图
    [self createHeaderView];
    [self createTableViewWithPlainStyle:NO andSeparatorStyleNone:NO andBackGroudImageName:nil];

    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ResumeInfoCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ResumeInfoCell class])];
    [self loadData];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.frame  = CGRectMake(0, 129, WIDTH, HEIGHT - 129-64);
}
-(void)loadData{
    if([DDUserInfo intern] == nil||[DDUserInfo introduction] == nil||[DDUserInfo skill] == nil || [DDUserInfo major] == nil){
    //如果为空进行网络请求
        NSString *urlstr = [NSString stringWithFormat:@"%@%@",publicUrl,@"index.php?s=/Home/Student/get_resume"];
        NSDictionary *dic = @{@"student_id":[DDUserInfo uid]};
        [self.request QZRequest_POST:urlstr parameters:dic tagNSString:@"get_resume" stopRequest:YES isSerializer:NO isCache:YES];
        return;
    }else{
        [self.tableSource addObject:@[[DDUserInfo introduction]]];
        [self.tableSource addObject:@[[DDUserInfo major]]];
        [self.tableSource addObject:@[[DDUserInfo intern]]];
        [self.tableSource addObject:@[[DDUserInfo skill]]];
        [self.tableView reloadData];
    }
}
#pragma mark requestPool
-(void)request_POST_FinishValue:(id)value tagNSString:(NSString *)tag
{
    NSLog(@"value = %@",value);
    NSString *errcode = [value objectForKey:@"status"];
    if ([errcode intValue] == 1) {
        [[TMCache sharedCache] setObject:value[@"data"][@"intern"] forKey:@"intern"];
        [[TMCache sharedCache] setObject:value[@"data"][@"major"] forKey:@"major"];
        [[TMCache sharedCache] setObject:value[@"data"][@"introduction"] forKey:@"introduction"];
        [[TMCache sharedCache] setObject:value[@"data"][@"skill"] forKey:@"skill"];
        [self.tableSource addObject:@[[DDUserInfo intern]]];
        [self.tableSource addObject:@[[DDUserInfo major]]];
        [self.tableSource addObject:@[[DDUserInfo introduction]]];
        [self.tableSource addObject:@[[DDUserInfo skill]]];
        [self.tableView reloadData];
    }else{
        NSString *msg = [value objectForKey:@"msg"];
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"PromptKey", @"") message:msg delegate:self cancelButtonTitle:NSLocalizedString(@"OkKey", @"") otherButtonTitles:nil];
        [alertView show];
    }
}
//加载头视图
-(void)createHeaderView{
    headView = [[[NSBundle mainBundle]loadNibNamed:@"DDStuResumeHeadView" owner:self options:nil]lastObject];
    headView.frame = CGRectMake(0, 0, WIDTH, 129);
    headView.backgroundColor = COLOR_DEFAULT;
    if ([[DDUserInfo studentSex]isEqualToString:@"1"]) {
        headView.sexLabel.text = NSLocalizedString(@"maleKey", @"");
    }else{
        headView.sexLabel.text =NSLocalizedString(@"FemaleKey", @"");
    }
    if ([[[TMCache sharedCache]objectForKey:@"degree"] isEqualToString:@"1"]) {
        headView.degree.text = NSLocalizedString(@"courseKey", @"");
    }else{
        headView.degree.text = NSLocalizedString(@"PostgraduateKey", "");
    }
    headView.schoolLabel.text = [DDUserInfo studentSchool];
    if ([[TMCache sharedCache]objectForKey:@"cityStr"] == nil||[[[TMCache sharedCache]objectForKey:@"cityStr"]isKindOfClass:[NSNull class]]||[[[TMCache sharedCache]objectForKey:@"cityStr"]isEqualToString:NSLocalizedString(@"OtherKey", @"")]){
        headView.workAddress.text = NSLocalizedString(@"OtherKey", @"");
    }else{
        headView.workAddress.text = [NSString stringWithFormat:@"%@ %@",[[TMCache sharedCache]objectForKey:@"cityStr"],[[TMCache sharedCache]objectForKey:@"district"]];
    }
    [self.view addSubview:headView];
}
#pragma mark - UITableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    resumeCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ResumeInfoCell class])];
    resumeCell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (resumeCell == nil) {
        resumeCell = [[ResumeInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NSStringFromClass([ResumeInfoCell class])];
    }
    
    [resumeCell setIntroductionText:self.tableSource[indexPath.section][indexPath.row]];
    if (indexPath.section == 0) {
        resumeCell.titleImage.image = [UIImage imageNamed:@"自我介绍"];
        resumeCell.titleLabel.text =NSLocalizedString(@"SelfIntroKey", @"");
    }
    if (indexPath.section == 1) {
        resumeCell.titleImage.image = [UIImage imageNamed:@"专业"];
        resumeCell.titleLabel.text = NSLocalizedString(@"ProfessionalsKey", @"");
    }
    if (indexPath.section == 2) {
        resumeCell.titleImage.image = [UIImage imageNamed:@"图层-9"];
        resumeCell.titleLabel.text = NSLocalizedString(@"AssociationsKey", @"");
    }
    if (indexPath.section == 3) {
        resumeCell.titleImage.image = [UIImage imageNamed:@"技能"];
        resumeCell.titleLabel.text = NSLocalizedString(@"SkillRewardKey", @"");
    }
    
    return resumeCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    resumeCell = (ResumeInfoCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return resumeCell.frame.size.height;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.000001;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.000001;
}
- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
