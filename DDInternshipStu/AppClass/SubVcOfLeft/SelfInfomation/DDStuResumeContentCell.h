//
//  DDStuResumeContentCell.h
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/16.
//  Copyright © 2015年 何川. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDStuResumeContentCell : UITableViewCell
/** 图片 */
@property (weak, nonatomic) IBOutlet UIImageView *contentImage;
/** 名字 */
@property (weak, nonatomic) IBOutlet UILabel *contentCellName;
/** 内容 */
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end
