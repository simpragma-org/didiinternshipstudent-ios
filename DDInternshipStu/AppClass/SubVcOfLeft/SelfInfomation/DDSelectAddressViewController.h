//
//  DDSelectAddressViewController.h
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/20.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "QZBaseViewController.h"

@interface DDSelectAddressViewController : QZBaseViewController
/** 已选地址 */
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
/** tableView的内容数组 */
@property (nonatomic, strong) NSMutableArray *tableDataSource;
/** collection的内容数组 */
@property (nonatomic, strong) NSMutableArray *collectionDataSource;
/** collectionView */
@property (nonatomic, strong) UICollectionView *selectCollection;
@end
