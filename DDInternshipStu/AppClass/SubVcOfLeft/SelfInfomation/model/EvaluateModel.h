//
//  EvaluateModel.h
//  shixi
//
//  Created by 何川 on 15/8/1.
//  Copyright (c) 2015年 shixi.Team. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EvaluateModel : NSObject
//id = 7;
//"tag_name" = "\U975e\U5e38\U719f\U7ec3";
//used = 11;
//user = "\U8bad\U7ec3\U6761\U76ee";

@property (strong,nonatomic)NSString *id;
@property (strong,nonatomic)NSString *tag_name;
@property (strong,nonatomic)NSString *used;
@property (strong,nonatomic)NSString *user;

@property (strong,nonatomic)NSString *uid;
@property (strong,nonatomic)NSString *tag_id;
@property (strong,nonatomic)NSString *group;
@property (strong,nonatomic)NSString *number;

@property (assign,nonatomic)BOOL isSeleted;//是否是已选择
@end
