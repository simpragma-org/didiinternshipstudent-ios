//
//  CommentListModel.h
//  DDIntershipStu
//
//  Created by HERO-PC on 15/12/3.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "JSONModel.h"

@interface CommentListModel : JSONModel
/** 名字 */
@property (nonatomic, copy) NSString *name;
/** 评论 */
@property (nonatomic, copy) NSString *comment;
/** 时间 */
@property (nonatomic, copy) NSString *add_time;
/** 分数 */
@property (nonatomic, copy) NSString *score;

@end
