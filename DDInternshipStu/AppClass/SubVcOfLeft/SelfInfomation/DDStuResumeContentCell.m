//
//  DDStuResumeContentCell.m
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/16.
//  Copyright © 2015年 何川. All rights reserved.
//

#import "DDStuResumeContentCell.h"

@implementation DDStuResumeContentCell

- (void)awakeFromNib {
    // Initialization code
    _contentCellName.text= NSLocalizedString(@"selfintroductionKey", @"");
    _contentLabel.text = NSLocalizedString(@"ContentKey", @"");
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
