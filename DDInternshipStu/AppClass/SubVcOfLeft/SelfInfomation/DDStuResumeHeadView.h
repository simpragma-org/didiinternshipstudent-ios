//
//  DDStuResumeHeadView.h
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/16.
//  Copyright © 2015年 何川. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDStuResumeHeadView : UITableViewCell
/** 工作地址 */
@property (weak, nonatomic) IBOutlet UILabel *workAddress;
/** 学校 */
@property (weak, nonatomic) IBOutlet UILabel *schoolLabel;
/** 在读 */
@property (weak, nonatomic) IBOutlet UILabel *degree;
/** 性别 */
@property (weak, nonatomic) IBOutlet UILabel *sexLabel;
@end
