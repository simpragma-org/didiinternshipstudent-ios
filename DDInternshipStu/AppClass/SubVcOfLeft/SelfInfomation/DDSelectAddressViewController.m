//
//  DDSelectAddressViewController.m
//  DDInternshipStu
//
//  Created by HERO-PC on 15/11/20.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDSelectAddressViewController.h"
#import "AppDelegate.h"
#import "DDSelectAddDetailCell.h"
#import "DDSelectAddressCell.h"
#import "CityModel.h"
#import "Reachability.h"
#define getAddress @"index.php?s=/Home/Common/get_district_list"
#define makeAddress @"index.php?s=/Home/Student/set_work_range"

@interface DDSelectAddressViewController ()<UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    NSInteger _selectTag;
    UITableView *_addDetailView;
    UIView *_alertView;
    NSString *_selectCity;
    NSArray *_allName;
    NSArray *_selectDicArray;
    NSInteger _selecCityId;
    NSInteger _selecDisId;
}
@end

@implementation DDSelectAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title =NSLocalizedString(@"SelectAddressKey", @"");
    //选择地址
    //Select Address
    [self addBtnOnNavWithTitle:nil andImageName:@"返回" andTarget:self andAction:@selector(backAction) andFrame:CGRectMake(0, 0, 18, 15) andDirection:LEFT];
    _selectTag = -1;
    _selecCityId = -10;
    _selecDisId = -10;
    //建立collectionView
    [self setCollectionView];
    [self loadData];
}
-(void)setCollectionView{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [flowLayout setItemSize:CGSizeMake((WIDTH - 16 - 10)/2, 40)];
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
     self.selectCollection = [[UICollectionView alloc]initWithFrame:CGRectMake(8, 56, WIDTH - 16, HEIGHT - 56) collectionViewLayout:flowLayout];
    self.selectCollection.delegate = self;
    self.selectCollection.dataSource = self;
    self.selectCollection.backgroundColor = [UIColor whiteColor];
    UINib *cellNib = [UINib nibWithNibName:@"DDSelectAddressCell" bundle:nil];
    [self.selectCollection registerNib:cellNib forCellWithReuseIdentifier:@"selectCell"];
    [self.view addSubview:self.selectCollection];
}
-(void)loadData{
    self.tableDataSource = [[NSMutableArray alloc]init];
    self.collectionDataSource = [[NSMutableArray alloc]init];
    NSString *url = [NSString stringWithFormat:@"http://c.simpragma.com:18080/internserver/Home/Common/get_district_list?secret_key=C8AEF3FD988AF5F6"];
    [self.request QZRequest_POST:url parameters:nil tagNSString:@"get_district_list" stopRequest:YES isSerializer:NO];
}
#pragma mark ------<网络请求>
-(void)request_POST_FinishValue:(id)value tagNSString:(NSString *)tag
{
    NSLog(@"value%@,tag:%@",value,tag);
    if ([tag isEqualToString:@"get_district_list"]) {
        NSString *errcode = [value objectForKey:@"status"];
        if ([errcode intValue] == 1) {
            NSArray *cityDataArr = value[@"data"];
            NSMutableArray *cityArr = [[NSMutableArray alloc]init];
            for (NSDictionary *dic in cityDataArr) {
                CityModel *model = [[CityModel alloc] initWithDictionary:dic error:nil];
                [cityArr addObject:model.name];
                [self.collectionDataSource addObject:model];
                [self.selectCollection reloadData];
            }
            NSMutableArray * dicAllName = [[NSMutableArray alloc]init];
            for ( int i = 0 ; i<cityDataArr.count; i++) {
                NSMutableArray *dictrictNameArr = [[NSMutableArray alloc]init];
                for (NSDictionary *dic in cityDataArr[i][@"district"]) {
                    CityModel *model3 = [[CityModel alloc]initWithDictionary:dic error:nil];
                    [dictrictNameArr addObject: model3.name];
                }
                [dicAllName addObject:dictrictNameArr];
            }
            _allName = dicAllName;
        }else{
            NSString *msg = [value objectForKey:@"msg"];
            [[iToast makeText:msg]show];
        }
    }else if ([tag isEqualToString:@"chang_address"]){
        NSString *errcode = [value objectForKey:@"status"];
        if ([errcode intValue] == 1) {
            //更改成功返回界面
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            NSString *msg = [value objectForKey:@"msg"];
            [[iToast makeText:msg]show];
        }
    }
}
-(void)setTableView{
    _alertView = [[UIAlertView alloc]initWithFrame:CGRectMake(0,0,WIDTH,HEIGHT)];
    _alertView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapView)];
    [_alertView addGestureRecognizer:tap];
    [self.view.window addSubview:_alertView];
    
    _addDetailView = [[UITableView alloc]initWithFrame:CGRectMake(60, 115, WIDTH - 60*2, HEIGHT - 115 - 100) style:UITableViewStylePlain];
    _addDetailView.delegate = self;
    _addDetailView.dataSource = self;
    _addDetailView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [_addDetailView registerNib:[UINib nibWithNibName:@"DDSelectAddDetailCell" bundle:nil] forCellReuseIdentifier:@"addDetailCell"];
    [self.view.window addSubview:_addDetailView];
}
-(void)tapView{
    _alertView.hidden = YES;
    _addDetailView.hidden = YES;
    

}
#pragma mark ------<tableVIew>
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  [_allName[_selectTag] count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * addName = @"addDetailCell";
    DDSelectAddDetailCell * cell = [tableView dequeueReusableCellWithIdentifier:addName];
    if (!cell) {
        cell = [[DDSelectAddDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addName];
    }
    cell.selectedArrow.hidden = YES;
    cell.detailLabel.text = _allName[_selectTag][indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _addDetailView.userInteractionEnabled = NO;
    [[iToast makeText:NSLocalizedString(@"Select a city success", @"")]show];
    DDSelectAddDetailCell *cell = (DDSelectAddDetailCell *)[_addDetailView cellForRowAtIndexPath:indexPath];
    cell.selectedArrow.hidden = NO;
    cell.detailLabel.textColor = COLOR_DEFAULT;
    //延迟1秒隐藏tableView
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        _alertView.hidden = YES;
        _addDetailView.hidden = YES;
        CityModel *model = self.collectionDataSource[_selectTag];
        self.addressLabel.text = [NSString stringWithFormat:@"%@ %@",model.name,cell.detailLabel.text];
        [[TMCache sharedCache] setObject:model.name forKey:@"cityStr"];
        [[TMCache sharedCache] setObject:cell.detailLabel.text forKey:@"district"];
    });
    CityModel *selectedCityModel = self.collectionDataSource[_selectTag];
    _selecCityId = selectedCityModel.id;
    CityModel *selectedDisModel = selectedCityModel.district[indexPath.row];
    _selecDisId = selectedDisModel.id;
    NSLog(@"%ld  %ld",(long)_selecCityId,(long)_selecDisId);
    
}
#pragma mark ------<collectionView>
//每个section的item个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.collectionDataSource.count;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
//每个UICollectionView展示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * CellIdentifier = @"selectCell";
    DDSelectAddressCell *cell = (DDSelectAddressCell *)[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    CityModel *model = self.collectionDataSource[indexPath.row];
    cell.addressLabel.text = model.name;
    if (model.district.count == 0) {
        cell.arrowImage.hidden = YES;
    }
    return cell;
}
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    //遍历所有item 选中的按钮改变
    for (int i = 0; i< 12; i++) {
        NSIndexPath *index = [NSIndexPath indexPathForRow:i inSection:0];
        DDSelectAddressCell * cell = (DDSelectAddressCell *)[collectionView cellForItemAtIndexPath:index];
        if (i == indexPath.row) {
            cell.arrowImage.image = [UIImage imageNamed:@"iconfont-jiantou-拷贝-4"];
            cell.bgImage.image = [UIImage imageNamed:@"蓝色"];
            cell.addressLabel.textColor = [UIColor whiteColor];
        }else{
            cell.arrowImage.image = [UIImage imageNamed:@"iconfont-jiantou-拷贝-5"];
            cell.bgImage.image = [UIImage imageNamed:@"灰色框"];
            cell.addressLabel.textColor = [UIColor colorWithHexString:@"#232323"];
        }
    }
    _selectTag = indexPath.row;
    CityModel *model = self.collectionDataSource[_selectTag];
    if (model.district.count == 0) {
        self.addressLabel.text = [NSString stringWithFormat:@"%@",model.name];
        _selecCityId = model.id;
        _selecDisId = -1;
        [[TMCache sharedCache] setObject:@"其他" forKey:@"cityStr"];
        [[TMCache sharedCache] setObject:@" " forKey:@"district"];
        NSLog(@"%ld  %ld",(long)_selecCityId,(long)_selecDisId);
    }else{
        [self setTableView];
    }
}
//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
- (void)backAction
{
    if (![self isNetworkConnected]||_selecDisId == -10||_selecCityId == -10||[self.addressLabel.text isEqualToString:@""]||[self.addressLabel.text isEqualToString:NSLocalizedString(@"ScopeKey", @"")]) {
        //请选择你的工作范围
        //Please choose your scope of work
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    //返回键的时候请求
    NSString *url = [NSString stringWithFormat:@"%@%@",publicUrl,makeAddress];
    int district;
    if (_selecDisId == -1) {
        district = -1;
    }else{
        district = (int)_selecDisId ;
    }
    NSDictionary *dic = @{
                          @"student_id":[DDUserInfo uid],
                          @"city":@(_selecCityId),
                          @"district":@(district)
                          };
    [self.request QZRequest_POST:url parameters:dic tagNSString:@"chang_address" stopRequest:YES isSerializer:NO];
}
- (BOOL)isNetworkConnected {
    Reachability *r = [Reachability reachabilityWithHostName:@"www.baidu.com"];
    switch ([r currentReachabilityStatus]) {
        case NotReachable:
            return NO;
            break;
        case ReachableViaWWAN:
        case ReachableViaWiFi:
            return YES;
            break;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
