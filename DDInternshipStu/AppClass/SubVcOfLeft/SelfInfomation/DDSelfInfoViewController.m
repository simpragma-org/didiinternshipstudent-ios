//
//  DDSelfInfoViewController.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/17.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "DDSelfInfoViewController.h"
#import "StudentInfoHeadView.h"
#import "DDStuResumeTableView.h"
#import "DDSelectAddressViewController.h"
#import "AllJudgeCell.h"
#import "StudentCommentCell.h"
#import "CommnetCell.h"
#import "UIImage+size.h"
#import "UIViewController+tool.h"
#import "CommentListModel.h"
#import <AVFoundation/AVFoundation.h>
#define Man 1
#define Woman 0
//更换头像接口
#define setHead @"index.php?s=/Home/Student/set_avatar"
//获取个人信息接口
#define get_person @"index.php?s=/Home/Student/get_person"
//获取评论列表
#define get_comment_list @"index.php?s=/Home/Hr/get_comment_list"
//获取评论标签
#define get_comment @"index.php?s=/Home/Hr/get_hr_comment"
@interface DDSelfInfoViewController ()<UIActionSheetDelegate,UIImagePickerControllerDelegate>
{
    StudentInfoHeadView *headView;
    NSString *name;
    NSString *personGender;
    UIImage *_headImage; //头像
    UIView *_noDataView;
    NSMutableArray *_tagArr;
    UIImageView *nodata;
    NSMutableArray *_commentArr;
    NSString *_allScore;//整体星级;
//    CommnetCell *commentCell;
    NSMutableArray *commentCellHeightArr;
    CGFloat tagsViewHeight;
}
@end

@implementation DDSelfInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"PersonalInformationKey", @"");
    //Personal information
    //个人信息
//    [self addBtnOnNavWithTitle:nil andImageName:@"返回" andTarget:self andAction:@selector(backAction) andFrame:CGRectMake(0, 0, 18, 15) andDirection:LEFT];
    [self createTableViewWithPlainStyle:NO andSeparatorStyleNone:NO andBackGroudImageName:nil];
    
    [self getPersonInfomation];
    
     [self createHeaderView];
    commentCellHeightArr = [[NSMutableArray alloc] initWithCapacity:10];
    //公司评价
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([AllJudgeCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([AllJudgeCell class])];
    //评价标签
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([StudentCommentCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([StudentCommentCell class])];
    //评论cell
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CommnetCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([CommnetCell class])];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    _noDataView  = [[UIView alloc]initWithFrame:CGRectMake(0, headView.frame.size.height, WIDTH, self.tableView.frame.size.height)];
    _noDataView.backgroundColor = [UIColor colorWithHexString:@"#EFF0F1"];
    nodata = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"没有评价"]];
    nodata.frame = CGRectMake((_noDataView.frame.size.width - 115)/2, (_noDataView.frame.size.height - 115)/2, 115, 115);
    [_noDataView addSubview:nodata];
    [self.view addSubview:_noDataView];
    _noDataView.hidden = YES;
    [self getWebRequest];
}
#pragma mark - 返回 页面功能
- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
- (void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}
//创建头
- (void)createHeaderView
{
    //NSString *url = [NSString stringWithFormat:@"%@%@",publicUrl,get_person];
    NSString *url = [NSString stringWithFormat:@"http://c.simpragma.com:18080/internserver/Home/Student/get_person?secret_key=C8AEF3FD988AF5F6"];
    NSDictionary *dic = @{
                          // #######@"student_id":[DDUserInfo uid]
                          @"student_id":@"1"
                          };
    [self.request QZRequest_POST:url parameters:dic tagNSString:@"get_person" stopRequest:NO isSerializer:NO isCache:YES];
    headView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([StudentInfoHeadView class]) owner:self options:nil] lastObject];
    [headView.backGroundView setBackgroundColor:COLOR_DEFAULT];
    if([DDUserInfo headerImage]){
        [headView.headBtn setBackgroundImage:[DDUserInfo headerImage] forState:UIControlStateNormal];
    }else{
        if ([DDUserInfo headerImageUrl]) {
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:[DDUserInfo headerImageUrl]
                                  options:0
                                 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                     // progression tracking code
                                 }
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                    if (image) {
                                        // do something with image
                                        [DDUserInfo headerImage];
                                        [headView.headBtn setBackgroundImage:image forState:UIControlStateNormal];
                                    }
                                }];
        }
    }
    [headView createNameLablelWithName:[DDUserInfo name] andGender:personGender andAge:[DDUserInfo studentAge]];
    headView.schoolLabel.text = [DDUserInfo studentSchool];
    if ([DDUserInfo major]) {
        headView.majorLabel.text = [DDUserInfo major];
    }
    if ([[TMCache sharedCache]objectForKey:@"hour"]) {
        headView.totalHourLabel.text = [NSString stringWithFormat:@"%@h",[[TMCache sharedCache]objectForKey:@"hour"]];
    }
    [headView.backBtn addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [headView.resumeBtn addTarget:self action:@selector(checkPersonResume) forControlEvents:UIControlEventTouchUpInside];
    [headView.addressSelBtn addTarget:self action:@selector(checkAddressList) forControlEvents:UIControlEventTouchUpInside];
    headView.frame = CGRectMake(0, 0, WIDTH, 254);
    [headView.headBtn addTarget:self action:@selector(headAction:) forControlEvents:UIControlEventTouchUpInside];
    headView.headBtn.tag = 100;
    [self.view addSubview:headView];
    
    self.tableView.frame = CGRectMake(0, 254 - 20, WIDTH, HEIGHT-254+20);
}
#pragma mark requestPool
-(void)request_POST_FinishValue:(id)value tagNSString:(NSString *)tag
{
    NSLog(@"value = %@ tag = %@",value,tag);
    NSString *errcode = [value objectForKey:@"status"];
    NSDictionary *data = [value objectForKey:@"data"];
    if ([tag isEqualToString:@"get_person"]) {
        if ([errcode intValue] == 1) {
            NSString *hour = [NSString stringWithFormat:@"%@",[data objectForKey:@"duration"]];
            NSString *hourStr;
            if ([hour isEqualToString:@"0"]) {
                hourStr = @"0";
            }else{
                hourStr = [hour substringToIndex:[hour length] - 3];
            }
            
            NSDictionary *dic = @{@"mobile":[data objectForKey:@"mobile"],
                                  @"degree":[data objectForKey:@"degree"],
                                  @"major":[data objectForKey:@"major"],
                                  @"studentAge":[data objectForKey:@"age"],
//                                  @"class_hour":[data objectForKey:@"class_hour"],
                                  @"cityStr":[data objectForKey:@"city"],
                                  @"district":[data objectForKey:@"district"],
                                  @"hour":hourStr
                                  };
            [DDUserInfo keepUserMessageWithDic:dic];
            headView.majorLabel.text = [DDUserInfo major];
            headView.totalHourLabel.text = [NSString stringWithFormat:@"%@h",[[TMCache sharedCache]objectForKey:@"hour"]];
        }else{
            [[iToast makeText:[value objectForKey:@"msg"]]show];
        }
    }else if ([tag isEqualToString:@"get_comment"]){
        if ([errcode intValue] == 1) {
            if (self.tableSource == nil || self.tableSource.count == 0) {
                [self loadData];
            }
            NSDictionary *dic = value[@"data"];
            NSArray *arr = dic[@"tags"];
            _allScore = dic[@"score"];
            if (arr.count == 0) {
                _noDataView.hidden = NO;
                nodata.image = [UIImage imageNamed:@"没有评价"];
                return;
            }
            NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:1];
            StudentCommentCell *cell = (StudentCommentCell *)[self.tableView cellForRowAtIndexPath:index];
            [cell creatLabelView:dic labelArray:arr];
            tagsViewHeight = cell.frame.size.height;
            //请求评论
            NSString *url2 = [NSString stringWithFormat:@"%@%@",publicUrl,get_comment_list];
            NSDictionary *dic2 = @{@"uid":[DDUserInfo uid]};
            [self.request QZRequest_POST:url2 parameters:dic2 tagNSString:@"get_comment_list" stopRequest:NO isSerializer:NO isCache:YES];
            }else{
                //显示没有评价的视图
                _noDataView.hidden = NO;
            }
    }else if ([tag isEqualToString:@"get_comment_list"]){
        if ([errcode intValue] == 1) {
            NSArray *arr = value[@"data"];
            if (arr.count == 0) {
                _noDataView.hidden = NO;
                nodata.image = [UIImage imageNamed:@"没有评价"];
                return;
            }
            _commentArr = [[NSMutableArray alloc]init];
            for (NSDictionary *dic in arr) {
                CommentListModel *model = [[CommentListModel alloc]initWithDictionary:dic error:nil];
                [_commentArr addObject:model];
            }
            for (int i = 0; i<_commentArr.count; i++) {
                [commentCellHeightArr addObject:@(0)];
            }
            if (self.tableSource == nil || self.tableSource.count == 0) {
                [self loadData];
            }
            [self.tableSource addObject:_commentArr];
            [self.tableView reloadData];
            
        }else{
            //显示没有评价的视图
            _noDataView.hidden = NO;
        }
    }
}
//学生简历
- (void)checkPersonResume{
    DDStuResumeTableView *resumeVc = [[DDStuResumeTableView alloc] init];
    [self.navigationController pushViewController:resumeVc animated:YES];
}
//选择地点
-(void)checkAddressList{
    DDSelectAddressViewController *addSelectView = [[DDSelectAddressViewController alloc]init];
    [self.navigationController pushViewController:addSelectView animated:YES];
}
- (void)loadData
{
    [self.tableSource removeAllObjects];
    [self.tableSource addObject:@[@""]];//公司评价
    [self.tableSource addObject:@[@""]];
    [self.tableSource addObject:@[@""]];//整体评价
//    [self.tableSource addObject:@[@""]];
    [self.tableView reloadData];
}
- (void)getWebRequest
{
    //请求标签
    NSString *url = [NSString stringWithFormat:@"%@%@",publicUrl,get_comment];
    NSDictionary *dic = @{@"uid":[DDUserInfo uid]
                          };
    [self.request QZRequest_POST:url parameters:dic tagNSString:@"get_comment" stopRequest:NO isSerializer:NO isCache:YES];
}
#pragma mark - UITableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        //公司评价
        AllJudgeCell *allCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AllJudgeCell class])];
        allCell.titleImage.image = [UIImage imageNamed:@"iconfont-pingjia"];
        allCell.titleLabel.text = NSLocalizedString(@"CompanyEvalutionKey", @"");
        allCell.starOne.hidden = YES;
        allCell.starTwo.hidden = YES;
        allCell.starThree.hidden = YES;
        allCell.starFour.hidden = YES;
        allCell.starFive.hidden = YES;
        allCell.selectionStyle = UITableViewCellSelectionStyleNone;
        return allCell;
    }
    if (indexPath.section == 1)
    {
        //标签
        StudentCommentCell *labelCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([StudentCommentCell class])];
        // [self creatLabelView:(NSDictionary*)_labelInfDic labelArray:(NSArray*)_arr];
        labelCell.selectionStyle = UITableViewCellSelectionStyleNone;
        return labelCell;
        
    }
    if (indexPath.section == 2)
    {
        //整体评价
        AllJudgeCell *totalCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AllJudgeCell class])];
        totalCell.titleImage.image = [UIImage imageNamed:@"整体评价"];
        totalCell.titleLabel.text = NSLocalizedString(@"OverAllKey", @"");
        //整体评价
        //
        totalCell.selectionStyle = UITableViewCellSelectionStyleNone;
        [totalCell createCellHasStar:YES andStar:_allScore];
        return totalCell;
    }
    //评论cell
    CommnetCell *commentCell = [tableView dequeueReusableCellWithIdentifier:@"CommnetCell" forIndexPath:indexPath];
    commentCell.selectionStyle = UITableViewCellSelectionStyleNone;
    CommentListModel *model = _commentArr[indexPath.row];
    
    [commentCell loadCellWithModel:model];
    
    return commentCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.00000001;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.00000001;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 3) {
        CommentListModel *model = _commentArr[indexPath.row];
       return [self heightOfCellWithModel:model];
    }else if (indexPath.section == 1){
        return tagsViewHeight;
    }
    else{
        return 40;
    }
}
//获得cell的高度
-(CGFloat)heightOfCellWithModel:(CommentListModel *)model
{
    
    CGSize labelSize  = [model.comment boundingRectWithSize:CGSizeMake(WIDTH - 30, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]} context:nil].size;
    return labelSize.height + 45;
}
#pragma mark - 链接网络
//联网获取用户信息
- (void)getPersonInfomation
{
    if([[DDUserInfo studentSex] isEqualToString:@"1"]){
        personGender = @"1";
    }else{
        personGender = @"2";
    }
}
#pragma mark - 修改头像
- (void)headAction:(UIButton *)sender
{
    //修改头像
    UIActionSheet *sheet;
    // 判断是否支持相机
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        sheet  = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"CancelKey", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"PhotograpghKey", @""),NSLocalizedString(@"GallaryKey", @""), nil];
    }else {
        sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"CancelKey", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"GallaryKey", @""), nil];
    }
    sheet.tag = 266;
    [sheet showInView:self.view];
}
#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 266) {
        __block NSUInteger sourceType = 0;
        // 判断是否支持相机
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            switch (buttonIndex) {
                case 0:
                    {
                        // 相机
                        //判断相机是否能够使用
                        AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
                        if (status == AVAuthorizationStatusAuthorized) {
                            //authorized 授权
                            sourceType = UIImagePickerControllerSourceTypeCamera;
                            
                        } else if (status == AVAuthorizationStatusDenied) {
                            //denied 拒绝
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"NotcameraKey", @"") message:NSLocalizedString(@"Privacykey", "") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OkKey", @""), nil];
                            [alert show];
                            return;
                            //            } else if (status == AVAuthorizationStatusRestricted || ) {
                            //restricted 限制
                        }
                    }
                    break;
                    
                case 1:
                    // 相册
                    sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    break;
                    
                case 2:
                    // 取消
                    return;
            }
        }else {
            if (buttonIndex == 0) {
                if (IS_IOS7) {
                    //判断相机是否能够使用
                    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
                    if (status == AVAuthorizationStatusAuthorized) {
                        //authorized 授权
                        sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                        
                    } else if (status == AVAuthorizationStatusDenied) {
                        //denied 拒绝
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"NotcameraKey", @"") message:NSLocalizedString(@"Privacykey", "") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OkKey", @""), nil];
                        [alert show];
                        return;
                        //            } else if (status == AVAuthorizationStatusRestricted || ) {
                        //restricted 限制
                    } else if (status == AVAuthorizationStatusRestricted ||status == AVAuthorizationStatusNotDetermined) {
                        //not determined 不确定
                        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                            if (granted) {
                                // OK
                                sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                                
                            } else {
                                return;
                            }
                        }];
                    }
                }
            } else {
                return;
            }
        }
        // 跳转到相机或相册页面
        [self createImagePickerControllerWithSourceType:sourceType];
    }
}

#pragma mark - image picker delegte
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{}];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    
    /* 此处info 有六个值
     * UIImagePickerControllerMediaType; // an NSString UTTypeImage)
     * UIImagePickerControllerOriginalImage;  // a UIImage 原始图片
     * UIImagePickerControllerEditedImage;    // a UIImage 裁剪后图片
     * UIImagePickerControllerCropRect;       // an NSValue (CGRect)
     * UIImagePickerControllerMediaURL;       // an NSURL
     * UIImagePickerControllerReferenceURL    // an NSURL that references an asset in the AssetsLibrary framework
     * UIImagePickerControllerMediaMetadata    // an NSDictionary containing metadata from a captured photo
     */
    
    UIButton *headBth = (UIButton *)[self.view viewWithTag:100];
    [headBth setBackgroundImage:image forState:UIControlStateNormal];
    // 保存图片至本地，方法见下文
    [self saveImage:image withName:@"123.png"];
    //上传头像
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",publicUrl,setHead];
    //压缩图片
    UIImage *newImage = [image imageScaledToSize:CGSizeMake(319, 213)];
    _headImage = newImage;
    [headView.headBtn setBackgroundImage:newImage forState:UIControlStateNormal];
    NSData *imageData = UIImageJPEGRepresentation(newImage,0.7);//转二进制
    NSDictionary *parmDic = @{@"uid":[DDUserInfo uid]};
    [self.request QZRequest_Upload_POST:urlStr parameters:parmDic imageData:imageData pathKey:@"file" tagNSString:@"set_avatar" stopRequest:YES isSerializer:NO];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}
#pragma mark - 保存图片至沙盒
- (void) saveImage:(UIImage *)currentImage withName:(NSString *)imageName
{
    
    NSData *imageData = UIImageJPEGRepresentation(currentImage, 0.7);
    // 获取沙盒目录
    
    NSString *fullPath = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] stringByAppendingPathComponent:imageName];
    // 将图片写入文件
    NSLog(@"fullPath = %@",fullPath);
    [imageData writeToFile:fullPath atomically:NO];
}
#pragma mark - 联网处理
- (void)request_Upload_POST_FinishValue:(id)value tagNSString:(NSString *)tag
{
    NSLog(@"value:%@,tag:%@",value,tag);
    if ([value[@"status"] intValue] == 0) {
        [iToast makeText:[NSString stringWithFormat:@"%@",value[@"data"]]];
    }
    
    if ([tag isEqualToString:@"set_avatar"]) {
        if ([value[@"status"] intValue] == 1) {
            NSString *head_img_url = value[@"data"][@"avatar"];
            //保存头像URL
            [[TMCache sharedCache] setObject:head_img_url forKey:@"headerImageUrl"];
            [[TMCache sharedCache] setObject:_headImage forKey:@"headerImage"];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
