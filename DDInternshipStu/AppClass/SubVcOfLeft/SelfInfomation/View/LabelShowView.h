//
//  LabelShowView.h
//  DDInternshipStu
//
//  Created by 何川 on 15/12/3.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LabelShowView : UIView
/*标签标题**/
@property (strong, nonatomic) UILabel* titleLabel;
/*标签数量**/
@property (strong, nonatomic) UILabel* numberLabel;
/*标题背景颜色**/
@property (strong, nonatomic) UIColor* titleBackgroundColor;
/**标签数量背景颜色*/
@property (strong, nonatomic) UIColor* numberBackgroundColor;
/*标签宽度**/
@property (assign, nonatomic) CGFloat labelWidth;
/*标签评价数量**/
@property (strong, nonatomic) NSString* commentNumberStr;

@property (strong ,nonatomic) UIButton *btn;
- (instancetype)initWithFrame:(CGRect)frame andMode:(NSString *)titleName;
@end
