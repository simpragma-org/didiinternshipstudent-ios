//
//  StudentCommentCell.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/21.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "StudentCommentCell.h"
#import "LabelShowView.h"

@implementation StudentCommentCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)creatLabelView:(NSDictionary*)_labelInfDic labelArray:(NSArray*)_arr
{
    
    CGRect newFrame = [self frame];
    CGFloat tagY = 5;
    
    CGFloat gapWidth = 8;
    if (WIDTH == 375) {
        //6
        gapWidth = 15;
    }else if (WIDTH == 414) {
        gapWidth = 20;
    }
    CGFloat tagsWidth = (WIDTH - gapWidth*4)/3.0; //每行只放三个
    long numOfLine = _arr.count / 3; //取整
    int leftNum = _arr.count % 3; //取余
    int count = 0;
    for (int i = 0; i<numOfLine; i++) {
        for (int j = 0; j<3; j++) {
            CGRect tagFrame = CGRectZero;
            tagFrame = CGRectMake(gapWidth*(j+1) + tagsWidth*j, tagY, tagsWidth, 25);
            
            LabelShowView *labeView = [[LabelShowView alloc]initWithFrame:tagFrame andMode:@"show"];
            labeView.titleLabel.text      = [_arr[count] objectForKey:@"name"]; //[labDic objectForKey:@"name"];
            labeView.numberLabel.text     = [_arr[count] objectForKey:@"total"]; //[labDic objectForKey:@"total"];
            [self.contentView addSubview:labeView];
            count ++;
        }
        tagY += 30;
    }
    for (int i = 0; i<leftNum; i++) {
        CGRect tagFrame = CGRectZero;
        tagFrame = CGRectMake(gapWidth*(i+1) + tagsWidth*i, tagY, tagsWidth, 25);
        
        LabelShowView *labeView = [[LabelShowView alloc]initWithFrame:tagFrame andMode:@"show"];
        labeView.titleLabel.text      = [_arr[count] objectForKey:@"name"]; //[labDic objectForKey:@"name"];
        labeView.numberLabel.text     = [_arr[count] objectForKey:@"total"]; //[labDic objectForKey:@"total"];
        [self.contentView addSubview:labeView];
        count ++;
    }
    
    if (_arr.count) {
        if (_arr.count%3) {
            newFrame.size.height = 40.5 + tagY;
        }else{//余3==0
            newFrame.size.height = 40.5 + tagY - 30;
        }
    }
    self.frame = newFrame;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
