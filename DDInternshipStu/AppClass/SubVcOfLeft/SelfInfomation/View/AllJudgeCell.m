//
//  AllJudgeCell.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/21.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "AllJudgeCell.h"

@implementation AllJudgeCell

- (void)awakeFromNib {
    // Initialization code
    _titleLabel.text = NSLocalizedString(@"CompanyEvalutionKey", @"");
}

- (void)createCellHasStar:(BOOL)hasStar andStar:(NSString *)star
{
    if (!hasStar) {
        self.starBackView.hidden = YES;
        return;
    }
    int starNum = [star intValue];
    //全变灰色
    for (int i = 500; i<505; i++) {
        UIImageView *tempStarImage = [self.starBackView viewWithTag:i];
        tempStarImage.image = [UIImage imageNamed:@"灰星"];
    }
    //部分变黄
    for (int i = 500; i<500+starNum; i++) {
        UIImageView *tempStarImage = [self.starBackView viewWithTag:i];
        tempStarImage.image = [UIImage imageNamed:@"黄星"];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
