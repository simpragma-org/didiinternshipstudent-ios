//
//  LabelShowView.m
//  DDInternshipStu
//
//  Created by 何川 on 15/12/3.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "LabelShowView.h"

@implementation LabelShowView

- (instancetype)initWithFrame:(CGRect)frame andMode:(NSString *)titleName
{
    self = [super initWithFrame:frame];
    if (self) {
        UIImageView *bgImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        bgImage.image = [UIImage imageNamed:@"未选中"];
        UIEdgeInsets inset = UIEdgeInsetsMake(12.5, 12.5, 12.5, 30);
        bgImage.image = [bgImage.image resizableImageWithCapInsets:inset];
        
        [self addSubview:bgImage];
        
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0,self.frame.size.width-25, 25)];
        _titleLabel.textAlignment       = NSTextAlignmentCenter;
        _titleLabel.font                = [UIFont systemFontOfSize:10];
        [self addSubview:_titleLabel];
        
        _numberLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width-28, 0,30, 25)];
        _numberLabel.textAlignment       = NSTextAlignmentCenter;
        _numberLabel.font                = [UIFont systemFontOfSize:10];
        [self addSubview:_numberLabel];
        
        _btn = [UIButton buttonWithType:UIButtonTypeCustom];
        _btn.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        _btn.backgroundColor = [UIColor clearColor];
        [self addSubview:_btn];
    }
    return self;
}
@synthesize commentNumberStr = _commentNumberStr;
-(void)setCommentNumberStr:(NSString *)commentNumberStr
{
    _commentNumberStr = commentNumberStr;
    _numberLabel.text = commentNumberStr;
}

-(NSString *)commentNumberStr
{
    return _commentNumberStr;
}

@synthesize titleBackgroundColor = _titleBackgroundColor;
- (void)setTitleBackgroundColor:(UIColor *)titleBackgroundColor
{
    _titleBackgroundColor = titleBackgroundColor;
    _titleLabel.backgroundColor = titleBackgroundColor;
}
-(UIColor *)titleBackgroundColor
{
    return _titleBackgroundColor;
}
@end
