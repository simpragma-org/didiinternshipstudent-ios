//
//  DDSelectAddressCell.h
//  DDIntershipStu
//
//  Created by HERO-PC on 15/11/27.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDSelectAddressCell : UICollectionViewCell
/** 背景图片 */
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;

/** cell里的label */
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
/** cell里的箭头 */
@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;

@end
