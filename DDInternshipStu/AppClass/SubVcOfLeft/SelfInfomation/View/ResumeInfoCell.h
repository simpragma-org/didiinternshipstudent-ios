//
//  ResumeInfoCell.h
//  DDInternshipStu
//
//  Created by 何川 on 15/11/22.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResumeInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *titleImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *line;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

-(void)setIntroductionText:(NSString*)text;

@end
