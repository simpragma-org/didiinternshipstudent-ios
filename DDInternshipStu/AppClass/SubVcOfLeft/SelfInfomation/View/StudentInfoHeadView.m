//
//  StudentInfoHeadView.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/20.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "StudentInfoHeadView.h"
#import "UIView+nice.h"

@implementation StudentInfoHeadView

- (void)awakeFromNib {
    [_resumeBtn setTitle:NSLocalizedString(@"ResumeKey", @"") forState:UIControlStateNormal];
    [_addressSelBtn setTitle:NSLocalizedString(@"AddressKey", @"") forState:UIControlStateNormal];
    _ageLabel.text = NSLocalizedString(@"AgeKey", @"");
    _ohDownLabel.text = NSLocalizedString(@"AccumulatedKey", @"");
    _lv1DownLabel.text = NSLocalizedString(@"gradeKey", @"");
    [self.titleHeadView setBackgroundColor:COLOR_DEFAULT];
    [self.genderAgeView setBackgroundColor:COLOR_DEFAULT];
    [self.headView RoundedLayerWithCornerRadius:self.headView.frame.size.width/2 andBorderColor:COLOR_HEADAROUND andBorderWidth:4];
    [self.headBtn RoundedLayerWithCornerRadius:self.headBtn.frame.size.width/2 andBorderColor:[UIColor clearColor] andBorderWidth:0];
//    [self.resumeBtn setBackgroundImage:[self resizableImageWithName:@"简历bg"] forState:UIControlStateNormal];
}


- (void)createNameLablelWithName:(NSString *)nameStr andGender:(NSString *)gender andAge:(NSString *)age
{
    //名字
    CGSize size =[nameStr sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:18]}];
    NSLog(@"size.width:%f,size.height:%f",size.width,size.height);
    self.titleHeadViewWidth.constant = size.width + 66;
    self.namelabel.text = nameStr;
    //性别
    if ([gender isEqualToString:@"1"]) {
        //男
        self.genderImage.image = [UIImage imageNamed:@"男标志"];
    }else{
        //女
        self.genderImage.image = [UIImage imageNamed:@"女标志"];
    }
    //年龄
    self.ageLabel.text = [NSString stringWithFormat:@"%@岁",age];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
