//
//  LabelView.m
//  shixi
//
//  Created by 何川 on 15/7/25.
//  Copyright (c) 2015年 shixi.Team. All rights reserved.
//

#import "LabelView.h"

@implementation LabelView

- (instancetype)initWithFrame:(CGRect)frame andMode:(NSString *)titleName
{
    self = [super initWithFrame:frame];
    if (self) {
//        self.layer.cornerRadius     = 5;
//        self.layer.masksToBounds    = YES;
//        self.layer.borderColor      = COLOR_DEFAULT.CGColor;
//        self.layer.borderWidth      = .5f;
//        self.userInteractionEnabled = NO;
        UIImageView *bgImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        bgImage.image = [UIImage imageNamed:@"标签蓝框"];
        UIEdgeInsets inset = UIEdgeInsetsMake(12.5, 12.5, 12.5, 30);
        bgImage.image = [bgImage.image resizableImageWithCapInsets:inset];
        
        [self addSubview:bgImage];

        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0,self.frame.size.width-25, 25)];
//        _titleLabel.text                = @"做的不错";
        _titleLabel.textAlignment       = NSTextAlignmentCenter;
//        _titleLabel.textColor           = [UIColor colorWithHexString:@"#5c5d7b"];
        _titleLabel.font                = [UIFont systemFontOfSize:10];
        [self addSubview:_titleLabel];
        
        _numberLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width-27, 0,25, 25)];
        _numberLabel.textAlignment       = NSTextAlignmentCenter;
//        _numberLabel.textColor           = [UIColor whiteColor];
        _numberLabel.font                = [UIFont systemFontOfSize:10];
//        _numberLabel.backgroundColor     = [UIColor colorWithHexString:@"#c6c6c6"];
        [self addSubview:_numberLabel];

        _btn = [UIButton buttonWithType:UIButtonTypeCustom];
        _btn.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        _btn.backgroundColor = [UIColor clearColor];
        [self addSubview:_btn];
    }
    return self;
}
//按钮是否被选择
-(void)BtnisSeleted
{
    BOOL YesOrNo = _evaModel.isSeleted;
    if(YesOrNo){
        //选择为橙色
        self.titleBackgroundColor = [UIColor colorWithRed:0.96 green:0.49 blue:0 alpha:1];
        self.titleLabel.textColor = [UIColor whiteColor];
    }else{
        self.titleBackgroundColor = [UIColor whiteColor];
        self.titleLabel.textColor = [UIColor colorWithHexString:@"#5c5d7b"];
    }
}
@synthesize commentNumberStr = _commentNumberStr;
-(void)setCommentNumberStr:(NSString *)commentNumberStr
{
    _commentNumberStr = commentNumberStr;
    _numberLabel.text = commentNumberStr;
    
//    if ([commentNumberStr intValue] <=5) {
//        _numberLabel.backgroundColor     = [UIColor colorWithHexString:@"#82b1ff"];
//        _titleLabel.backgroundColor     = [UIColor colorWithHexString:@"#4489fe"];
//    }else
//    {
//        _titleLabel.backgroundColor     = [UIColor colorWithHexString:@"#f67c01"];
//        _numberLabel.backgroundColor     = [UIColor colorWithHexString:@"#ff9c00"];
//    }
}

-(NSString *)commentNumberStr
{
    return _commentNumberStr;
}

@synthesize titleBackgroundColor = _titleBackgroundColor;
- (void)setTitleBackgroundColor:(UIColor *)titleBackgroundColor
{
    _titleBackgroundColor = titleBackgroundColor;
    _titleLabel.backgroundColor = titleBackgroundColor;
}
-(UIColor *)titleBackgroundColor
{
    return _titleBackgroundColor;
}

@end
