//
//  CommnetCell.m
//  DiDiHR
//
//  Created by 何川 on 15/7/19.
//  Copyright (c) 2015年 com.shixi.didihr All rights reserved.
//

#import "CommnetCell.h"
#import "CommentListModel.h"
#import "UIView+nice.h"

@implementation CommnetCell

- (void)awakeFromNib {
    _comNameLabel.text = NSLocalizedString(@"SmileKey", @"");
    _commentTextLabel.text = NSLocalizedString(@"AppriciateKey", @"");
    
    for (int i=0; i<5; i++) {
        UIImageView* starImageView = [[UIImageView alloc]initWithFrame:CGRectMake(WIDTH-63+10*i,16, 8, 8)];
        [starImageView setImage:[UIImage imageNamed:@"小星-灰"]];
        [starImageView setTag:i+753];
        [self addSubview:starImageView];
    }
    
    self.line.backgroundColor       = COLOR_LINE;
    self.comTimeLabel.textColor     = COLOR_FONT_REGIST;
    self.comNameLabel.textColor     = COLOR_FONT_REGIST;
    self.commentTextLabel.textColor = [UIColor colorWithHexString:@"#515266"];
}

- (void)createCoachSoreStar:(NSInteger)_score andComment:(NSString *)commentStr
{
    
    if (_score != 0) {
        for (int i=0; i<5; i++) {
            UIImageView * starImageView = (UIImageView*)[self viewWithTag:i+753];
            [starImageView setImage:[UIImage imageNamed:@"小星-灰"]];
        }
        for (int i=0; i<_score; i++) {
            UIImageView * starImageView = (UIImageView*)[self viewWithTag:i+753];
            [starImageView setImage:[UIImage imageNamed:@"小星-黄"]];
            //[self addSubview:starImageView];
        }
    }
    self.commentTextLabel.text = commentStr;
    self.commentTextLabel.numberOfLines = 0;
    CGRect origonFrame = self.frame;
    CGSize size = CGSizeMake(WIDTH-30,1200); //设置一个行高上限
    NSDictionary *attribute = @{NSFontAttributeName:self.commentTextLabel.font};
    CGSize labelSize = [self.commentTextLabel.text boundingRectWithSize:size options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    origonFrame.size.height += (labelSize.height-20);
    self.frame = origonFrame;
    if (origonFrame.size.height < 61.013671875) {
        self.cellHeight = 61.013671875;
    }else{
        self.cellHeight = origonFrame.size.height;
    }
}
-(void)loadCellWithModel:(CommentListModel *)model
{
    self.comNameLabel.text = model.name;
    NSInteger score = [model.score integerValue];
    if ([model.score isKindOfClass:[NSNull class]]) {
        score = 0;
    }
    
    self.comTimeLabel.text = [self timeStringWithDate:model.add_time];
    if ([model.score intValue] != 0) {
        for (int i=0; i<5; i++) {
            UIImageView * starImageView = (UIImageView*)[self viewWithTag:i+753];
            [starImageView setImage:[UIImage imageNamed:@"小星-灰"]];
        }
        for (int i=0; i<[model.score intValue]; i++) {
            UIImageView * starImageView = (UIImageView*)[self viewWithTag:i+753];
            [starImageView setImage:[UIImage imageNamed:@"小星-黄"]];
        }
        
    }
    self.commentTextLabel.text = model.comment;
//    [self.commentTextLabel lineSpacing:0];
}
//时间戳处理
-(NSString *)timeStringWithDate:(NSString *)add_time{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm"];
    NSTimeZone* timeZone = [NSTimeZone systemTimeZone];
    [formatter setTimeZone:timeZone];
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:[add_time floatValue]];
    NSString *startStr = [formatter stringFromDate:startDate];
    NSString *startTimeSlot = [startStr substringToIndex:10];
    return startTimeSlot;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
