//
//  CommnetCell.h
//  DiDiHR
//
//  Created by 何川 on 15/7/19.
//  Copyright (c) 2015年 com.shixi.didihr All rights reserved.
//

#import <UIKit/UIKit.h>
@class CommentListModel;

@interface CommnetCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *commentTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *comTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *comNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *line;

@property (nonatomic, assign) CGFloat cellHeight;

- (void)createCoachSoreStar:(NSInteger)_score andComment:(NSString *)commentStr;

-(void)loadCellWithModel:(CommentListModel *)model;

@end
