//
//  LabelView.h
//  shixi
//
//  Created by 何川 on 15/7/25.
//  Copyright (c) 2015年 shixi.Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EvaluateModel.h"

@interface LabelView : UIView
/*标签标题**/
@property (strong, nonatomic) UILabel* titleLabel;
/*标签数量**/
@property (strong, nonatomic) UILabel* numberLabel;
/*标题背景颜色**/
@property (strong, nonatomic) UIColor* titleBackgroundColor;
/**标签数量背景颜色*/
@property (strong, nonatomic) UIColor* numberBackgroundColor;
/*标签宽度**/
@property (assign, nonatomic) CGFloat labelWidth;
/*标签评价数量**/
@property (strong, nonatomic) NSString* commentNumberStr;

@property (strong ,nonatomic) UIButton *btn;

@property (strong,nonatomic)EvaluateModel *evaModel;
//按钮是否被选择
-(void)BtnisSeleted;
- (instancetype)initWithFrame:(CGRect)frame andMode:(NSString *)titleName;

@end
