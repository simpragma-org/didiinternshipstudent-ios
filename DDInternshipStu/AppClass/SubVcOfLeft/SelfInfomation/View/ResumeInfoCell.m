//
//  ResumeInfoCell.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/22.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import "ResumeInfoCell.h"

@implementation ResumeInfoCell

- (void)awakeFromNib {
    _titleLabel.text = NSLocalizedString(@"ExperinceKey", @"");
    _contentLabel.text = NSLocalizedString(@"ContentKey", @"");
    self.titleLabel.textColor = COLOR_DEFAULT;
    self.line.backgroundColor = COLOR_DEFAULT;
    self.contentLabel.textColor = COLOR_FONT_INTERNSHIP;
}

//赋值 and 自动换行,计算出cell的高度
-(void)setIntroductionText:(NSString*)text{
    //获得当前cell高度
    CGRect frame = [self frame];
    //文本赋值
    self.contentLabel.text = text;
    //设置label的最大行数
    self.contentLabel.numberOfLines = 10;
    
    CGFloat tempWidth = WIDTH - 60;
    
    CGSize size = CGSizeMake(tempWidth,1200); //设置一个行高上限
    NSDictionary *attribute = @{NSFontAttributeName: self.contentLabel.font};
    CGSize labelSize = [self.contentLabel.text boundingRectWithSize:size options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    
    frame.size.height = labelSize.height + 74.5;
    
    self.frame = frame;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
