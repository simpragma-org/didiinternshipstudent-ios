//
//  StudentCommentCell.h
//  DDInternshipStu
//
//  Created by 何川 on 15/11/21.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LabelShowView.h"

@interface StudentCommentCell : UITableViewCell

@property (strong, nonatomic)  LabelShowView *labelView;
- (void)creatLabelView:(NSDictionary*)_labelInfDic labelArray:(NSArray*)_arr;

@end
