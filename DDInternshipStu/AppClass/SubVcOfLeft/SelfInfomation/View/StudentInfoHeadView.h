//
//  StudentInfoHeadView.h
//  DDInternshipStu
//
//  Created by 何川 on 15/11/20.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//
 
#import <UIKit/UIKit.h>

@interface StudentInfoHeadView : UITableViewCell
//中间线
@property (weak, nonatomic) IBOutlet UILabel *midLine;
//背景色
@property (weak, nonatomic) IBOutlet UIView *backGroundView;
//返回按钮
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
//头视图背景
@property (weak, nonatomic) IBOutlet UIView *headView;
//头视图图片
@property (weak, nonatomic) IBOutlet UIButton *headBtn;

//个人认证图片
@property (weak, nonatomic) IBOutlet UIImageView *certifyImage;
//总计时长
@property (weak, nonatomic) IBOutlet UILabel *totalHourLabel;
//等级水平
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
//姓名
@property (weak, nonatomic) IBOutlet UILabel *namelabel;
//学校
@property (weak, nonatomic) IBOutlet UILabel *schoolLabel;
//专业
@property (weak, nonatomic) IBOutlet UILabel *majorLabel;
//学生简历
@property (weak, nonatomic) IBOutlet UIButton *resumeBtn;
/** 工作范围按钮 */
@property (weak, nonatomic) IBOutlet UIButton *addressSelBtn;
//名字、性别、年龄的背景视图
@property (weak, nonatomic) IBOutlet UIView *titleHeadView;
//性别、年龄视图
@property (weak, nonatomic) IBOutlet UIView *genderAgeView;
//性别图片
@property (weak, nonatomic) IBOutlet UIImageView *genderImage;
//年龄标签
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
//名字、性别、年龄的背景视图-宽
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleHeadViewWidth;
@property (strong, nonatomic) IBOutlet UILabel *ohDownLabel;
@property (strong, nonatomic) IBOutlet UILabel *lv1DownLabel;


- (void)createNameLablelWithName:(NSString *)nameStr andGender:(NSString *)gender andAge:(NSString *)age;

@end
