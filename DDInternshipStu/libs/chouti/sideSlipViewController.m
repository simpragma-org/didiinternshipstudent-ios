//
//  sideSlipViewController.m
//  DiDiStu
//
//  Created by 何川 on 15/10/2.
//  Copyright (c) 2015年 何川. All rights reserved.


#import "sideSlipViewController.h"

#define kMenuFullWidth self.view.bounds.size.width
#define kMenuDisplayedWidth (self.view.bounds.size.width * 0.75)
#define kMenuOverlayWidth (self.view.bounds.size.width - kMenuDisplayedWidth)
#define kMenuBounceOffset 10.0f
#define kMenuBounceDuration .3f
#define kMenuSlideDuration .3f


@interface sideSlipViewController (Internal)

- (void)showShadow:(BOOL)val;


@end

@implementation sideSlipViewController

@synthesize delegate;

@synthesize pageTag = _pageTag;
@synthesize leftViewController = _left;
@synthesize rootViewController = _root;

@synthesize tap = _tap;
@synthesize pan = _pan;


- (instancetype)init
{
    if (self = [super init]) {
        
    }
    return self;
}

// 添加withTag:(NSInteger)tag
- (id)initWithRootViewController:(UIViewController *)controller
{
    if (self = [super init]) {
        _root = controller;
        
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setRootViewController:_root];
    
    if (!_tap) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
        tap.delegate = (id<UIGestureRecognizerDelegate>)self;
        [self.view addGestureRecognizer:tap];
        [tap setEnabled:NO];
        _tap = tap;
    }
}

- (void)viewDidUnload {
    [super viewDidUnload];
    _tap = nil;
    _pan = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return [_root shouldAutorotateToInterfaceOrientation:toInterfaceOrientation];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    if (_root) {
        
        [_root willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
        
        UIView *view = _root.view;
        
        if (_menuFlags.showingRightView) {
            
            view.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
            
        } else if (_menuFlags.showingLeftView) {
            
            view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
            
        } else {
            
            view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
            
        }
        
    }
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    if (_root) {
        
        [_root didRotateFromInterfaceOrientation:fromInterfaceOrientation];
        
        CGRect frame = self.view.bounds;
        if (_menuFlags.showingLeftView) {
            frame.origin.x = frame.size.width - kMenuOverlayWidth;
        } else if (_menuFlags.showingRightView) {
            frame.origin.x = -(frame.size.width - kMenuOverlayWidth);
        }
        _root.view.frame = frame;
        _root.view.autoresizingMask = self.view.autoresizingMask;
        
        [self showShadow:(_root.view.layer.shadowOpacity!=0.0f)];
        
    }
    
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    if (_root) {
        [_root willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    }
    
}

#pragma mark - GestureRecognizers

- (void)pan:(UIPanGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan) {
        [self showShadow:YES];
        _panOriginX = self.view.frame.origin.x;
        _panVelocity = CGPointMake(0.0f, 0.0f);
        
        if ([gesture velocityInView:self.view].x > 0) {
            _panDirection = SSMenuPanDirectionRight;
        }else{
            _panDirection = SSMenuPanDirectionLeft;
        }
        
    }
    
    if (gesture.state == UIGestureRecognizerStateChanged) {
        CGPoint velocity = [gesture velocityInView:self.view];
        if ((velocity.x*_panVelocity.x+velocity.y*_panVelocity.y)<0) {
            _panDirection = (_panDirection == SSMenuPanDirectionRight) ? SSMenuPanDirectionLeft:SSMenuPanDirectionRight;
        }
        
        _panVelocity = velocity;
        CGPoint translation = [gesture translationInView:self.view];
        CGRect frame = _root.view.frame;
        frame.origin.x = _panOriginX + translation.x;
        
        if (frame.origin.x > 0.0f && !_menuFlags.showingLeftView) {
            
            if (_menuFlags.canShowLeft) {
                _menuFlags.showingLeftView = YES;
                CGRect frame = self.view.bounds;
                frame.size.width = kMenuFullWidth;
                self.leftViewController.view.frame = frame;
                [self.view insertSubview:self.leftViewController.view aboveSubview:0];
            }else{
                frame.origin.x = 0.0f;
            }
            
        } else if (frame.origin.x < 0.0f && !_menuFlags.showingRightView)
        {
            if (_menuFlags.showingLeftView) {
                _menuFlags.showingLeftView = NO;
                [self.leftViewController.view removeFromSuperview];
            }
            
        
        }
        _root.view.frame = frame;
    }else if (gesture.state == UIGestureRecognizerStateEnded || gesture.state == UIGestureRecognizerStateCancelled)
    {
        [self.view setUserInteractionEnabled:NO];
        
        SSMenuPanCompletion completion = SSMenuPanCompletionRoot;
        
        if (_panDirection == SSMenuPanDirectionRight && _menuFlags.showingLeftView) {
            completion = SSMenuPanCompletionLeft;
        } else if (_panDirection == SSMenuPanDirectionLeft && _menuFlags.showingRightView)
        {
            completion = SSMenuPanCompletionRight;
        }
        
        CGPoint velocity = [gesture velocityInView:self.view];
        if (velocity.x < 0.0f) {
            velocity.x *= -1.0f;
        }
        BOOL bounce = (velocity.x > 800);
        CGFloat originX = _root.view.frame.origin.x;
        CGFloat width = _root.view.frame.size.width;
        CGFloat span = (width - kMenuOverlayWidth);
        CGFloat duration = kMenuSlideDuration;
        
        if (bounce) {
            duration = (span / velocity.x);
        }else{
            duration = ((span - originX) / span)*duration;
        }
        
        [CATransaction begin];
        [CATransaction setCompletionBlock:^{
            if (completion == SSMenuPanCompletionLeft) {
                [self showLeftController:NO];
            }else if (completion == SSMenuPanCompletionRight) {
                
            }else{
                [self showRootController:NO];
            }
            [_root.view.layer removeAllAnimations];
            [self.view setUserInteractionEnabled:YES];
        }];
        
        CGPoint pos = _root.view.layer.position;
        CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
        
        NSMutableArray *keyTimes = [[NSMutableArray alloc] initWithCapacity:bounce ? 3 : 2];
        NSMutableArray *values = [[NSMutableArray alloc] initWithCapacity:bounce ? 3 : 2];
        NSMutableArray *timingFunctions = [[NSMutableArray alloc] initWithCapacity:bounce ? 3 : 2];
        
        [values addObject:[NSValue valueWithCGPoint:pos]];
        [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
        [keyTimes addObject:[NSNumber numberWithFloat:0.0f]];
        if (bounce) {
            duration += kMenuBounceDuration;
            [keyTimes addObject:[NSNumber numberWithFloat:1.0f - (kMenuBounceDuration / duration)]];
            if (completion == SSMenuPanCompletionLeft) {
                
                [values addObject:[NSValue valueWithCGPoint:CGPointMake(((width/2)+span)+kMenuBounceOffset, pos.y)]];
                
            } else if (completion == SSMenuPanCompletionRight) {
                
                [values addObject:[NSValue valueWithCGPoint:CGPointMake(-((width/2) - (kMenuOverlayWidth-kMenuBounceOffset)), pos.y)]];
                
            } else {
                
                // depending on which way we're panning add a bounce offset
                if (_panDirection == SSMenuPanDirectionLeft) {
                    [values addObject:[NSValue valueWithCGPoint:CGPointMake((width/2) - kMenuBounceOffset, pos.y)]];
                } else {
                    [values addObject:[NSValue valueWithCGPoint:CGPointMake((width/2) + kMenuBounceOffset, pos.y)]];
                }
                
            }
            
            [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
            
        }
        
        if (completion == SSMenuPanCompletionLeft) {
            [values addObject:[NSValue valueWithCGPoint:CGPointMake((width/2) + span, pos.y)]];
        } else if (completion == SSMenuPanCompletionRight) {
            [values addObject:[NSValue valueWithCGPoint:CGPointMake(-((width/2) - kMenuOverlayWidth), pos.y)]];
        } else {
            [values addObject:[NSValue valueWithCGPoint:CGPointMake(width/2, pos.y)]];
        }
        
        [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [keyTimes addObject:[NSNumber numberWithFloat:1.0f]];
        
        animation.timingFunctions = timingFunctions;
        animation.keyTimes = keyTimes;
        //animation.calculationMode = @"cubic";
        animation.values = values;
        animation.duration = duration;
        animation.removedOnCompletion = NO;
        animation.fillMode = kCAFillModeForwards;
        [_root.view.layer addAnimation:animation forKey:nil];
        [CATransaction commit];
    }
}

- (void)tap:(UITapGestureRecognizer *)gesture
{
    [gesture setEnabled:NO];
    [self showRootController:YES];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    
    // Check for horizontal pan gesture
    if (gestureRecognizer == _pan) {
        
        UIPanGestureRecognizer *panGesture = (UIPanGestureRecognizer*)gestureRecognizer;
        CGPoint translation = [panGesture translationInView:self.view];
        
        if ([panGesture velocityInView:self.view].x < 600 && sqrt(translation.x * translation.x) / sqrt(translation.y * translation.y) > 1) {
            return YES;
        }
        
        return NO;
    }
    
    if (gestureRecognizer == _tap) {
        
        if (_root && (_menuFlags.showingRightView || _menuFlags.showingLeftView)) {
            return CGRectContainsPoint(_root.view.frame, [gestureRecognizer locationInView:self.view]);
        }
        
        return NO;
        
    }
    
    return YES;
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if (gestureRecognizer==_tap) {
        return YES;
    }
    return NO;
}

#pragma mark Nav Handling

- (void)resetNavButtons
{
    if (!_root) return;
    
    UIViewController *topController = nil;
    if ([_root isKindOfClass:[UINavigationController class]]) {
        
        UINavigationController *navController = (UINavigationController *)_root;
        if ([[navController viewControllers] count]>0) {
            topController = [[navController viewControllers] objectAtIndex:0];
        }else if ([_root isKindOfClass:[UITabBarController class]]){
            
            UITabBarController *tabController = (UITabBarController *)_root;
            topController = [tabController selectedViewController];
        }else{
            topController = _root;
        }
        
        if (_menuFlags.canShowLeft) {
            
            
                UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Home_Icon"] style:UIBarButtonItemStylePlain target:self action:@selector(showLeft:)];
                topController.navigationItem.leftBarButtonItem = button;
                        
        }
        
    }
}

- (void)showShadow:(BOOL)val {
    if (!_root) return;
    
    _root.view.layer.shadowOpacity = val ? 0.8f : 0.0f;
    if (val) {
        _root.view.layer.cornerRadius = 4.0f;
        _root.view.layer.shadowOffset = CGSizeZero;
        _root.view.layer.shadowRadius = 4.0f;
        _root.view.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.view.bounds].CGPath;
    }
    
}

- (void)showRootController:(BOOL)animated
{
    [_tap setEnabled:NO];
    _root.view.userInteractionEnabled = YES;
    
    CGRect frame = _root.view.frame;
    frame.origin.x = 0.0f;
    
    BOOL _enabled = [UIView areAnimationsEnabled];
    if (!animated) {
        [UIView setAnimationsEnabled:NO];
    }
    
    [UIView animateWithDuration:.3 animations:^{
        
        _root.view.frame = frame;
        
    } completion:^(BOOL finished) {
        
        if (_left && _left.view.superview) {
            [_left.view removeFromSuperview];
        }
        
        _menuFlags.showingLeftView = NO;
        [self showShadow:NO];
    }];
    
    if (!animated) {
        [UIView setAnimationsEnabled:_enabled];
    }
}

- (void)showLeftController:(BOOL)animated
{
    if (!_menuFlags.canShowLeft) {
        return;
    }
    
    if (_menuFlags.respondsToWillShowViewController) {
        [self.delegate menuController:self willShowViewController:self.leftViewController];
    }
    _menuFlags.showingLeftView = YES;
    [self showShadow:YES];
    
    UIView *view = self.leftViewController.view;
    CGRect frame = self.view.bounds;
    frame.size.width = kMenuFullWidth;
    view.frame = frame;
    [self.view insertSubview:view atIndex:0];
    [self.leftViewController viewWillAppear:animated];
    
    frame = _root.view.frame;
    frame.origin.x = CGRectGetMaxX(view.frame) - (kMenuFullWidth - kMenuDisplayedWidth);
    
    BOOL _enabled = [UIView areAnimationsEnabled];
    if (!animated) {
        [UIView setAnimationsEnabled:NO];
    }
    
    _root.view.userInteractionEnabled = NO;
    [UIView animateWithDuration:.3 animations:^{
        
        _root.view.frame = frame;
        
    } completion:^(BOOL finished) {
        
        [_tap setEnabled:YES];
        
    }];
    
    if (!animated) {
        [UIView setAnimationsEnabled:_enabled];
    }
}

#pragma mark Setters

- (void)setDelegate:(id<SSMenuControllerDelegate>)val
{
    delegate = val;
    _menuFlags.respondsToWillShowViewController = [(id)self.delegate respondsToSelector:@selector(menuController:willShowViewController:)];
}

- (void)setLeftViewController:(UIViewController *)leftViewController
{
    _left = leftViewController;
    _menuFlags.canShowLeft = (_left!=nil);
    [self resetNavButtons];
}

- (void)setRootViewController:(UIViewController *)rootViewController
{
    UIViewController *tempRoot = _root;
    _root = rootViewController;
    
    if (_root) {
        
        if (tempRoot) {
            [tempRoot.view removeFromSuperview];
            tempRoot = nil;
        }
        
        UIView *view = _root.view;
        view.frame = self.view.bounds;
        [self.view addSubview:view];
        
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
        pan.delegate = (id<UIGestureRecognizerDelegate>)self;
        [view addGestureRecognizer:pan];
        _pan = pan;
    }else{
        
        if (tempRoot) {
            [tempRoot.view removeFromSuperview];
            tempRoot = nil;
        }
    }
}

- (void)setRootController:(UIViewController *)controller animated:(BOOL)animated
{
    if (!controller) {
        [self setRootViewController:controller];
        return;
    }
    
    if (_menuFlags.showingLeftView) {
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        __block sideSlipViewController *selfRef = self;
        __block UIViewController *rootRef = _root;
        CGRect frame = rootRef.view.frame;
        frame.origin.x = rootRef.view.bounds.size.width;
        
        [UIView animateWithDuration:.1 animations:^{
            
            rootRef.view.frame = frame;
            
        } completion:^(BOOL finished) {
            
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            
            [selfRef setRootViewController:controller];
            _root.view.frame = frame;
            [selfRef showRootController:animated];
        }];
    }else{
        [self setRootViewController:controller];
        [self showRootController:animated];
    }
}




#pragma mark - Root Controller Navigation

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    NSAssert((_root!=nil), @"no root controller set");
    
    UINavigationController *navController = nil;
    
    if ([_root isKindOfClass:[UINavigationController class]]) {
        
        navController = (UINavigationController*)_root;
        
    } else if ([_root isKindOfClass:[UITabBarController class]]) {
        
        UIViewController *topController = [(UITabBarController*)_root selectedViewController];
        if ([topController isKindOfClass:[UINavigationController class]]) {
            navController = (UINavigationController*)topController;
        }
        
    }
    
    if (navController == nil) {
        
        NSLog(@"root controller is not a navigation controller.");
        return;
    }
    
    
    if (_menuFlags.showingRightView) {
        
        // if we're showing the right it works a bit different, we'll make a screen shot of the menu overlay, then push, and move everything over
        __block CALayer *layer = [CALayer layer];
        CGRect layerFrame = self.view.bounds;
        layer.frame = layerFrame;
        
        UIGraphicsBeginImageContextWithOptions(layerFrame.size, YES, 0);
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        [self.view.layer renderInContext:ctx];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        layer.contents = (id)image.CGImage;
        
        [self.view.layer addSublayer:layer];
        [navController pushViewController:viewController animated:NO];
        CGRect frame = _root.view.frame;
        frame.origin.x = frame.size.width;
        _root.view.frame = frame;
        frame.origin.x = 0.0f;
        
        CGAffineTransform currentTransform = self.view.transform;
        
        [UIView animateWithDuration:0.25f animations:^{
            
            if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
                
                self.view.transform = CGAffineTransformConcat(currentTransform, CGAffineTransformMakeTranslation(0, -[[UIScreen mainScreen] applicationFrame].size.height));
                
            } else {
                
                self.view.transform = CGAffineTransformConcat(currentTransform, CGAffineTransformMakeTranslation(-[[UIScreen mainScreen] applicationFrame].size.width, 0));
            }
            
            
        } completion:^(BOOL finished) {
            
            [self showRootController:NO];
            self.view.transform = CGAffineTransformConcat(currentTransform, CGAffineTransformMakeTranslation(0.0f, 0.0f));
            [layer removeFromSuperlayer];
            
        }];
        
    } else {
        
        [navController pushViewController:viewController animated:animated];
        
    }
    
}


#pragma mark - Action

- (void)showLeft:(id)sender
{
    [self showLeftController:YES];
}

@end
