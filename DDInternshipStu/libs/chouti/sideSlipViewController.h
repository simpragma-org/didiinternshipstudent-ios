//
//  sideSlipViewController.h
//  DiDiStu
//
//  Created by 何川 on 15/10/2.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    SSMenuPanDirectionLeft = 0,
    SSMenuPanDirectionRight,
} SSMenuPanDirection;

typedef enum {
    SSMenuPanCompletionLeft = 0,
    SSMenuPanCompletionRight,
    SSMenuPanCompletionRoot,
} SSMenuPanCompletion;

@protocol SSMenuControllerDelegate;


@interface sideSlipViewController : UIViewController <UIGestureRecognizerDelegate>
{
    id _tap;
    id _pan;
    CGFloat _panOriginX;
    CGPoint _panVelocity;
    SSMenuPanDirection _panDirection;


    struct {
        unsigned int respondsToWillShowViewController:1;
        unsigned int showingLeftView:1;
        unsigned int showingRightView:1;
        unsigned int canShowRight:1;
        unsigned int canShowLeft:1;
    } _menuFlags;
    
}

- (id)initWithRootViewController:(UIViewController *)controller;

@property (nonatomic, assign) id<SSMenuControllerDelegate> delegate;

@property (nonatomic, assign) NSInteger pageTag;
@property (nonatomic, strong) UIViewController *leftViewController;
@property (nonatomic, strong) UIViewController *rootViewController;

@property (nonatomic, readonly) UITapGestureRecognizer *tap;
@property (nonatomic, readonly) UIPanGestureRecognizer *pan;

- (void)setRootController:(UIViewController *)controller animated:(BOOL)animated;

- (void)showRootController:(BOOL)animated;

- (void)showLeftController:(BOOL)animated;

@end

@protocol SSMenuControllerDelegate <NSObject>

- (void)menuController:(sideSlipViewController *)root willShowViewController:(UIViewController *)controller;

@end

