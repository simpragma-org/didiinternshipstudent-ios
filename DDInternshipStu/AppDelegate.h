//
//  AppDelegate.h
//  DDInternshipStu
//
//  Created by 何川 on 15/11/13.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//
//                            _ooOoo_
//                           o8888888o
//                           88" . "88
//                           (| -_- |)
//                            O\ = /O
//                        ____/`---'\____
//                      .   ' \\| |// `.
//                       / \\||| : |||// \
//                     / _||||| -:- |||||- \
//                       | | \\\ - /// | |
//                     | \_| ''\---/'' | |
//                      \ .-\__ `-` ___/-. /
//                   ___`. .' /--.--\ `. . __
//                ."" '< `.___\_<|>_/___.' >'"".
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |
//                 \ \ `-. \_ __\ /__ _/ .-` / /
//         ======`-.____`-.___\_____/___.-`____.-'======
//                            `=---='
//
//         .............................................
//                  佛祖镇楼                 永无BUG
//                  佛祖也蒙比

#import <UIKit/UIKit.h>
#import "DDHomeViewController.h"
#import "DDMenuController.h"
#import "QZLoginViewController.h"
#import <BaiduMapAPI_Base/BMKBaseComponent.h>//引入base相关所有的头文件
#import <BaiduMapAPI_Location/BMKLocationComponent.h>//引入定位功能所有的头文件
@interface AppDelegate : UIResponder <UIApplicationDelegate,BMKLocationServiceDelegate>
{
    UIWindow *window;
    UINavigationController *navigationController;
    BMKMapManager* _mapManager;
    BMKLocationService * _locService;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) DDMenuController *ddMenu;//抽屉
@property (strong, nonatomic) DDHomeViewController *homeVc;//主视图
@property (strong, nonatomic) QZLoginViewController *loginVc;

//新浪微博 参数
@property (strong, nonatomic) NSString *wbtoken;
@property (strong, nonatomic) NSString *wbRefreshToken;
@property (strong, nonatomic) NSString *wbCurrentUserID;

+ (NSString *)nowTime;
+ (BOOL)isNetworkConnected;
/**获取系统当前时间*/
+ (NSString *)nowDateTime;
+ (NSString*)timeKey;
+ (NSString*)secretMd5Key;

@end

