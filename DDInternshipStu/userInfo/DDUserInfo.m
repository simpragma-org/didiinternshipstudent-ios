//
//  DDUserInfo.m
//  DDInternshipStu
//
//  Created by 何川 on 15/11/24.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

static NSString* asstokenString;
static NSString* name;
static NSString* mobile;
static NSString* device_number;
static NSString* registrationId;
static NSString* lastGetAccesstokenTime;
static NSString* headerImageUrl;
static NSString* const FORM_FLE_INPUT = @"file";

#import "DDUserInfo.h"
#import "TMCache.h"

@implementation DDUserInfo

#pragma mark - 类方法
+(void)keepUserMessageWithDic:(NSDictionary *)dic
{
    NSArray *keys = [dic allKeys];
    NSArray *values = [dic allValues];
    
    for (int i = 0;i < keys.count; i++) {
        if ([keys[i] isEqualToString:@"companyAddress"]) {
            continue;
        }
        [[TMCache sharedCache] setObject:values[i] forKey:keys[i]];
    }
}
+(void)cleanUserMessageWithArr:(NSArray *)arr
{
    for (NSString *key in arr) {
        [[TMCache sharedCache] removeObjectForKey:key];
    }
}

//清掉全部缓存数据，退出登录时使用
+(void)cleanAllUserMessage
{
    double log = [[self longitude] doubleValue];
    double lat = [[self latitude] doubleValue];
    NSString *registrationId = [DDUserInfo registrationId];
    
    [[TMCache sharedCache] removeAllObjects];
    //取消所有的本地通知
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [[TMCache sharedCache] setObject:registrationId forKey:@"registrationId"];
    [[TMCache sharedCache] setObject:registrationId forKey:@"device_number"];
    [[TMCache sharedCache] setObject:@(log) forKey:@"longitude"];
    [[TMCache sharedCache] setObject:@(lat) forKey:@"latitude"];
    
}
+(NSArray *)allItems
{
    NSArray *arr = @[@"registrationId",@"accesstoken",@"userName",@"password",@"getAccesstokenTime",@"userId",@"fullname",@"personsign",@"sex",@"headImage",@"codeImage",@"birthday",@"address",@"balance",@"finishedEdit",@"isPush"];
    return arr;
}
//判断有没有关闭通知-系统
+ (BOOL)isCanNotification
{
    if ([[[TMCache sharedCache] objectForKey:@"isCanNotification"] intValue]) {
        return YES;
    }
    return NO;
}

#pragma mark - 单例方法

//用户手机号
+ (NSString *)mobile
{
    mobile = [[TMCache sharedCache] objectForKey:@"mobile"];
    return mobile;
}
//设备ID:用户设备号
+ (NSString *)device_number
{
#warning 这里改成推送的
    if (IS_IOS7 && !IS_IOS8) {
        return [NSString stringWithFormat:@"%@",[NSDate date]];
    }
    device_number = [[TMCache sharedCache] objectForKey:@"device_number"];
    if([[TMCache sharedCache] objectForKey:@"device_number"]){
        return [[TMCache sharedCache] objectForKey:@"device_number"];
    }else{
        if (IS_IOS6 && !IS_IOS8){
            return [NSString stringWithFormat:@"%@",[NSDate date]];
        }else{
            return @"071be7a25b3";
        }
    }
}
/*接收推送*/
+(BOOL)isPush
{
    if ([[TMCache sharedCache] objectForKey:@"isPush"] == nil) {
        [[TMCache sharedCache] setObject:@(1) forKey:@"isPush"];
    }
    if ([[[TMCache sharedCache] objectForKey:@"isPush"] intValue]) {
        return YES;
    }
    return NO;
}

//访问令牌
+ (NSString* )assTokenString
{
    asstokenString = [[TMCache sharedCache] objectForKey:@"accesstoken"];
    return asstokenString;
}
//用户名
+ (NSString* )name
{
    name = [[TMCache sharedCache] objectForKey:@"name"];
    return name;
}
//推送id
+ (NSString* )registrationId
{
    registrationId = [[TMCache sharedCache] objectForKey:@"registrationId"];
    return registrationId;
}
//上次获取asstoken的时间
+ (NSString* )lastGetAccesstokenTime
{
    lastGetAccesstokenTime = [[TMCache sharedCache] objectForKey:@"getAccesstokenTime"];
    return lastGetAccesstokenTime;
}
//用户Id
+ (NSString* )uid
{
    //return  [[TMCache sharedCache] objectForKey:@"uid"];
    return @"602";
}
//用户信息是否编辑完整
+ (BOOL)finishedEdit
{
    if ([[[TMCache sharedCache] objectForKey:@"finishedEdit"] intValue]) {
        return YES;
    }
    return NO;
}

#pragma mark 公司信息
//公司名称
+ (NSString *)company
{
    return [[TMCache sharedCache] objectForKey:@"company"];
}
//企业注册号
+ (NSString *)registration
{
    return [[TMCache sharedCache] objectForKey:@"registration"];
}
//组织机构代码
+ (NSString *)organization
{
    return [[TMCache sharedCache] objectForKey:@"organization"];
}
//税务登记代码
+ (NSString *)tax
{
    return [[TMCache sharedCache] objectForKey:@"tax"];
}
#pragma mark 地址信息
//HR公司地点
+ (NSString *)company_address
{
    return [[TMCache sharedCache] objectForKey:@"company_address"];
}
//HR公司地点id
+ (NSString *)companyAddress_id
{
    return [[TMCache sharedCache] objectForKey:@"companyAddress_id"];
}
//HR公司地点类型
+ (NSString *)company_type
{
    return [[TMCache sharedCache] objectForKey:@"company_type"];
}
//HR接送地点经度
+ (NSString *)longitude
{
    return [[TMCache sharedCache] objectForKey:@"longitude"];
}
//HR接送地点纬度
+ (NSString *)latitude
{
    return [[TMCache sharedCache] objectForKey:@"latitude"];
}
//HR所在城市
+ (NSString *)userCity
{
    return [[TMCache sharedCache] objectForKey:@"userCity"];
}
//HR所在城市id
+ (NSString *)citycode
{
    return [[TMCache sharedCache] objectForKey:@"citycode"];
}
//HR所在区县
+ (NSString *)userDistrict
{
    return [[TMCache sharedCache] objectForKey:@"userDistrict"];
}
//HR所在区县id
+ (NSString *)userDistrict_id
{
    return [[TMCache sharedCache] objectForKey:@"userDistrict_id"];
}
//HR公司地点经度
+ (NSMutableDictionary *)companyAddressLocationDic
{
    return [[TMCache sharedCache] objectForKey:@"companyAddressLocationDic"];
}

#pragma mark 基本信息
//性别
+(BOOL)sex{
    return [[[TMCache sharedCache] objectForKey:@"sex"] intValue];
}
//学生头像地址
+ (NSURL *)headerImageUrl
{
    if ([[TMCache sharedCache] objectForKey:@"headerImageUrl"]) {
        headerImageUrl = [[TMCache sharedCache] objectForKey:@"headerImageUrl"];
    }else{
        headerImageUrl = [[TMCache sharedCache] objectForKey:@"avatar"];
    }
    NSURL *url;
//    if ([[headerImageUrl substringToIndex:4] isEqualToString:@"http"]) {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",headerImageUrl]];
//    }else{
//        url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",headerImageUrl]];
//    }
    return url;
}
//头像
+(UIImage *)headerImage{
    return [[TMCache sharedCache] objectForKey:@"headerImage"];
}
//二维码图片
+(UIImage *)codeImage{
    return [[TMCache sharedCache] objectForKey:@"codeImage"];
}
//HR证件号码
+ (NSString *)idcard
{
    return [[TMCache sharedCache] objectForKey:@"idcard"];
}
//出生日期
+(NSString *)birthday
{
    return [[TMCache sharedCache] objectForKey:@"birthday"];
}
//地区信息
+(NSString *)address
{
    return [[TMCache sharedCache] objectForKey:@"address"];
}
//用户签名
+(NSString *)personsign
{
    return [[TMCache sharedCache] objectForKey:@"personsign"];
}
//接收消息
+ (NSString *)message
{
    return [[TMCache sharedCache] objectForKey:@"message"];
}
/*邀请好友的奖励规则*/
+ (NSAttributedString *)articleFour
{
    return [[TMCache sharedCache] objectForKey:@"articleFour"];
}
/*HR课程支付类型*/
+ (NSString *)payTypeSelect
{
    return [[TMCache sharedCache] objectForKey:@"selectPay"];
}
#pragma mark - 预留
+ (NSString *)course
{
    return [[TMCache sharedCache] objectForKey:@"course"];
}
+ (NSString *)license_type
{
    return [[TMCache sharedCache] objectForKey:@"license_type"];
}
+ (NSString *)status
{
    return [[TMCache sharedCache] objectForKey:@"status"];
}
//学生姓名
+ (NSString *)studentName{
    return [[TMCache sharedCache] objectForKey:@"studentName"];
}
//学生性别
+ (NSString *)studentSex{
    NSString *studentSex = [NSString stringWithFormat:@"%@",[[TMCache sharedCache] objectForKey:@"studentSex"]];
    return studentSex;
}
//在读学位
+ (NSString *)studentRead{
    NSString *studentRead = [NSString stringWithFormat:@"%@",[[TMCache sharedCache] objectForKey:@"studentRead"]];
    return studentRead;
}
//年龄
+ (NSString *)studentAge{
    NSString *studentAge = [NSString stringWithFormat:@"%@",[[TMCache sharedCache] objectForKey:@"studentAge"]];
    return studentAge;
}

//学校名称
+ (NSString *)studentSchool{
    return [[TMCache sharedCache] objectForKey:@"studentSchool"];
}
//工作城市码
+ (NSString *)workCity{
    NSString *workCity = [NSString stringWithFormat:@"%@",[[TMCache sharedCache] objectForKey:@"workCity"]];
    return workCity;
}
//工作区域码
+ (NSString *)workDic{
    NSString *workDic = [NSString stringWithFormat:@"%@",[[TMCache sharedCache] objectForKey:@"workDic"]];
    return workDic;
}
//工作范围名称
+ (NSString *)workCityStr{
    return [[TMCache sharedCache] objectForKey:@"workCityStr"];
}
//个人介绍
+ (NSString *)introduction{
    return [[TMCache sharedCache] objectForKey:@"introduction"];
}
//专业
+ (NSString *)major{
    return [[TMCache sharedCache] objectForKey:@"major"];
}
//社团实习经历
+ (NSString *)intern{
    return [[TMCache sharedCache] objectForKey:@"intern"];
}
//技能奖项
+ (NSString *)skill{
    return [[TMCache sharedCache] objectForKey:@"skill"];
}
#pragma mark - 存储本地文件
+ (NSString *)postRequestWithURL: (NSString *)url  // IN
                      postParems: (NSDictionary *)postParems // IN
                    picFileImage: (UIImage *)picFileImage  // IN
                     picFileName: (NSString *)picFileName;  // IN
{
    
    
    NSString *TWITTERFON_FORM_BOUNDARY = @"0xKhTmLbOuNdArY";
    //根据url初始化request
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                       timeoutInterval:10];
    //分界线 --AaB03x
    NSString *MPboundary=[[NSString alloc] initWithFormat:@"--%@",TWITTERFON_FORM_BOUNDARY];
    //结束符 AaB03x--
    NSString *endMPboundary=[[NSString alloc] initWithFormat:@"%@--",MPboundary];
    //得到图片的data
    NSData* data = UIImageJPEGRepresentation(picFileImage, 0.7);
    
    //http body的字符串
    NSMutableString *body=[[NSMutableString alloc] init];
    //参数的集合的所有key的集合
    NSArray *keys= [postParems allKeys];
    
    //遍历keys
    for(int i=0;i<[keys count];i++)
    {
        //得到当前key
        NSString *key=[keys objectAtIndex:i];
        
        //添加分界线，换行
        [body appendFormat:@"%@\r\n",MPboundary];
        //添加字段名称，换2行
        [body appendFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key];
        //添加字段的值
        [body appendFormat:@"%@\r\n",[postParems objectForKey:key]];
        
        NSLog(@"添加字段的值==%@",[postParems objectForKey:key]);
    }
    
    if(picFileImage){
        ////添加分界线，换行
        [body appendFormat:@"%@\r\n",MPboundary];
        
        //声明pic字段，文件名为boris.png
        [body appendFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n",FORM_FLE_INPUT,picFileName];
        //声明上传文件的格式
        [body appendFormat:@"Content-Type: image/jpge,image/gif, image/jpeg, image/pjpeg, image/pjpeg\r\n\r\n"];
    }
    
    //声明结束符：--AaB03x--
    NSString *end=[[NSString alloc] initWithFormat:@"\r\n%@",endMPboundary];
    //声明myRequestData，用来放入http body
    NSMutableData *myRequestData=[NSMutableData data];
    
    //将body字符串转化为UTF8格式的二进制
    [myRequestData appendData:[body dataUsingEncoding:NSUTF8StringEncoding]];
    if(picFileImage){
        //将image的data加入
        [myRequestData appendData:data];
    }
    //加入结束符--AaB03x--
    [myRequestData appendData:[end dataUsingEncoding:NSUTF8StringEncoding]];
    
    //设置HTTPHeader中Content-Type的值
    NSString *content=[[NSString alloc] initWithFormat:@"multipart/form-data; boundary=%@",TWITTERFON_FORM_BOUNDARY];
    //设置HTTPHeader
    [request setValue:content forHTTPHeaderField:@"Content-Type"];
    //设置Content-Length
    [request setValue:[NSString stringWithFormat:@"%ld", (unsigned long)[myRequestData length]] forHTTPHeaderField:@"Content-Length"];
    //设置http body
    [request setHTTPBody:myRequestData];
    //http method
    [request setHTTPMethod:@"POST"];
    
    
    NSHTTPURLResponse *urlResponese = nil;
    NSError *error = [[NSError alloc] init];
    NSData* resultData = [NSURLConnection sendSynchronousRequest:request   returningResponse:&urlResponese error:&error];
    NSString* result= [[NSString alloc] initWithData:resultData encoding:NSUTF8StringEncoding];
    //if([urlResponese statusCode] >=200&&[urlResponese statusCode]<300){
    NSLog(@"返回结果=====%@",result);
    return result;
    //}
    return nil;
}

@end
