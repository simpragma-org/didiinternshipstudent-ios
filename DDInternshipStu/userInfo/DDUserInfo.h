//
//  DDUserInfo.h
//  DDInternshipStu
//
//  Created by 何川 on 15/11/24.
//  Copyright © 2015年 GaoJipeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DDUserInfo : NSObject
//缓存一些数据
+(void)keepUserMessageWithDic:(NSDictionary *)dic;
//清掉一些缓存数据
+(void)cleanUserMessageWithArr:(NSArray *)arr;
//清掉全部缓存数据，退出登录时使用
+(void)cleanAllUserMessage;
+(NSArray *)allItems;//所有缓存起来的用户信息的键的名字

//启动程序时缓存
+ (NSString *)registrationId;//推送id,同device_number
//登录时缓存
+ (NSString *)assTokenString;//访问令牌
+ (NSString *)uid;//用户Id
+ (NSString *)name;//用户名
+ (NSString *)mobile;//用户手机号
+ (NSString *)lastGetAccesstokenTime; //上次获取asstoken的时间
+ (NSString *)device_number;//设备ID:用户设备号
+ (BOOL)finishedEdit;//用户信息是否编辑完整

//地址信息
+ (NSString *)company_address;//HR公司地点
+ (NSString *)companyAddress_id;//HR公司地点id
+ (NSString *)company_type;//HR公司类型
+ (NSString *)longitude;//HR接送地点经度
+ (NSString *)latitude;//HR接送地点纬度
+ (NSString *)userCity;//HR所在城市
+ (NSString *)citycode;//HR所在城市id
+ (NSString *)userDistrict;//HR所在区县
+ (NSString *)userDistrict_id;//HR所在区县id
+ (NSString *)message;//接收消息
+ (NSMutableDictionary *)companyAddressLocationDic;//HR公司地点经度

//公司信息
+ (NSString *)company;//公司名称
+ (NSString *)registration;//企业注册号
+ (NSString *)organization;//组织机构代码
+ (NSString *)tax;//税务登记代码

//进入到首页时缓存（包括公司信息）
+ (BOOL)sex;//性别
+ (NSString *)personsign;//用户签名
+ (NSString *)idcard;//HR证件号码
+ (NSString *)birthday;//出生日期
+ (NSString *)address;//地区信息
+ (NSURL *)headerImageUrl;//HR头像地址
+ (UIImage *)headerImage;//头像
+ (UIImage *)codeImage;//二维码图片

//学生信息
+ (NSString *)studentName;//学生姓名
+ (NSString *)studentSex;//学生性别
+ (NSString *)studentRead;//在读学位
+ (NSString *)studentAge;//年龄
+ (NSString *)studentSchool;//学校名称
+ (NSString *)workCity;//工作城市码
+ (NSString *)workDic;//工作区域码
+ (NSString *)workCityStr;//工作范围名称

+ (NSString *)introduction;//个人介绍
+ (NSString *)major;//专业
+ (NSString *)intern;//社团实习经历
+ (NSString *)skill;//技能奖项

+(BOOL)isPush;//是否接收远程推送消息
/*邀请好友的奖励规则*/
+ (NSAttributedString *)articleFour;
/*HR课程支付类型*/
+ (NSString *)payTypeSelect;
//判断有没有关闭通知-系统
+ (BOOL)isCanNotification;

+ (NSString *)postRequestWithURL: (NSString *)url  // IN
                      postParems: (NSDictionary *)postParems // IN
                    picFileImage: (UIImage *)picFileImage  // IN
                     picFileName: (NSString *)picFileName;

@end
