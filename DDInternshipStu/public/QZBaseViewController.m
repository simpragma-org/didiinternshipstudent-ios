//
//  QZBaseViewController.m
//  shenBian
//
//  Created by 何川 on 15/8/18.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import "QZBaseViewController.h"

@interface QZBaseViewController ()

@end

@implementation QZBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //导航栏背景颜色
//    self.navigationController.navigationBar.barTintColor = COLOR_DEFAULT;
    //导航栏背景图片
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"1px"] forBarMetrics:UIBarMetricsDefault];
    //导航栏标题颜色
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,nil]];
    //
    //设置右滑返回上一级
    //
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    _request = [[QZRequest alloc]init];
//    _request = [QZRequest shareRequest];
    _request.delegate = self;
}
/**
 添加左右导航栏按钮
 */
-(void)addBtnOnNavWithTitle:(NSString *)title andImageName:(NSString *)name andTarget:(id)target andAction:(SEL)action andFrame:(CGRect)frame andDirection:(LXDirection)direction
{
    //
    //在导航栏添加按钮
    //
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    btn.frame = frame;
    [btn setImage:[UIImage imageNamed:name] forState:UIControlStateNormal];
    if (direction == LEFT) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:btn];
    }else{
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:btn];
    }
}
//避免重复代码的封装
-(void)requestWithKeys:(NSArray *)keys andValues:(NSArray *)values andUrlKey:(NSString *)urlKey
{
//    NSString *urlStr = [NSString stringWithFormat:@"%@%@",publicUrl,urlKey];
//    NSString *accesstoken = [QZUserInfo assTokenString];
//    NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithDictionary:@{@"accesstoken":accesstoken}];
    for (int i = 0; i < keys.count; i ++) {
//        [dic setObject:values[i] forKey:keys[i]];
    }
//    [self.request QZRequest_POST:urlStr parameters:dic tagNSString:urlKey stopRequest:YES isSerializer:NO];
}
//有些界面是webView的
-(void)createWebViewWithUrlStr:(NSString *)urlStr
{
    _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, WIDTH,HEIGHT-64)];
    [self.view addSubview:_webView];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:request];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
