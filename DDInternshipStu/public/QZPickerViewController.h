//
//  QZPickerViewController.h
//  DDInternshipStu
//
//  Created by 何川 on 15/9/9.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QZPickerViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>

@property (strong,nonatomic)UIPickerView *pickerView;
@property (strong,nonatomic)NSMutableArray *pickerSource;
@property (strong,nonatomic)NSMutableArray *pickerSource2;
@property (strong,nonatomic)UIColor *showColor;
@property (strong,nonatomic)UILabel *showLabel;
@property (strong,nonatomic)UITextField *showFiled;
@property (strong,nonatomic)NSMutableArray *showArr;//存储每一列所选择的内容
@property (nonatomic , assign)NSInteger selectCom;

-(void)actionAtRow:(NSInteger)row inComponent:(NSInteger)component;

@end
