//
//  QZBaseViewController.h
//  shenBian
//
//  Created by 何川 on 15/8/18.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+nice.h"
//#import "QZUserInfo.h"

@interface QZBaseViewController : UIViewController<QZRequestProtocol>
@property (strong,nonatomic)UIWebView *webView;//方便有些界面是webView
@property (strong,nonatomic)QZRequest *request;

-(void)addBtnOnNavWithTitle:(NSString *)title andImageName:(NSString *)name andTarget:(id)target andAction:(SEL)action andFrame:(CGRect)frame andDirection:(LXDirection)direction;

-(void)createWebViewWithUrlStr:(NSString *)urlStr;

//通过errcodde获得相应的信息
-(NSString *)getResultMsgWithErrcode:(NSString *)errcodde;
//快捷方式代码表
-(NSString *)getTitleNameWithScid:(NSString *)scid;

//避免重复代码的封装
-(void)requestWithKeys:(NSArray *)keys andValues:(NSArray *)values andUrlKey:(NSString *)urlKey;
@end
