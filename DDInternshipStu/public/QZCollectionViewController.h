//
//  QZCollectionViewController.h
//  shenBian
//
//  Created by 何川 on 15/8/18.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import "QZBaseViewController.h"

@interface QZCollectionViewController : QZBaseViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (strong)UICollectionView *collectionView;
@property (strong)NSMutableArray *collectionSource;

//注册分区头
-(void)registerHeaderCellWithClass:(Class)class addHeaderIdentifier:(NSString *)headerIdentifier andSizeForHeader:(CGSize)headerSize;

-(void)registerHeaderCellWithNibName:(NSString *)nibName addHeaderIdentifier:(NSString *)headerIdentifier andSizeForHeader:(CGSize)headerSize;

//注册分区尾
-(void)registerFooterCellWithClass:(Class)class addFooterIdentifier:(NSString *)footerIdentifier andSizeForFooter:(CGSize)footerSize;

-(void)registerFooterCellWithNibName:(NSString *)nibName addFooterIdentifier:(NSString *)footerIdentifier andSizeForFooter:(CGSize)footerSize;

//注册代码写的cell
-(void)registerCellWithClass:(Class)class addIdentifier:(NSString *)identifier addItemSize:(CGSize)itemSize  andItemSpacing:(CGFloat)itemSpacing andInsetsForSection:(UIEdgeInsets)insetsForSection;

//注册xib写的cell
-(void)registerCellWithNibName:(NSString *)nibName addIdentifier:(NSString *)identifier addItemSize:(CGSize)itemSize andItemSpacing:(CGFloat)itemSpacing andInsetsForSection:(UIEdgeInsets)insetsForSection;

-(void)loadCell:(UICollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

-(void)loadHeaderReusableView:(UICollectionReusableView *)reusableView atIndexPath:(NSIndexPath *)indexPath;

-(void)loadFooterReusableView:(UICollectionReusableView *)reusableView atIndexPath:(NSIndexPath *)indexPath;

-(void)actionAtIndexPath:(NSIndexPath *)indexPath;

@end
