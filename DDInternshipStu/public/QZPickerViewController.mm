
//
//  QZPickerViewController.m
//  DDInternshipStu
//
//  Created by 何川 on 15/9/9.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import "QZPickerViewController.h"

@interface QZPickerViewController ()

@end

@implementation QZPickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _pickerSource = [[NSMutableArray alloc]init];
    _showArr = [[NSMutableArray alloc]init];
    
    [self createPickerView];
}
#pragma mark - 初始化界面
- (void)createPickerView
{
    // 提示：在使用pickerView之前，必须指定数据源
    _pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, 200)];//_pickerView默认是216
    // 指定数据源
    [_pickerView setDataSource:self];
    // 指定代理
    [_pickerView setDelegate:self];
    // PickerView默认是没有选中标示的
    [_pickerView setShowsSelectionIndicator:YES];
    
    [self.view addSubview:_pickerView];
}

#pragma mark - PickerView的数据源方法
#pragma mark 指定列数
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return self.pickerSource.count;
}

#pragma mark 指定行数
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.pickerSource[component] count];
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return  35;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return  WIDTH / self.pickerSource.count;
}
#pragma mark 指定component列row行的内容
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0) {
        return self.pickerSource[component][row];
    }else{
        return self.pickerSource[1][component][row];
    
    }
    
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    if (component == 0) {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, WIDTH / self.pickerSource.count, 35)];
        label.textColor = _showColor;
        label.textAlignment = NSTextAlignmentCenter;
        label.text = self.pickerSource[component][row];
        label.font = [UIFont systemFontOfSize:17];
        
        return label;
    }else{
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, WIDTH / self.pickerSource.count, 35)];
        label.textColor = _showColor;
        label.textAlignment = NSTextAlignmentCenter;
        label.text = self.pickerSource[1][component][row];
        label.font = [UIFont systemFontOfSize:17];
        return label;
    
    }
    
}
#pragma mark - PickerView代理方法
/**
 component : 用户当前操作的列
 row :用户在component列选中的行
 
 这两个参数主要目的是用于数据联动操作的
 如果不需要数据联动，可以无视这两个参数
 
 如果需要知道用户在所有列中选择的信息，可以直接使用pickerView的selectedRowInComponent
 */
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self actionAtRow:row inComponent:component];
}

-(void)actionAtRow:(NSInteger)row inComponent:(NSInteger)component
{
    //        _showLabel.text = [NSString stringWithFormat:@"%@%@",_showLabel.text,self.pickerSource[component][row]];
    //        _showFiled.text = [NSString stringWithFormat:@"%@%@",_showFiled.text,self.pickerSource[component][row]];
     [self.pickerView reloadComponent:1];
    [_showArr replaceObjectAtIndex:component withObject:self.pickerSource[component][row]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
