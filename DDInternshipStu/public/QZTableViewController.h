//
//  QZTableViewController.h
//  shenBian
//
//  Created by 何川 on 15/8/18.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import "QZBaseViewController.h"

@interface QZTableViewController : QZBaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong)UITableView *tableView;
@property (strong)NSMutableArray *tableSource;

@property (assign)int page;//用来刷新加载的


//创建表格
-(void)createTableViewWithPlainStyle:(BOOL)isPlain andSeparatorStyleNone:(BOOL)isStyleNone andBackGroudImageName:(NSString *)imageName;

//注册xib写的cell
-(void)registCellWithNib:(NSString *)nibName addIdentifier:(NSString *)identifier addHeight:(CGFloat)height;

//注册代码写的cell
-(void)registCellWithClassName:(Class)className addIdentifier:(NSString *)identifier addHeight:(CGFloat)height;

//让子类重写,因为每个都不一样
-(void)requestData;

//让子类重写,因为每个都不一样
-(void)loadCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

//让子类重写,因为每个都不一样
-(void)actionAtIndexPath:(NSIndexPath *)indexPath;

//不需要添加上拉加载和下拉刷新得情况下就不调用这个方法
- (void)addRefreshWithRefreshAnimationWithImages:(NSArray *)images;
@end
