//
//  QZCollectionViewController.m
//  shenBian
//
//  Created by 何川 on 15/8/18.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import "QZCollectionViewController.h"

@interface QZCollectionViewController ()

@end

@implementation QZCollectionViewController
{
    NSString *_identifier;
    NSString *_headerIdentifier;
    NSString *_footerIndetifier;
    
    CGSize _itemSize;//每个item的size
    NSString *_className;//当前类名
    CGSize _headerSize;//每个header的size
    CGSize _footerSize;//每个footer的size
    
    CGFloat _itemSpacing;//行间距
    UIEdgeInsets _insetsForSection;// margin
    
    NSMutableArray *_idArr;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _className = NSStringFromClass([self class]);
    _collectionSource = [[NSMutableArray alloc]init];
    _idArr = [[NSMutableArray alloc]init];
    [self createCollectionView];
}
-(void)requestData
{
    
}
//创建瀑布流
-(void)createCollectionView
{
    //确定是水平滚动，还是垂直滚动
    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(44, 44, WIDTH - 44,HEIGHT - 44 - 64) collectionViewLayout:flowLayout];
//    if (IS_IOS7 && !IS_IOS8) {
//        _collectionView.frame = CGRectMake(44, 44 + 64, WIDTH - 44,HEIGHT - 44);
//    }
    _collectionView.dataSource=self;
    _collectionView.delegate=self;
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:_collectionView];
}
//-(void)registerHeaderCellWithClass:(Class)class addHeaderIdentifier:(NSString *)headerIdentifier andSizeForHeader:(CGSize)headerSize
//{
//    [_collectionView registerClass:class forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerIdentifier];
//    _headerIdentifier = headerIdentifier;
//    _headerSize = headerSize;
//}
//-(void)registerHeaderCellWithNibName:(NSString *)nibName addHeaderIdentifier:(NSString *)headerIdentifier andSizeForHeader:(CGSize)headerSize
//{
//    [_collectionView registerNib:[UINib nibWithNibName:nibName bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerIdentifier];
//    _headerIdentifier = headerIdentifier;
//    _headerSize = headerSize;
//}
//
//-(void)registerFooterCellWithClass:(Class)class addFooterIdentifier:(NSString *)footerIdentifier andSizeForFooter:(CGSize)footerSize
//{
//    [_collectionView registerClass:class forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:footerIdentifier];
//    _footerIndetifier = footerIdentifier;
//    _footerSize = footerSize;
//}
//-(void)registerFooterCellWithNibName:(NSString *)nibName addFooterIdentifier:(NSString *)footerIdentifier andSizeForFooter:(CGSize)footerSize
//{
//    [_collectionView registerNib:[UINib nibWithNibName:nibName bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:footerIdentifier];
//    _footerIndetifier = footerIdentifier;
//    _footerSize = footerSize;
//}

//注册代码写的cell
-(void)registerCellWithClass:(Class)class addIdentifier:(NSString *)identifier addItemSize:(CGSize)itemSize  andItemSpacing:(CGFloat)itemSpacing andInsetsForSection:(UIEdgeInsets)insetsForSection
{
    //注册Cell，必须要有
    [_collectionView registerClass:class forCellWithReuseIdentifier:identifier];
    _identifier = identifier;
    _itemSize = itemSize;
    
    _itemSpacing = itemSpacing;
    _insetsForSection = insetsForSection;
}
//注册xib写的cell
-(void)registerCellWithNibName:(NSString *)nibName addIdentifier:(NSString *)identifier addItemSize:(CGSize)itemSize andItemSpacing:(CGFloat)itemSpacing andInsetsForSection:(UIEdgeInsets)insetsForSection
{
    [_collectionView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellWithReuseIdentifier:identifier];
    [_idArr addObject:identifier];
    _identifier = identifier;
    _itemSize = itemSize;
    
    _itemSpacing = itemSpacing;
    _insetsForSection = insetsForSection;
}
#pragma mark -- UICollectionViewDataSource

//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _collectionSource.count;
}
//每个UICollectionView展示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:[_idArr objectAtIndex:indexPath.row] forIndexPath:indexPath];

//    if(!cell){
//        cell = [[UICollectionViewCell alloc]init];
//    }
    cell.backgroundColor = [UIColor clearColor];
    if (_collectionSource.count) {
        [self loadCell:cell atIndexPath:indexPath];
    }
    return cell;
}
// 设置分区头&分区尾
//-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
//{
//    if (kind == UICollectionElementKindSectionHeader) {
//
//        UICollectionReusableView *reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:_headerIdentifier forIndexPath:indexPath];
//
//        if (_collectionSource.count) {
//            [self loadHeaderReusableView:reusableView atIndexPath:indexPath];
//        }
//        return reusableView;
//    }else if(kind == UICollectionElementKindSectionFooter){
//        UICollectionReusableView *reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:_footerIndetifier forIndexPath:indexPath];
//
//        if (_collectionSource.count) {
//            [self loadFooterReusableView:reusableView atIndexPath:indexPath];
//        }
//        return reusableView;
//    }
//    return nil;
//}
//#pragma mark- UICollectionViewDelegateFlowLayout
//// 分区头的宽高
//-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
//{
//    return _headerSize;
//}
//// 分区尾的宽高
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
//{
//    return _footerSize;
//}
//纵向间距
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return _itemSpacing;
}
// 行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.5;
}
//定义每个Item 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return _itemSize;
}
//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return _insetsForSection;
}
#pragma mark --UICollectionViewDelegate

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self actionAtIndexPath:indexPath];
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

//点击触发的方法
-(void)actionAtIndexPath:(NSIndexPath *)indexPath
{
    
}
//加载页面
-(void)loadCell:(UICollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
}
-(void)loadHeaderReusableView:(UICollectionReusableView *)reusableView atIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void)loadFooterReusableView:(UICollectionReusableView *)reusableView atIndexPath:(NSIndexPath *)indexPath
{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
