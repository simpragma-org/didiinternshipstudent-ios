//
//  QZTableViewController.m
//  shenBian
//
//  Created by 何川 on 15/8/18.
//  Copyright (c) 2015年 何川. All rights reserved.
//

#import "QZTableViewController.h"
#import "MJRefresh.h"
#import "UIViewController+tool.h"

@interface QZTableViewController ()<UIGestureRecognizerDelegate>

@end

@implementation QZTableViewController
{
    BOOL _isPlain;//表格格式
    BOOL _isStyleNone;//表格有无分割线
    
    NSString *_identifier;//cell标识
    CGFloat _height;//cell的高度
    
    NSString *_className;//类名
    NSMutableArray *_picArr;//刷新加载动画图片组
    CGFloat _keyHeight;//视图距离屏幕底部的高度

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.page = 1;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _className = NSStringFromClass([self class]);
    
    _tableSource = [[NSMutableArray alloc]init];
}
//创建表格
-(void)createTableViewWithPlainStyle:(BOOL)isPlain andSeparatorStyleNone:(BOOL)isStyleNone andBackGroudImageName:(NSString *)imageName
{
    if (isPlain) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0,WIDTH, HEIGHT) style:UITableViewStylePlain];
    }else{
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0,WIDTH, HEIGHT-64) style:UITableViewStyleGrouped];
    }
    if (isStyleNone) {
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    if (IS_IOS7 && !IS_IOS8) {
        _tableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT);
    }
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:_tableView];
    
    [self setTableViewBackGroudWithImage:imageName];
    
    _isPlain = isPlain;
    _isStyleNone = isStyleNone;
}
//设置表格背景图片
-(void)setTableViewBackGroudWithImage:(NSString *)imageName
{
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:self.view.frame];
    imageView.image = [UIImage imageNamed:imageName];
    self.tableView.backgroundView = imageView;
}
#pragma mark - 下拉刷新和上拉加载相关方法
//不需要添加上拉加载和下拉刷新得情况下就不调用这个方法
- (void)addRefreshWithRefreshAnimationWithImages:(NSArray *)images
{
//    __weak UITableView *tableView = self.tableView;
    
    self.tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self refreshAction];
            [self.tableView.header endRefreshing];
        });
    }];
    self.tableView.header.automaticallyChangeAlpha = YES;
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self addObjAction];
            [self.tableView.footer endRefreshing];
        });
    }];
//    footer.hidden = YES;
    self.tableView.footer = footer;
    
    //一进来就刷新吧
    [self requestData];

}
//添加刷新加载动画
-(void)addRefreshAnimationWithImages:(NSArray *)images
{
    //刷新加载动画图片
}
-(void)refreshAction
{
    [_tableSource removeAllObjects];
    _page = 1;
    [self requestData];
}
-(void)addObjAction
{
    _page ++;
    [self requestData];
}

#pragma mark -

//让子类重写,因为每个都不一样
-(void)requestData
{
    
}

//注册xib写的cell
-(void)registCellWithNib:(NSString *)nibName addIdentifier:(NSString *)identifier addHeight:(CGFloat)height
{
    [_tableView registerNib:[UINib nibWithNibName:nibName bundle:nil]forCellReuseIdentifier:identifier];
    _identifier = identifier;
    _height = height;
}
//注册代码写的cell
-(void)registCellWithClassName:(Class)className addIdentifier:(NSString *)identifier addHeight:(CGFloat)height
{
    [_tableView registerClass:className forCellReuseIdentifier:identifier];
    _identifier = identifier;
    _height = height;
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(_isPlain){
        return 1;
    }else{
        return self.tableSource.count;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_isPlain) {
        return self.tableSource.count;
    }else{
        return [self.tableSource[section] count];
    }
}
//表格用到很多不同的cell最好重新写这个方法
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:_identifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;//默认为选中无效果，想有效果在loadCell里面写吧
    if (_tableSource.count) {
        [self loadCell:cell atIndexPath:indexPath];
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return _height;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_tableSource.count) {
        [self actionAtIndexPath:indexPath];
    }
}

//留给子类重写的方法
-(void)actionAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void)loadCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
