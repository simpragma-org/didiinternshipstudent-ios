//
//  StubbedControllerViewController.m
//  DDIntershipStu
//
//  Created by Abhijit Paul on 1/12/16.
//  Copyright © 2016 GaoJipeng. All rights reserved.
//

#import "StubbedControllerViewController.h"
#import "QZRequest.h"

@interface StubbedControllerViewController ()
@property (strong,nonatomic) NSString *json;
@end

@implementation StubbedControllerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSDictionary *)get_stubbed_data_urlString:(NSString *)urlString tag:(NSString *)tag {
//    if ([tag isEqualToString:@"get_district_list"] ) {
//            self.json = @"{\"status\":1,\"msg\":\"\u6b63\u786e\",\"data\":[{\"id\":\"1\",\"name\":\"\u5317\u4eac\u5e02\",\"alias\":\"e_city\",\"pid\":\"0\",\"sort\":\"1\",\"district\":[{\"id\":\"2\",\"name\":\"\u4e1c\u57ce\u533a\",\"alias\":\"e_city\",\"pid\":\"1\",\"sort\":\"1\"},{\"id\":\"3\",\"name\":\"\u897f\u57ce\u533a\",\"alias\":\"e_city\",\"pid\":\"1\",\"sort\":\"2\"},{\"id\":\"4\",\"name\":\"\u5d07\u6587\u533a\",\"alias\":\"e_city\",\"pid\":\"1\",\"sort\":\"3\"},{\"id\":\"5\",\"name\":\"\u5ba3\u6b66\u533a\",\"alias\":\"e_city\",\"pid\":\"1\",\"sort\":\"4\"},{\"id\":\"6\",\"name\":\"\u671d\u9633\u533a\",\"alias\":\"e_city\",\"pid\":\"1\",\"sort\":\"5\"},{\"id\":\"7\",\"name\":\"\u6d77\u6dc0\u533a\",\"alias\":\"e_city\",\"pid\":\"1\",\"sort\":\"6\"},{\"id\":\"8\",\"name\":\"\u4e30\u53f0\u533a\",\"alias\":\"e_city\",\"pid\":\"1\",\"sort\":\"7\"},{\"id\":\"9\",\"name\":\"\u77f3\u666f\u5c71\u533a\",\"alias\":\"e_city\",\"pid\":\"1\",\"sort\":\"8\"},{\"id\":\"10\",\"name\":\"\u623f\u5c71\u533a\",\"alias\":\"e_city\",\"pid\":\"1\",\"sort\":\"9\"},{\"id\":\"11\",\"name\":\"\u901a\u5dde\u533a\",\"alias\":\"e_city\",\"pid\":\"1\",\"sort\":\"10\"},{\"id\":\"12\",\"name\":\"\u987a\u4e49\u533a\",\"alias\":\"e_city\",\"pid\":\"1\",\"sort\":\"11\"},{\"id\":\"13\",\"name\":\"\u5927\u5174\u533a\",\"alias\":\"e_city\",\"pid\":\"1\",\"sort\":\"12\"},{\"id\":\"14\",\"name\":\"\u660c\u5e73\u533a\",\"alias\":\"e_city\",\"pid\":\"1\",\"sort\":\"13\"},{\"id\":\"15\",\"name\":\"\u5e73\u8c37\u533a\",\"alias\":\"e_city\",\"pid\":\"1\",\"sort\":\"14\"},{\"id\":\"16\",\"name\":\"\u6000\u67d4\u533a\",\"alias\":\"e_city\",\"pid\":\"1\",\"sort\":\"15\"},{\"id\":\"17\",\"name\":\"\u95e8\u5934\u6c9f\",\"alias\":\"e_city\",\"pid\":\"1\",\"sort\":\"16\"},{\"id\":\"18\",\"name\":\"\u5ef6\u5e86\u53bf\",\"alias\":\"e_city\",\"pid\":\"1\",\"sort\":\"17\"},{\"id\":\"19\",\"name\":\"\u5bc6\u4e91\u53bf\",\"alias\":\"e_city\",\"pid\":\"1\",\"sort\":\"18\"}]},{\"id\":\"20\",\"name\":\"\u4e0a\u6d77\u5e02\",\"alias\":\"e_city\",\"pid\":\"0\",\"sort\":\"2\",\"district\":[{\"id\":\"21\",\"name\":\"\u9ec4\u6d66\u533a\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"1\"},{\"id\":\"22\",\"name\":\"\u5362\u6e7e\u533a\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"2\"},{\"id\":\"23\",\"name\":\"\u5f90\u6c47\u533a\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"3\"},{\"id\":\"24\",\"name\":\"\u957f\u5b81\u533a\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"4\"},{\"id\":\"25\",\"name\":\"\u9759\u5b89\u533a\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"5\"},{\"id\":\"26\",\"name\":\"\u666e\u9640\u533a\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"6\"},{\"id\":\"27\",\"name\":\"\u95f8\u5317\u533a\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"7\"},{\"id\":\"28\",\"name\":\"\u8679\u53e3\u533a\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"8\"},{\"id\":\"29\",\"name\":\"\u6d66\u4e1c\u65b0\u533a\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"9\"},{\"id\":\"30\",\"name\":\"\u6768\u6d66\u533a\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"10\"},{\"id\":\"31\",\"name\":\"\u5b9d\u5c71\u533a\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"11\"},{\"id\":\"32\",\"name\":\"\u95f5\u884c\u533a\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"12\"},{\"id\":\"33\",\"name\":\"\u5609\u5b9a\u533a\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"13\"},{\"id\":\"34\",\"name\":\"\u677e\u6c5f\u533a\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"14\"},{\"id\":\"35\",\"name\":\"\u91d1\u5c71\u533a\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"15\"},{\"id\":\"36\",\"name\":\"\u9752\u6d66\u533a\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"16\"},{\"id\":\"37\",\"name\":\"\u5357\u6c47\u533a\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"17\"},{\"id\":\"38\",\"name\":\"\u5949\u8d24\u533a\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"18\"},{\"id\":\"39\",\"name\":\"\u5d07\u660e\u53bf\",\"alias\":\"e_city\",\"pid\":\"20\",\"sort\":\"19\"}]},{\"id\":\"40\",\"name\":\"\u676d\u5dde\u5e02\",\"alias\":\"e_city\",\"pid\":\"0\",\"sort\":\"3\",\"district\":[{\"id\":\"41\",\"name\":\"\u4e0a\u57ce\u533a\",\"alias\":\"e_city\",\"pid\":\"40\",\"sort\":\"1\"},{\"id\":\"42\",\"name\":\"\u4e0b\u57ce\u533a\",\"alias\":\"e_city\",\"pid\":\"40\",\"sort\":\"2\"},{\"id\":\"43\",\"name\":\"\u6c5f\u5e72\u533a\",\"alias\":\"e_city\",\"pid\":\"40\",\"sort\":\"3\"},{\"id\":\"44\",\"name\":\"\u62f1\u5885\u533a\",\"alias\":\"e_city\",\"pid\":\"40\",\"sort\":\"4\"},{\"id\":\"45\",\"name\":\"\u897f\u6e56\u533a\",\"alias\":\"e_city\",\"pid\":\"40\",\"sort\":\"5\"},{\"id\":\"46\",\"name\":\"\u6ee8\u6c5f\u533a\",\"alias\":\"e_city\",\"pid\":\"40\",\"sort\":\"6\"},{\"id\":\"47\",\"name\":\"\u8427\u5c71\u533a\",\"alias\":\"e_city\",\"pid\":\"40\",\"sort\":\"7\"},{\"id\":\"48\",\"name\":\"\u4f59\u676d\u533a\",\"alias\":\"e_city\",\"pid\":\"40\",\"sort\":\"8\"},{\"id\":\"49\",\"name\":\"\u6850\u5e90\u53bf\",\"alias\":\"e_city\",\"pid\":\"40\",\"sort\":\"9\"},{\"id\":\"50\",\"name\":\"\u6df3\u5b89\u53bf\",\"alias\":\"e_city\",\"pid\":\"40\",\"sort\":\"10\"},{\"id\":\"51\",\"name\":\"\u5efa\u5fb7\u5e02\",\"alias\":\"e_city\",\"pid\":\"40\",\"sort\":\"11\"},{\"id\":\"52\",\"name\":\"\u5bcc\u9633\u5e02\",\"alias\":\"e_city\",\"pid\":\"40\",\"sort\":\"12\"},{\"id\":\"53\",\"name\":\"\u4e34\u5b89\u5e02\",\"alias\":\"e_city\",\"pid\":\"40\",\"sort\":\"13\"}]},{\"id\":\"54\",\"name\":\"\u6df1\u5733\u5e02\",\"alias\":\"e_city\",\"pid\":\"0\",\"sort\":\"4\",\"district\":[{\"id\":\"55\",\"name\":\"\u798f\u7530\u533a\",\"alias\":\"e_city\",\"pid\":\"54\",\"sort\":\"1\"},{\"id\":\"56\",\"name\":\"\u7f57\u6e56\u533a\",\"alias\":\"e_city\",\"pid\":\"54\",\"sort\":\"2\"},{\"id\":\"57\",\"name\":\"\u5357\u5c71\u533a\",\"alias\":\"e_city\",\"pid\":\"54\",\"sort\":\"3\"},{\"id\":\"58\",\"name\":\"\u76d0\u7530\u533a\",\"alias\":\"e_city\",\"pid\":\"54\",\"sort\":\"4\"},{\"id\":\"59\",\"name\":\"\u5b9d\u5b89\u533a\",\"alias\":\"e_city\",\"pid\":\"54\",\"sort\":\"5\"},{\"id\":\"60\",\"name\":\"\u9f99\u5c97\u533a\",\"alias\":\"e_city\",\"pid\":\"54\",\"sort\":\"6\"},{\"id\":\"61\",\"name\":\"\u5149\u660e\u65b0\u533a\",\"alias\":\"e_city\",\"pid\":\"54\",\"sort\":\"7\"},{\"id\":\"62\",\"name\":\"\u576a\u5c71\u65b0\u533a\",\"alias\":\"e_city\",\"pid\":\"54\",\"sort\":\"8\"},{\"id\":\"63\",\"name\":\"\u9f99\u534e\u65b0\u533a\",\"alias\":\"e_city\",\"pid\":\"54\",\"sort\":\"9\"},{\"id\":\"64\",\"name\":\"\u5927\u9e4f\u65b0\u533a\",\"alias\":\"e_city\",\"pid\":\"54\",\"sort\":\"10\"}]},{\"id\":\"65\",\"name\":\"\u6210\u90fd\u5e02\",\"alias\":\"e_city\",\"pid\":\"0\",\"sort\":\"5\",\"district\":[{\"id\":\"66\",\"name\":\"\u6210\u534e\u533a\",\"alias\":\"e_city\",\"pid\":\"65\",\"sort\":\"1\"},{\"id\":\"67\",\"name\":\"\u6b66\u4faf\u533a\",\"alias\":\"e_city\",\"pid\":\"65\",\"sort\":\"2\"},{\"id\":\"68\",\"name\":\"\u9752\u7f8a\u533a\",\"alias\":\"e_city\",\"pid\":\"65\",\"sort\":\"3\"},{\"id\":\"69\",\"name\":\"\u9526\u6c5f\u533a\",\"alias\":\"e_city\",\"pid\":\"65\",\"sort\":\"4\"},{\"id\":\"70\",\"name\":\"\u91d1\u725b\u533a\",\"alias\":\"e_city\",\"pid\":\"65\",\"sort\":\"5\"},{\"id\":\"71\",\"name\":\"\u9f99\u6cc9\u9a7f\u533a\",\"alias\":\"e_city\",\"pid\":\"65\",\"sort\":\"6\"},{\"id\":\"72\",\"name\":\"\u9752\u767d\u6c5f\u533a\",\"alias\":\"e_city\",\"pid\":\"65\",\"sort\":\"7\"},{\"id\":\"73\",\"name\":\"\u65b0\u90fd\u533a\",\"alias\":\"e_city\",\"pid\":\"65\",\"sort\":\"8\"},{\"id\":\"74\",\"name\":\"\u6e29\u6c5f\u533a\",\"alias\":\"e_city\",\"pid\":\"65\",\"sort\":\"9\"}]},{\"id\":\"75\",\"name\":\"\u5e7f\u5dde\u5e02\",\"alias\":\"e_city\",\"pid\":\"0\",\"sort\":\"6\",\"district\":[{\"id\":\"76\",\"name\":\"\u8d8a\u79c0\u533a\",\"alias\":\"e_city\",\"pid\":\"75\",\"sort\":\"1\"},{\"id\":\"77\",\"name\":\"\u8354\u6e7e\u533a\",\"alias\":\"e_city\",\"pid\":\"75\",\"sort\":\"2\"},{\"id\":\"78\",\"name\":\"\u6d77\u73e0\u533a\",\"alias\":\"e_city\",\"pid\":\"75\",\"sort\":\"3\"},{\"id\":\"79\",\"name\":\"\u5929\u6cb3\u533a\",\"alias\":\"e_city\",\"pid\":\"75\",\"sort\":\"4\"},{\"id\":\"80\",\"name\":\"\u767d\u4e91\u533a\",\"alias\":\"e_city\",\"pid\":\"75\",\"sort\":\"5\"},{\"id\":\"81\",\"name\":\"\u9ec4\u57d4\u533a\",\"alias\":\"e_city\",\"pid\":\"75\",\"sort\":\"6\"},{\"id\":\"82\",\"name\":\"\u756a\u79ba\u533a\",\"alias\":\"e_city\",\"pid\":\"75\",\"sort\":\"7\"},{\"id\":\"83\",\"name\":\"\u82b1\u90fd\u533a\",\"alias\":\"e_city\",\"pid\":\"75\",\"sort\":\"8\"},{\"id\":\"84\",\"name\":\"\u5357\u6c99\u533a\",\"alias\":\"e_city\",\"pid\":\"75\",\"sort\":\"9\"},{\"id\":\"85\",\"name\":\"\u841d\u5c97\u533a\",\"alias\":\"e_city\",\"pid\":\"75\",\"sort\":\"10\"},{\"id\":\"86\",\"name\":\"\u589e\u57ce\u5e02\",\"alias\":\"e_city\",\"pid\":\"75\",\"sort\":\"11\"},{\"id\":\"87\",\"name\":\"\u4ece\u5316\u5e02\",\"alias\":\"e_city\",\"pid\":\"75\",\"sort\":\"12\"}]},{\"id\":\"88\",\"name\":\"\u5176\u4ed6\",\"alias\":\"e_city\",\"pid\":\"0\",\"sort\":\"7\",\"district\":[]}]}";
//
//        } //*** api added
    
    if([tag isEqualToString:@"set_loc"]) {
        self.json = @"{\"status\":1,\"msg\":\"\u7ecf\u7eac\u5ea6\u5b58\u50a8\u6210\u529f\"}";
    }
//    else if([tag isEqualToString:@"get_article"]) {
//        self.json = @"";
//    } //*** api added
    
    NSError *error;
    NSData *data = [self.json dataUsingEncoding:NSUTF8StringEncoding];
    if (!(data == nil)) {
        NSDictionary *value = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
        return value;
    }
    else {
        return nil;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
